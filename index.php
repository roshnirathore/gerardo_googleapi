<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <link rel="icon" href="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/icon.png">
  <title>Clever clinic</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="assets/css/mdb.min.css" rel="stylesheet">
  <link href="assets/css/addons/datatables.min.css" rel="stylesheet">


    <link href='assets/fullcalendar-4.2.0/packages/core/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/daygrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/core/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/bootstrap/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/timegrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/daygrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/list/main.css' rel='stylesheet' />
    <script src='assets/fullcalendar-4.2.0/packages/core/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/interaction/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/bootstrap/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/daygrid/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/timegrid/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/list/main.js?asdf'></script>

  <!-- Your custom styles (optional) -->
  <link href="assets/css/style.css?dshjjdfshj" rel="stylesheet">

 <!-- ZAPIER EXPERT'S CODE START -->
    <link href="https://rawgit.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet" />
  <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js?asdf"></script>
  <script src="https://rawgit.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script> 
 <style type="text/css">
   .multiple-tag-history , .multiple-tag-info, .multiple-tag-metho, .multiple-tag-history-soap ,.multiple-tag-treatment-soap , .multiple-tag-homework-soap , .multiple-tag-medical-soap , .multiple-tag-current-soap , .multiple-tag-intervention-soap , .multiple-tag-individual-homework-soap,.multiple-tag-individual-plan-soap, .multiple-tag-couple-soap-version,.multiple-tag-couple-soap-homework,.multiple-tag-couple-soap-plan,.multiple-tag-couple-soap-intervention{
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-history, .close-info, .close-metho , .close-history-soap , .close-treatment-soap , .close-homework-soap , .close-medical-soap , .close-current-soap , .close-intervention-soap , .close-individual-homework-soap, .close-individual-plan-soap ,.close-couple-soap-version,.close-couple-soap-homework,.close-couple-soap-plan,.close-couple-soap-intervention {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .select-box{
    width: 702px; 
    display: inline-block;
  }
   .multiple-add-btn{
    padding: 8px 9px;
    border: 1px solid #ccc;
    background-color: #52bb52;
    color: #fff;
    cursor: pointer; 
    border-radius: 4px; 
    margin-top: 15px;
    display: inline-block;
    width: auto;
  }
  .text-color{
    color:#4B70C4;
    font-weight: 400;
    text-transform: uppercase;
  }
  /*=============== Digital and webcam css ==================*/
   .modal {
        position: absolute;
        left: 0;
        top: 0;
        z-index: 1050;
        display: none;
        width: 100%;
        height: 100%;
        overflow: visible;
        outline: 0;
    }

    button.browser-btn {
    display: flex;
    justify-content: space-between;
    padding: 0px;
    height: auto;
    outline: none;
    }

    button.browser-btn span {
        padding: 8px;
    }

    button.browser-btn span.browse-btn {
        background: #e6e6e6;
    }
    .fc-event-container {
      color: white;
    }
 </style>
<!-- ZAPIER EXPERT'S CODE END -->
</head>

<body onload="ini.ini();">
    <div class="d-flex" id="wrapper">
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <div class="sidebar-heading">
                <img class="mx-auto d-block" style="width: 7em;height: 100%;" src="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/cleverc-8.png">
            </div>
            <div class="list-group list-group-flush" id="menu_inicial2">
                
               
                
            </div>
        </div>
        
        <!-- Sidebar -->
        <div class="navbar-dark border-right white-text main_navbar d-none" id="sidebar-wrapper">
            <div class="sidebar-heading">
                <img class="mx-auto d-block" style="width: 7em;height: 100%;" src="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/cleverc-8.png">
            </div>
            <div class="list-group list-group-flush" id="menu_inicial">
                
               
                
            </div>
        </div>
        <!-- /#sidebar-wrapper -->
    
        <!-- Page Content -->
        <div id="page-content-wrapper">
    
           <nav class="navbar navbar-expand-lg navbar-dark main_navbar d-none">
                <!-- Navbar brand -->
                <span class="d-block d-md-none" style="color:#4B70C4;cursor:pointer" onclick="openNav()">&#9776;</span>
                <a class="navbar-brand" style="color:#4B70C4; font-weight:400" href="#"></a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                    <ul class="navbar-nav ml-auto">
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                style="color:#4B70C4;">
                                <i class="fas fa-user"></i> Profile </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                                <a class="dropdown-item waves-effect waves-light" style="color:#4B70C4;" onclick="login.logout();">Log out</a>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </nav>
    
            <div class="container-fluid p-0 p-2" id="main_view">

            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    
    
    
</body>
<!-- SCRIPTS -->

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="assets/js/popper.min.js?asdf"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="assets/js/mdb.min.js?asdf"></script>
<script type="text/javascript" src="assets/js/addons/datatables.min.js?asdf"></script>
<script type="text/javascript" src="assets/js/addons/typeahead.js?asdf"></script>
<!-- Full Calendar -->
<script src="assets/fullcalendar-4.2.0/packages/core/main.js?asdf"></script>
<script src='assets/js/addons/moment.js?asdf'></script>
<script type="text/javascript" src="app/ini.js?<?php echo rand(); ?>"></script>
<link
    href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css"
    rel="stylesheet">
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js">
</script>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
<!-- ZAPIER EXPERT'S CODE START -->
 <script type="text/javascript">
  $(document).ready(function(){ 

    /*=========== Edit Box show in therapist clinical note ===========*/
      $summerNoteCouple = "";
      $summerNoteCoupleFree = "";
      $summerNoteIndividual = "";

      $('body').on('click', '#modal_form_type_session', function(){
        if($summerNoteIndividual != ""){
          $('#add_terms_conditions_clinical_notes').summernote('destroy');
          $summerNoteIndividual = "";
        }

        if($summerNoteCouple != ""){
          $('#add_couple_conditions_clinical_notes').summernote('destroy');
          $summerNoteCouple = "";
        }
        if($summerNoteCoupleFree != ""){
          $('#free_style_conditions_clinical_notes').summernote('destroy');
          $summerNoteCoupleFree = "";
        }
          
      })

      $('body').on('click', '#select_couple_options', function(){
        var couple_free_version_class_name = $('#couple_free_style_couple_session').attr('class'); 

        var couple_free_style_class_name = $('#couple_free_writing').attr('class'); 

        if(couple_free_version_class_name == 'row mt-3'){
          $summerNoteCouple = $('#add_couple_conditions_clinical_notes').summernote({
            placeholder: '',
            tabsize: 2,
            height: 100,
            toolbar: [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['fontname', ['fontname']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['insert', ['link']],
            ],
          });
        }else{
          if($summerNoteCouple != ""){
            $('#add_couple_conditions_clinical_notes').summernote('destroy');
            $summerNoteCouple = "";
          }
        }

        if(couple_free_style_class_name == 'row mt-3'){
          $summerNoteCoupleFree = $('#free_style_conditions_clinical_notes').summernote({
            placeholder: '',
            tabsize: 2,
            height: 100,
            toolbar: [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['fontname', ['fontname']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['insert', ['link']],
            ],
          });
        }else{
          if($summerNoteCoupleFree != ""){
            $('#free_style_conditions_clinical_notes').summernote('destroy');
            $summerNoteCoupleFree = "";
          }
        }
        var couple_soap_full_version_class_name = $('#couple_soap_full_version').attr('class');
        if(couple_soap_full_version_class_name == 'row mt-3' ){
          console.log('couple_soap_full_version_class_name');
          $('#couples_current_medications').editableSelect({ filter: false });
          $('#couple_intervention').editableSelect({ filter: false });
          $('#couple_soap_homework').editableSelect({ filter: false });
          $('#couple_soap_plan').editableSelect({ filter: false });
        }

         var couple_clinical_note_autocomplete = $('#couple_clinical_note_autocomplete').attr('class');
        if(couple_clinical_note_autocomplete == 'row mt-3' ){
          $('#couple_client_history_copping_mechanism').editableSelect({ filter: false });
          $('#couple_clinical_note_autocomplete_treatment_and_interventions').editableSelect({ filter: false });
          $('#couple_homework_follow_up_next_session').editableSelect({ filter: false });
        }


      });
      
      $( 'body' ).on( "click", '#select_individual_options', function() {
          

          var free_version_class_name = $('#individual_free_version').attr('class');
          if(free_version_class_name == 'row mt-3'){
            $summerNoteIndividual = $('#add_terms_conditions_clinical_notes').summernote({
                placeholder: '',
                tabsize: 2,
                height: 100,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link']],
                ],
            });

         }else{
          if($summerNoteIndividual != ""){
            $('#add_terms_conditions_clinical_notes').summernote('destroy');
            $summerNoteIndividual = "";
          }
        }
         /*=========== Edit Box show in therapist clinical note end===========*/




        var soap_class_name = $('#individual_soap_autocomplete').attr('class');
        var intake_class_name = $('#individual_intake_session').attr('class');
        var full_version_class_name = $('#individual_soap_full_version').attr('class');

        if(intake_class_name == 'row mt-3'){
          $('#clients_health_history').editableSelect({ filter: false });
          $('#clients_background_information').editableSelect({ filter: false }); 
          $('#metho_collect_information').editableSelect({ filter: false });
          $('#individual_intake_treatment_and_interventions').editableSelect({ filter: false });
          $('#individual_intake_homework_follow_up_next_session').editableSelect({ filter: false });
        }

        if(soap_class_name == 'row mt-3'){
          $('#individual_client_history_copping_mechanism').editableSelect({ filter: false });
          $('#individual_soap_autocomplete_treatment_and_interventions').editableSelect({ filter: false });
          $('#individual_homework_follow_up_next_session').editableSelect({ filter: false });
        }

        if(full_version_class_name == 'row mt-3' ){
          $('#clients_medical_history').editableSelect({ filter: false });
          $('#clients_current_medications').editableSelect({ filter: false });
          $('#individual_intervention').editableSelect({ filter: false });
          $('#individual_soap_homework').editableSelect({ filter: false });
          $('#individual_soap_plan').editableSelect({ filter: false });
        }

      });
        /*=========== Intake Form start =======================*/
         //First select box
      $( 'body' ).on( "click", '#clients_health_history_btn', function() {
        var multiselect = $('#clients_health_history').val();
         $('#clients_health_history_div').append('<p class="multiple-tag-history">'+multiselect+' || <span class="close-history"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
       $( 'body' ).on('click','.close-history',function(){
         $(this).parents('.multiple-tag-history').remove();
      });

      //Second Select box
      $( 'body' ).on( "click", '#clients_background_information_btn', function() {
        var multiselect = $('#clients_background_information').val();
        $('#clients_background_information_div').append('<p class="multiple-tag-info">'+multiselect+' || <span class="close-info"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>');
      });
      $( 'body' ).on('click','.close-info',function(){
         $(this).parents('.multiple-tag-info').remove();
      });
       //Third Select box
      $( 'body' ).on( "click", '#metho_collect_information_btn', function() {
        var multiselect = $('#metho_collect_information').val();
        $('#metho_collect_information_div').append('<p class="multiple-tag-metho">'+multiselect+' || <span class="close-metho"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>');
      });
      $( 'body' ).on('click','.close-metho',function(){
         $(this).parents('.multiple-tag-metho').remove();
      });

      //fourth Select box
      $( 'body' ).on( "click", '#individual_intake_treatment_and_interventions_btn', function() {
        var multiselect = $('#individual_intake_treatment_and_interventions').val();
         $('#individual_intake_treatment_and_interventions_div').append('<p class="multiple-tag-treatment-soap">'+multiselect+' || <span class="close-treatment-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-treatment-soap',function(){
         $(this).parents('.multiple-tag-treatment-soap').remove();
      });
      //fifth select box
      $( 'body' ).on( "click", '#individual_intake_homework_follow_up_next_session_btn', function() {
        var multiselect = $('#individual_intake_homework_follow_up_next_session').val();
         $('#individual_intake_homework_follow_up_next_session_div').append('<p class="multiple-tag-homework-soap">'+multiselect+' || <span class="close-homework-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-homework-soap',function(){
         $(this).parents('.multiple-tag-homework-soap').remove();
      });

      /*=========== Intake Form end =======================*/

      /*==================== Soap Autocomplete Start =========================*/
      //First Select box
      $( 'body' ).on( "click", '#individual_client_history_copping_mechanism_btn', function() {
        var multiselect = $('#individual_client_history_copping_mechanism').val();
         $('#individual_client_history_copping_mechanism_div').append('<p class="multiple-tag-history-soap">'+multiselect+' || <span class="close-history-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-history-soap',function(){
         $(this).parents('.multiple-tag-history-soap').remove();
      });
      //Second Select box
      $( 'body' ).on( "click", '#individual_soap_autocomplete_treatment_and_interventions_btn', function() {
        var multiselect = $('#individual_soap_autocomplete_treatment_and_interventions').val();
         $('#individual_soap_autocomplete_treatment_and_interventions_div').append('<p class="multiple-tag-treatment-soap">'+multiselect+' || <span class="close-treatment-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-treatment-soap',function(){
         $(this).parents('.multiple-tag-treatment-soap').remove();
      });
      //third select box
      $( 'body' ).on( "click", '#individual_homework_follow_up_next_session_btn', function() {
        var multiselect = $('#individual_homework_follow_up_next_session').val();
         $('#individual_homework_follow_up_next_session_div').append('<p class="multiple-tag-homework-soap">'+multiselect+' || <span class="close-homework-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-homework-soap',function(){
         $(this).parents('.multiple-tag-homework-soap').remove();
      });
      /*==================== Soap Autocomplete End =========================*/

      /*==================== Soap Full VERSION Start =========================*/
      //First select box
      $( 'body' ).on( "click", '#clients_medical_history_btn', function() {
        var multiselect = $('#clients_medical_history').val();
         $('#clients_medical_history_div').append('<p class="multiple-tag-medical-soap">'+multiselect+' || <span class="close-medical-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-medical-soap',function(){
         $(this).parents('.multiple-tag-medical-soap').remove();
      });
      //Second select box
       $( 'body' ).on( "click", '#clients_current_medications_btn', function() {
        var multiselect = $('#clients_current_medications').val();
         $('#clients_current_medications_div').append('<p class="multiple-tag-current-soap">'+multiselect+' || <span class="close-current-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-current-soap',function(){
         $(this).parents('.multiple-tag-current-soap').remove();
      });
       //Third select box
       $( 'body' ).on( "click", '#individual_intervention_btn', function() {
        var multiselect = $('#individual_intervention').val();
         $('#individual_intervention_div').append('<p class="multiple-tag-intervention-soap">'+multiselect+' || <span class="close-intervention-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-intervention-soap',function(){
         $(this).parents('.multiple-tag-intervention-soap').remove();
      });
       //Fourth select box
       $( 'body' ).on( "click", '#individual_soap_homework_btn', function() {
        var multiselect = $('#individual_soap_homework').val();
         $('#individual_soap_homework_div').append('<p class="multiple-tag-individual-homework-soap">'+multiselect+' || <span class="close-individual-homework-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-individual-homework-soap',function(){
         $(this).parents('.multiple-tag-individual-homework-soap').remove();
      });

      //Fifth select Box
       $( 'body' ).on( "click", '#individual_soap_plan_btn', function() {
        var multiselect = $('#individual_soap_plan').val();
         $('#individual_soap_plan_div').append('<p class="multiple-tag-individual-plan-soap">'+multiselect+' || <span class="close-individual-plan-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-individual-plan-soap',function(){
         $(this).parents('.multiple-tag-individual-plan-soap').remove();
      });
      /*==================== Soap Full VERSION end =========================*/

      /*===================== Couple soap full version Start ====================*/
      $( 'body' ).on( "click", '#couples_current_medications_btn', function() {
        var multiselect = $('#couples_current_medications').val();
         $('#couples_current_medications_div').append('<p class="multiple-tag-couple-soap-version">'+multiselect+' || <span class="close-couple-soap-version"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-soap-version',function(){
         $(this).parents('.multiple-tag-couple-soap-version').remove();
      });


      $( 'body' ).on( "click", '#couple_intervention_btn', function() {
        var multiselect = $('#couple_intervention').val();
         $('#couple_intervention_div').append('<p class="multiple-tag-couple-soap-intervention">'+multiselect+' || <span class="close-couple-soap-intervention"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-soap-intervention',function(){
         $(this).parents('.multiple-tag-couple-soap-intervention').remove();
      });


      $( 'body' ).on( "click", '#couple_soap_homework_btn', function() {
        var multiselect = $('#couple_soap_homework').val();
         $('#couple_soap_homework_div').append('<p class="multiple-tag-couple-soap-homework">'+multiselect+' || <span class="close-couple-soap-homework"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-soap-homework',function(){
         $(this).parents('.multiple-tag-couple-soap-homework').remove();
      });

      $( 'body' ).on( "click", '#couple_soap_plan_btn', function() {
        var multiselect = $('#couple_soap_plan').val();
         $('#couple_soap_plan_div').append('<p class="multiple-tag-couple-soap-plan">'+multiselect+' || <span class="close-couple-soap-plan"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-soap-plan',function(){
         $(this).parents('.multiple-tag-couple-soap-plan').remove();
      });

      /*===================== Couple soap full version End ====================*/ 

      /*=============== Clinical note (autocomplete version) start ================*/
      $( 'body' ).on( "click", '#couple_copping_mechanism_btn', function() {
        var multiselect = $('#couple_client_history_copping_mechanism').val();
         $('#couple_copping_mechanism_div').append('<p class="multiple-tag-couple-autocomplete-copping">'+multiselect+' || <span class="close-couple-autocomplete-copping"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-autocomplete-copping',function(){
         $(this).parents('.multiple-tag-couple-autocomplete-copping').remove();
      });


      $( 'body' ).on( "click", '#couple_clinical_note_autocomplete_treatment_and_interventions_btn', function() {
        var multiselect = $('#couple_clinical_note_autocomplete_treatment_and_interventions').val();
         $('#couple_clinical_note_autocomplete_treatment_and_interventions_div').append('<p class="multiple-tag-couple-autocomplete-interventions">'+multiselect+' || <span class="close-couple-autocomplete-interventions"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-autocomplete-interventions',function(){
         $(this).parents('.multiple-tag-couple-autocomplete-interventions').remove();
      });


      $( 'body' ).on( "click", '#homework_follow_up_btn', function() {
        var multiselect = $('#couple_homework_follow_up_next_session').val();
         $('#homework_follow_up_div').append('<p class="multiple-tag-couple-autocomplete-homework">'+multiselect+' || <span class="close-couple-autocomplete-homework"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-couple-autocomplete-homework',function(){
         $(this).parents('.multiple-tag-couple-autocomplete-homework').remove();
      });
      /*================= Clinical note (autocomplete version) End ================*/

       $('body').on('click','#intak_sub',function() {
         var soap_class_name = $('#individual_soap_autocomplete').attr('class');
         var intake_class_name = $('#individual_intake_session').attr('class');
         var full_version_class_name = $('#individual_soap_full_version').attr('class');
         var couple_soap_full_version_class_name = $('#couple_soap_full_version').attr('class');
         var couple_clinical_note_autocomplete_class_name = $('#couple_clinical_note_autocomplete').attr('class');

        /*=========== Intake Form start =======================*/
        //first div blank
         
        if(intake_class_name == 'row mt-3' && soap_class_name == 'row mt-3 d-none' && full_version_class_name == 'row mt-3 d-none'){
          $('#clients_health_history').val('');
          var history_div = $('#clients_health_history_div').text();
          console.log(history_div);
          $('#clients_health_history').val(history_div);

          //second div blank
          $('#clients_background_information').val('');
          var info_div = $('#clients_background_information_div').text();
          $('#clients_background_information').val(info_div);

          //third div blank
          $('#metho_collect_information').val('');
          var metho_div = $('#metho_collect_information_div').text();
          $('#metho_collect_information').val(metho_div);

          //fourth div blank
          $('#individual_intake_treatment_and_interventions').val('');
          var treatment_div_soap = $('#individual_intake_treatment_and_interventions_div').text();
          $('#individual_intake_treatment_and_interventions').val(treatment_div_soap);

          //fifth div blank
          $('#individual_intake_homework_follow_up_next_session').val('');
          var homework_div_soap = $('#individual_intake_homework_follow_up_next_session_div').text();
          $('#individual_intake_homework_follow_up_next_session').val(homework_div_soap);

         }
        /*=========== Intake Form end =======================*/
        /*=============================================================*/
        /*=========== soap Autocompleted Form start =======================*/
        //first div blank
        if(intake_class_name == 'row mt-3 d-none' && soap_class_name == 'row mt-3' && full_version_class_name == 'row mt-3 d-none'){
          $('#individual_client_history_copping_mechanism').val('');
          var history_div_soap = $('#individual_client_history_copping_mechanism_div').text();
          $('#individual_client_history_copping_mechanism').val(history_div_soap);

          //Second div blank
          $('#individual_soap_autocomplete_treatment_and_interventions').val('');
          var treatment_div_soap = $('#individual_soap_autocomplete_treatment_and_interventions_div').text();
          $('#individual_soap_autocomplete_treatment_and_interventions').val(treatment_div_soap);

          //Third div blank
          $('#individual_homework_follow_up_next_session').val('');
          var homework_div_soap = $('#individual_homework_follow_up_next_session_div').text();
          $('#individual_homework_follow_up_next_session').val(homework_div_soap);
        }
        /*=========== soap Autocompleted Form End =======================*/

        /*=========== soap Full Version Form start =======================*/
        if(intake_class_name == 'row mt-3 d-none' && soap_class_name == 'row mt-3 d-none' && full_version_class_name == 'row mt-3'){
          //First div blank
          $('#clients_medical_history').val('');
          var history_div_soap = $('#clients_medical_history_div').text();
          $('#clients_medical_history').val(history_div_soap);

          //Second div blank
          $('#clients_current_medications').val('');
          var treatment_div_soap = $('#clients_current_medications_div').text();
          $('#clients_current_medications').val(treatment_div_soap);

          //Third div blank
          $('#individual_intervention').val('');
          var intervention_div_soap = $('#individual_intervention_div').text();
          $('#individual_intervention').val(intervention_div_soap);

          //Fourth div blank
          $('#individual_soap_homework').val('');
          var homework_div_soap = $('#individual_soap_homework_div').text();
          $('#individual_soap_homework').val(homework_div_soap);

           //Fifth div blank
          $('#individual_soap_plan').val('');
          var plan_div_soap = $('#individual_soap_plan_div').text();
          $('#individual_soap_plan').val(plan_div_soap);
        }

        
         /*=========== soap Full Version Form end =======================*/

         /*=========== Couple soap Full Version Form Start =======================*/
         if (couple_soap_full_version_class_name == 'row mt-3') {
            $('#couples_current_medications').val('');
            var medications = $('#couples_current_medications_div').text();
            $('#couples_current_medications').val(medications);

            $('#couple_intervention').val('');
            var intervention = $('#couple_intervention_div').text();
            $('#couple_intervention').val(intervention);

            $('#couple_soap_homework').val('');
            var homework = $('#couple_soap_homework_div').text();
            $('#couple_soap_homework').val(homework);

              $('#couple_soap_plan').val('');
            var plan = $('#couple_soap_plan_div').text();
            $('#couple_soap_plan').val(plan);
         }

         /*=========== Couple soap Full Version Form end =======================*/

          /*=========== Couple autocomplete Form Start =======================*/
         if (couple_clinical_note_autocomplete_class_name == 'row mt-3') {
            $('#couple_client_history_copping_mechanism').val('');
            var copping_mechanism = $('#couple_copping_mechanism_div').text();
            $('#couple_client_history_copping_mechanism').val(copping_mechanism);

            $('#couple_clinical_note_autocomplete_treatment_and_interventions').val('');
            var intervention = $('#couple_clinical_note_autocomplete_treatment_and_interventions_div').text();
            $('#couple_clinical_note_autocomplete_treatment_and_interventions').val(intervention);

            $('#couple_homework_follow_up_next_session').val('');
            var homework = $('#homework_follow_up_div').text();
            $('#couple_homework_follow_up_next_session').val(homework);
         }

         /*=========== Couple autocomplete Form end =======================*/

  
       /* =========================================================================*/
       /* =========================================================================*/


        /*=========== Edit Box show in therapist clinical note ===========*/

        /*====================Individual session=================*/
        var free_version_class_name = $('#individual_free_version').attr('class');
        if(free_version_class_name == 'row mt-3'){
         
          var terms_conditions =  $('#add_terms_conditions_clinical_notes').summernote('code');
          //var decoded = $("<div/>").html(terms_conditions).text();
          $('#add_terms_conditions').val(terms_conditions);
           $('.note-editor').hide();
          }

          /*====================Couple session=================*/
         var couple_free_version_class_name = $('#couple_free_style_couple_session').attr('class');
         var couple_free_style_class_name = $('#couple_free_writing').attr('class');

        if(couple_free_version_class_name == 'row mt-3'){
          var terms_conditions =  $('#add_couple_conditions_clinical_notes').summernote('code');
           //var decoded = $("<div/>").html(terms_conditions).text();
          $('#add_couple_terms_conditions').val(terms_conditions);
           $('.note-editor').hide();
          }

          if(couple_free_style_class_name == 'row mt-3'){
          var terms_conditions =  $('#free_style_conditions_clinical_notes').summernote('code');
           //var decoded = $("<div/>").html(terms_conditions).text();
          $('#free_style_terms_conditions').val(terms_conditions);
           $('.note-editor').hide();
          }

         /*=========== Edit Box show in therapist clinical note ===========*/
        

      });

      /*======main issues add other option for intake form start======*/
      $( 'body' ).on( "click", '.miscellaneus-other-option', function() {
        $("#other_option_miscellaneus").css('display', 'block');
        
      })
      $( 'body' ).on( "click", '.miscellaneus', function() {
        $("#other_option_miscellaneus").css('display', 'none');
        var miscellaneus_text = $("input:radio.mental-health:checked").val();
        $('#other_option_miscellaneus').val(miscellaneus_text);
         
      })


      $( 'body' ).on( "click", '.workplace-other-option', function() {
        $("#other_option_workplace").css('display', 'block');
      })
      $( 'body' ).on( "click", '.workplace', function() {
        $("#other_option_workplace").css('display', 'none');
        var workplace_text = $("input:radio.mental-health:checked").val();
        $('#other_option_workplace').val(workplace_text);
      })


      $( 'body' ).on( "click", '.relationship-other-option', function() {
        $("#other_option_relationship").css('display', 'block');
      })
      $( 'body' ).on( "click", '.relationship', function() {
        $("#other_option_relationship").css('display', 'none');
        var relationship_text = $("input:radio.mental-health:checked").val();
         $('#other_option_relationship').val(relationship_text);
      })


       $( 'body' ).on( "click", '.mental-health-other-option', function() {
        $("#other_option_mental_health").css('display', 'block');
      })
      $( 'body' ).on( "click", '.mental-health', function() {
        $("#other_option_mental_health").css('display', 'none');
        var mental_text = $("input:radio.mental-health:checked").val();
         $('#other_option_mental_health').val(mental_text);
      })


      $( 'body' ).on( "click", '.addiction-other-option', function() {
        $("#other_option_addiction").css('display', 'block');
      })
      $( 'body' ).on( "click", '.addiction', function() {
        $("#other_option_addiction").css('display', 'none');
         //$('.addiction')
        var addication_text = $("input:radio.addiction:checked").val();
         $('#other_option_addiction').val(addication_text);
      })
      /*======main issues add other option for intake form end======*/
  });
</script>

<!-- ZAPIER EXPERT'S CODE END -->
</html>
