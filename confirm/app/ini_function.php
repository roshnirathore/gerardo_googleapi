<?php

class cita{
    function hiden_terms($id_cita){
        $sql = "SELECT * FROM cita WHERE idcita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['hiden_terms']=$this->validar_usuario_registrado($fila['idpaciente']);
            $co['estado'] = true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function validar_usuario_registrado($id_paciente){
        $sql = "SELECT * FROM clients_information WHERE idpaciente=$id_paciente";
        $res = mysqli_query($this->conexion,$sql);
        if($res->num_rows>=1){
            return true;
        }
        else{
            return false;
        }
    }

    function validar_estado_cita($id_cita,$estado, $phpmail){
        $id_estado=$estado;
        $sql = "SELECT * FROM cita WHERE idcita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            if($fila['idestado_cita']!=1){
                $co['estado'] = true;
                $co['info']     = "-1";
            }else{
                if($this->update_estado_cita($id_cita,$id_estado)){
                    $co=$this->select_client_info($fila['idpaciente'],$id_estado);
                }else{
                    $co['estado'] = false;
                }
                 
                $co=$this->select_client_info($fila['idpaciente'],$id_estado);

                    $sql = "SELECT * FROM v_citas WHERE idcita=$id_cita";
                    $this->conexion();
                    $res = mysqli_query($this->conexion,$sql);
                    while ($calendar_data=mysqli_fetch_array($res)) {
                        $start_date = $calendar_data['fecha_ini'];
                        $end_date = $calendar_data['fecha_fin'];
                        $status = $calendar_data['idestado_cita'];
                        $name = $calendar_data['medico'];
                        $openTokLink = "";
                        if(!empty($calendar_data['open_tok_session']))
                            $openTokLink = "https://cleverclinic.app/public/?sessionId=".$calendar_data['open_tok_session']."&id=".$id_cita;
                    }

                    if($status == 2){

                        $this->calendar_event($co['mail'],$start_date,$end_date,$name,$openTokLink,$phpmail);
                    }
            }
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function get_pdf($id_paciente){

        $sql = "SELECT idusuario FROM paciente WHERE idpaciente=$id_paciente";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        $fila=mysqli_fetch_array($res);
        $user_id = $fila["idusuario"];

        $sql_user = "SELECT editable_pdf FROM medico WHERE idusuario=$user_id";
        $res_user = mysqli_query($this->conexion,$sql_user);
        $fila_user=mysqli_fetch_array($res_user);
        $pdf_name = $fila_user["editable_pdf"];
        mysqli_close($this->conexion);
        return $pdf_name;
    }


    function get_terms($id_cita){
        $sql = "SELECT * FROM cita WHERE idcita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $id_therapist=$fila['idmedico'];
        }
        $co['terms_conditions'] = $this->get_terms2($id_therapist);
        $co['id_therapist'] = $id_therapist;
        $co['estado'] = true;
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function get_terms2($id_therapist){
        $sql = "SELECT * FROM medico WHERE idmedico=$id_therapist";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            return $fila['terms_conditions'];
        }
    }
    function select_client_info($id_paciente,$id_estado){
        $sql = "SELECT * FROM v_paciente WHERE idpaciente =$id_paciente";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['name'] = utf8_encode($fila['nombre']);
            $co['date'] = utf8_encode($fila['fecha_nacimiento']);
            $co['dir'] = utf8_encode($fila['direccion']);
            $co['tel'] = utf8_encode($fila['telefono']);
            $co['ciudad'] = utf8_encode($fila['ciudad']);
            $co['id_ciudad'] = $fila['idciudad'];
            $co['idpaciente'] = $fila['idpaciente'];
            $co['mail'] = utf8_encode($fila['email']);
            $co['fregistro'] = utf8_encode($fila['fecha_registro']);
            $co['info']     = $id_estado;
            $co['estado'] = true;
            $co['registrado'] = $this->validar_usuario_registrado($id_paciente);
        }
        $co["get_pdf"] = $this->get_pdf($id_paciente);
        if(isset($co)){return $co;}
    }

    function guardar_datos_cliente($date,$have_electronic_file,$clients_name,$idpaciente,$childs_name,$date_birth,$date_birth_of_child, $address,$idcity,$postal_code,$phone_number,$email,$clients_substitute_decision_maker,$gender,$relationship,$living_circumstances,$education,$employment,$benefits_to_cover_session,$insurances_name,$amount_they_cover_per_year,$referral,$referral_information,$referral_phone_number,$referral_address,$family_physician,$family_member,$emergency_contact,$receive_encrypted_mail_or_text,$how_do_you_know_about,$tipo,$tmp_name='',$type=''){

      /*function guardar_datos_cliente($info){
        $sql = "INSERT INTO clients_information VALUE(0,'$info[0]','$info[1]',$info[3],'$info[2]','$info[4]','$info[5]','$info[6]','$info[7]','$info[8]','$info[9]','$info[10]','$info[11]','$info[12]','$info[13]','$info[14]','$info[15]','$info[16]','$info[17]','$info[18]','$info[19]',$info[20],'$info[21]','$info[22]','$info[23]','$info[24]','$info[25]','$info[26]','$info[27]','$info[28]','$info[29]')";*/
        if(!empty($type)){
           $type = explode("/",$type);
            $name_file = uniqid().".".end($type);
            move_uploaded_file($tmp_name,"../../files/client_editable_pdf/$name_file"); 
        }else{
            $name_file = 'No File';
        }
        

        $sql = "INSERT INTO clients_information VALUE(0,'$date','$have_electronic_file','$idpaciente','$clients_name','$childs_name','$date_birth','$date_birth_of_child','$address','$idcity','$postal_code','$phone_number','$email','$clients_substitute_decision_maker','$gender','$relationship','$living_circumstances','$education','$employment','$benefits_to_cover_session','$insurances_name','".$amount_they_cover_per_year."','$referral','$referral_information','$referral_phone_number','$referral_address','$family_physician','$family_member','$emergency_contact','$receive_encrypted_mail_or_text','$how_do_you_know_about','$name_file')";

        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
            $co['query']=$sql;
            $co['error']=mysqli_error($this->conexion);
        }

        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function update_estado_cita($id_cita,$id_estado){

        $sql = "UPDATE cita SET idestado_cita=$id_estado WHERE idcita=$id_cita";

        if(mysqli_query($this->conexion,$sql)){
            return true;
        }else{

            return false;

        }

    }
     function calendar_event($email,$start_date,$end_date,$name,$openTokLink = "",$phpmail){

        $authEmail = $this->accessToken();
   

        $id = $_POST['user_id'];
 
        // $id = $_SESSION['id_user'];
        $sql = "SELECT calendar_tokan FROM usuario WHERE idusuario='$id'";
    
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            
            $tokenJSON = json_decode($fila[0], true);
            $authEmail = $tokenJSON['authEmail'];
        }
        
        $messege = "";
        if(!empty($openTokLink)){
            $messege = "To start a video conference, please click on this link. \n".$openTokLink;
            
            $phpmail->isSMTP();
            $phpmail->Host = "mail.cleverclinic.app";
            $phpmail->SMTPAuth= true;
            $phpmail->SMTPSecure = 'ssl';
            $phpmail->Username = "support@cleverclinic.app";
            $phpmail->Password = "S4lv4d0r.";
            $phpmail->Port     = 465;
            $phpmail->setFrom("support@cleverclinic.app");
            $phpmail->addAddress($email);

            $phpmail->CharSet = "UTF-8";
            $phpmail->Subject = "Clever Clinic Video Conference Link"; 
           // $phpmail->Body     = $openTokLink;
            $phpmail->Body     = 'To start a video conference, please click on this link :  <a href="'.$openTokLink.'" >Begin Our Session</a>';
            $phpmail->IsHTML(true);
            if($phpmail->send()){
                // return true;
                $status = 'true';
            }else{
                //return false;
                $status = 'false';
            }
        }
        
        $ch = curl_init();

        $arr = explode(' ', $start_date);
        $start_date_new =  $arr[0]."T".$arr[1];
        $arr1 = explode(' ', $end_date);
        $end_date_new =  $arr1[0]."T".$arr1[1];

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/calendar/v3/calendars/'.$authEmail.'/events?maxAttendees=1&sendNotifications=true&key=AIzaSyDVMnc5wmn1YUor6EOy5WvvFiMSg_WFGSw');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
          'summary' =>$name,
          'description' =>$messege,
          'start' => array(
            'dateTime' =>$start_date_new,
           'timeZone' => 'America/Toronto',
          ),
          'end' => array(
            'dateTime' =>$end_date_new,
            //'timeZone' => 'Asia/Kolkata',
            'timeZone' => 'America/Toronto',
          ),
           'recurrence' => array(
                            'RRULE:FREQ=DAILY;COUNT=1'
                           ),
            'attendees' => array(
              array('email' => $email,
                  'responseStatus' => 'needsAction'),
            ),
             'reminders'=> array(
                'useDefault' => false,
                'overrides' => array(
                array('method' => 'email', 'minutes' => 60),
                array('method' => 'email', 'minutes' => 48 * 60),
              ),
            ),

        )));
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$this->accessToken();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         $result = curl_exec($ch);
         
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    function accessToken() {
         session_start();
         $id = $_POST['user_id'];
      
        // $id = $_SESSION['id_user'];
         $sql = "SELECT calendar_tokan FROM usuario WHERE idusuario='$id'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
       
        while ($fila=mysqli_fetch_array($res)) {
            $tokenJSON = json_decode($fila[0], true);
           
           if ($fila[0]){
              $access_token = $tokenJSON['access_token'];
            

                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.googleapis.com/oauth2/v4/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "refresh_token=".$access_token."&client_id=853700852482-290ea0qpvm3qr0g7p872stafseb755cn.apps.googleusercontent.com&client_secret=HdzBAPWBp3Yv9bgBwqoaJo_q&grant_type=refresh_token",
                CURLOPT_HTTPHEADER => array(
                       "Cache-Control: no-cache",
                       "Content-Type: application/x-www-form-urlencoded",
                       "Postman-Token: 57d46e6f-d560-4931-a8be-b9dcc1e9698c",
                      ),
                    ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                return $access_token = json_decode($response)->access_token;
            }
            
        }
    }

    protected $conexion;

    function conexion(){

        $user = "salvador_root";
        $pass = "S4lv4d0r.";
        $server = "localhost";
        $db 	= "salvador_haab";
        

        $this->conexion = new mysqli($server,$user,$pass,$db);

        if(!$this->conexion)var_dump("error al conectar a la base de datos");

        

        

    }

    function dessemcript($cadena,$key){

        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

        return $decrypted;  //Devuelve el string desencriptado

    }

}


class reset{
    
    protected $conexion;

    function conexion(){
        $user = "salvador_root";
        $pass = "S4lv4d0r.";
        $server = "localhost";
        $db 	= "salvador_haab";
        $this->conexion = new mysqli($server,$user,$pass,$db);
        if(!$this->conexion)var_dump("error al conectar a la base de datos");
    }

    function reset_password($reset_token,$password){
        $id=$this->get_id($reset_token);
        if($id>0){
            $new_token=md5($id).uniqid();
            $sql = "UPDATE usuario SET reset_token='$new_token', password='$password' WHERE reset_token='$reset_token'";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
        }
        else{
            $co['estado']=false;
        }
        if(isset($co)){return $co;}
    }

    function get_id($reset_token){
        $sql = "SELECT idusuario FROM usuario WHERE reset_token='$reset_token'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        if($res->num_rows==1){
            while($fila=mysqli_fetch_array($res)){
                $co=$fila['idusuario'];
            }
        }
        else{
            $co=0;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}


?>