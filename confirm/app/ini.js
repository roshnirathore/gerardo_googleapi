confirm = {

    url: "app/ini_case.php",

    ini: function () {
        
        $this = this;
        $("#acept_terms").addClass('disabled');
        $("#confirm_form").hide();
        $('#terms_acept').change(function () {
            if (this.checked) {
                $("#acept_terms").removeClass('disabled');
            } else {
                $("#acept_terms").addClass('disabled');
            }
        });
        $this.get_terms();
        $this.hiden_terms();
    },
    hiden_terms: function (){
      
        let params = new URLSearchParams(location.search);

        let data = {
            tipo: 6,
            id: params.get('id_cita')
        }

        $.post($this.url, data, function (json) {
            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }
            if (!json.estado) {
                return false;
            }
            if(json.hiden_terms){

                $("#terms").hide();
                $("#terms_acept_checked").hide();
                $("#confirm_form").show();
                $this.validar_cita();
                $this.set_data_paciente();
            }
            else{
                $("#acept_terms").click(function () {
                    $("#terms").hide();
                    $("#terms_acept_checked").hide();
                    $("#confirm_form").show();
                    $this.validar_cita();
                    $this.set_data_paciente();
                });
            }
        }, "json");
    },
    get_terms: function () {
     
        let params = new URLSearchParams(location.search);

        let data = {
            tipo: 5,
            id: params.get('id_cita')
        }

        $.post($this.url, data, function (json) {

            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }

            if (!json.estado) {
                return false;
            }

            $("#terms").html(json.terms_conditions);

        }, "json");
    },
    validar_cita: function () {
       
        let params = new URLSearchParams(location.search);
     
        let data = {
            tipo: 1,
            id: params.get('id_cita'),
            estado: params.get("estado"),
            user_id: params.get("user_id") 
        }

        $.post($this.url, data, function (json) {
        
            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }

            if (!json.estado) {
                return false;
            }

            switch (parseInt(json.info)) {

                case 2:
             
                    $("#title").html("Accepted").addClass("green-text");
                    if (json.registrado) {
                        let str = `Hello <b>${json.name}</b>, thank you for confirming your appointment.`;
                        $("#subtitle").html(str);
                        $("#userValidationForm").hide();
                        $("#idpaciente").val(json.idpaciente);
                        $("#get_pdf").attr("href", "http://cleverclinic.app/files/therapist_pdf/"+json.get_pdf);
                    } else {
                        let str = `Hello <b>${json.name}</b>, thank you for confirming your appointment.
                        Please review your information and click on accept.`;
                        $("#subtitle").html(str);
                        $("#idpaciente").val(json.idpaciente);
                        $('.clients_name').val(json.name);
                        $('#date_birth').val(json.date);//
                        $('#address_clients_name').val(json.dir);
                        $('#city').val(json.ciudad);
                        $('#clients_phone_number').val(json.tel);
                        $('#email').val(json.mail);
                       // $('#get_pdf').val("http://cleverclinic.app/files/therapist_pdf/"+json.get_pdf);
                        $("#get_pdf").attr("href", "http://cleverclinic.app/files/therapist_pdf/"+json.get_pdf);
                       
                        let birth = json.date.split("-");
                        $('#year-date_birth option[value="'+birth[0]+'"]').attr('selected',true);
                        $('#month-date_birth option[value="'+birth[1]+'"]').attr('selected',true);
                        $('#day-date_birth option[value="'+birth[2]+'"]').attr('selected',true);
                        $(window).scrollTop(0);
                    }
                    break;

                case 4:

                    $("#title").html("Rejected").addClass("red-text");

                    $("#subtitle").html("This appointment has been rejected");

                    break;

                case -1:

                    $("#title").html("Not Available").addClass("blue-text");

                    $("#subtitle").html("This appointment has been previously verified");
                    $("#userValidationForm").hide();
                    break;

            }

        }, "json");

    },
    set_data_paciente: function () {
       
        $('#date').val($('#year-date').val()+"-"+$('#month-date').val()+"-"+$('#day-date').val());
        $('#year-date, #month-date, #day-date').change(function(){
            $('#date').val($('#year-date').val()+"-"+$('#month-date').val()+"-"+$('#day-date').val());
        });

        $('#date_birth').val($('#year-date_birth').val()+"-"+$('#month-date_birth').val()+"-"+$('#day-date_birth').val());
        $('#year-date_birth, #month-date_birth, #day-date_birth').change(function(){
            $('#date_birth').val($('#year-date_birth').val()+"-"+$('#month-date_birth').val()+"-"+$('#day-date_birth').val());
        });

        $('#date_birth_of_child').val($('#year-date_birth_of_child').val()+"-"+$('#month-date_birth_of_child').val()+"-"+$('#day-date_birth_of_child').val());
        $('#year-date_birth_of_child, #month-date_birth_of_child, #day-date_birth_of_child').change(function(){
            $('#date_birth_of_child').val($('#year-date_birth_of_child').val()+"-"+$('#month-date_birth_of_child').val()+"-"+$('#day-date_birth_of_child').val());
        });

        $("#form_edit_client").submit(function (e) {
            e.preventDefault();
            
           /* let data = {
                tipo: 2,
                info: $("#form_edit_client").serializeArray()
                //info:  new FormData(this)
            };

            $.post($this.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $("#title").html("Tank You!").addClass("green-text");
                    let str = `This appointment has been confirmed.`;
                    $("#subtitle").html(str);
                    $("#userValidationForm").hide();
                } else {
                    alert("ERROR");
                }
            }, "json");*/
            
            var data = new FormData();
            //Form data
            var form_data = $('#form_edit_client').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });


            //File data
            /*if($('input[name="upload_editable_pdf"]')[0].files){
                var file_data = $('input[name="upload_editable_pdf"]')[0].files;
                for (var i = 0; i < file_data.length; i++) {
                    data.append("upload_editable_pdf[]", file_data[i]);
                } 
            }*/
           

            data.append('tipo', 2);

            $.ajax({
                url: $this.url,
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: 'POST',
                success: function(json) {
                    var res  =  JSON.parse(json);
                    if (typeof (res) == 'string') {
                    console.log(res);
                    return false;
                    }
                    if (res.estado) {
                        $("#title").html("Tank You!").addClass("green-text");
                        let str = `This appointment has been confirmed.`;
                        $("#subtitle").html(str);
                        $("#userValidationForm").hide();
                    } else {
                        alert("ERROR");
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        });
    }

};

reset = {
    url: "app/ini_case.php",
    ini: function () {
        $this = this;
        $this.validate_token()
        $this.set_validate_password();
    },
    set_validate_password: function () {
        $('#form_newpassword').unbind("submit").on("submit", function (e) {
            e.preventDefault();
            if ($('#newpassword').val() === $('#validatenewpassword').val()) {
                $('#validatenewpassword').removeClass('is-invalid');
                $this.reset_passowrd();
            } else {
                $('#validatenewpassword').addClass('is-invalid');
            }
        });
    },
    reset_passowrd: function () {
        let params = new URLSearchParams(location.search);
        let data = {
            tipo: 3,
            reset_token: params.get('reset_token'),
            password: $('#newpassword').val()
        }
        $.post($this.url, data, function (json) {
            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }
            if (json.estado) {
                alert('The password has been successful.');
                document.location.href = "/";
                return true;
            } else {
                return false;
            }
        }, "json");
    },
    validate_token: function () {
        let params = new URLSearchParams(location.search);
        let data = {
            tipo: 4,
            reset_token: params.get('reset_token')
        }
        $.post($this.url, data, function (json) {
            if (parseInt(json) > 0) {
                $('#form_newpassword').show();
                $('#message_error').hide();
                return true;
            } else {
                $this.show_error_message();
            }
        }, "json");
    },
    show_error_message: function () {
        $('#form_newpassword').hide();
        $('#message_error').show();
    }
};

(function () {
    confirm.ini();
})();