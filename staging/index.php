<!DOCTYPE html>
<html lang="en">
 
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <link rel="icon" href="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/icon.png">
  <title>Clever clinic</title> 
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="assets/css/mdb.min.css" rel="stylesheet">
  <link href="assets/css/addons/datatables.min.css" rel="stylesheet">


    <link href='assets/fullcalendar-4.2.0/packages/core/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/daygrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/core/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/bootstrap/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/timegrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/daygrid/main.css' rel='stylesheet' />
    <link href='assets/fullcalendar-4.2.0/packages/list/main.css' rel='stylesheet' />
    <script src='assets/fullcalendar-4.2.0/packages/core/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/interaction/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/bootstrap/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/daygrid/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/timegrid/main.js?asdf'></script>
    <script src='assets/fullcalendar-4.2.0/packages/list/main.js?asdf'></script>

  <!-- Your custom styles (optional) -->
  <link href="assets/css/style.css" rel="stylesheet">

  <link href="https://rawgit.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet" />
  <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js?asdf"></script>
  <script src="https://rawgit.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script> 
  
  <style type="text/css">
    .multiple-tag-history {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-history {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .multiple-tag-info {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-info {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .multiple-tag-metho {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-metho {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .select-box{
    width: 702px; 
    display: inline-block;
  }
  .multiple-add-btn{
    padding: 8px 9px;
    border: 1px solid #ccc;
    background-color: #52bb52;
    color: #fff;
    cursor: pointer; 
    border-radius: 4px; 
    margin-top: 15px;
    display: inline-block;
    width: auto;
  }

  /*====== Soap Form css start=============*/
  .multiple-tag-history-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-history-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .multiple-tag-treatment-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-treatment-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
  .multiple-tag-homework-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-homework-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
 /*====== Soap Form css end=============*/

 /*====== Soap full version Form css start=============*/
 .multiple-tag-medical-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-medical-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }

  .multiple-tag-current-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-current-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }

  .multiple-tag-intervention-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-intervention-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }

  .multiple-tag-individual-homework-soap {
    border: 1px solid #ddd;
    margin-top: -1px; /* Prevent double borders */
    background-color: #f6f6f6;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    color: black;
    display: block;
    position: relative;
  }

  .close-individual-homework-soap {
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0%;
    padding: 12px 16px;
    transform: translate(0%, -50%);
  }
 /*====== Soap full version Form css end=============*/

  </style>
</head>

<!-- <body onload="ini.ini();"> -->
<body onload="ini.ini();">
    <div class="d-flex" id="wrapper">
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <div class="sidebar-heading">
                <img class="mx-auto d-block" style="width: 7em;height: 100%;" src="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/cleverc-8.png">
            </div>
            <div class="list-group list-group-flush" id="menu_inicial2">
                
               
                
            </div>
        </div>
        
        <!-- Sidebar -->
        <div class="navbar-dark border-right white-text main_navbar d-none" id="sidebar-wrapper">
            <div class="sidebar-heading">
                <img class="mx-auto d-block" style="width: 7em;height: 100%;" src="<?php echo "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>imagenes/cleverc-8.png">
            </div>
            <div class="list-group list-group-flush" id="menu_inicial">
                
               
                
            </div>
        </div>
        <!-- /#sidebar-wrapper -->
    
        <!-- Page Content -->
        <div id="page-content-wrapper">
    
           <nav class="navbar navbar-expand-lg navbar-dark main_navbar d-none">
                <!-- Navbar brand -->
                <span class="d-block d-md-none" style="color:#4B70C4;cursor:pointer" onclick="openNav()">&#9776;</span>
                <a class="navbar-brand" style="color:#4B70C4; font-weight:400" href="#">MEDICAL</a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuLink-4"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                                style="color:#4B70C4;">
                                <i class="fas fa-user"></i> Profile </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                                <a class="dropdown-item waves-effect waves-light" style="color:#4B70C4;" onclick="login.logout();">Log out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
    
            <div class="container-fluid p-0 p-2" id="main_view">

            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    
    
    
</body>
<!-- SCRIPTS -->
<!-- JQuery -->
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="assets/js/popper.min.js?asdf"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="assets/js/bootstrap.min.js?asdf"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="assets/js/mdb.min.js?asdf"></script>
<script type="text/javascript" src="assets/js/addons/datatables.min.js?asdf"></script>
<script type="text/javascript" src="assets/js/addons/typeahead.js?asdf"></script>
<!-- Full Calendar -->
<script src="assets/fullcalendar-4.2.0/packages/core/main.js?asdf"></script>
<script src='assets/js/addons/moment.js?asdf'></script>
<script type="text/javascript" src="app/ini.js"></script>
<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
<script type="text/javascript">
  $(document).ready(function(){ 
    
      $( 'body' ).on( "click", '#select_individual_options', function() {
        var soap_class_name = $('#individual_soap_autocomplete').attr('class');
        var intake_class_name = $('#individual_intake_session').attr('class');
        var full_version_class_name = $('#individual_soap_full_version').attr('class');

        if(intake_class_name == 'row mt-3'){
          $('#clients_health_history').editableSelect({ filter: false });
          $('#clients_background_information').editableSelect({ filter: false }); 
          $('#metho_collect_information').editableSelect({ filter: false });
        }

        if(soap_class_name == 'row mt-3'){
          $('#individual_client_history_copping_mechanism').editableSelect({ filter: false });
          $('#individual_soap_autocomplete_treatment_and_interventions').editableSelect({ filter: false });
          $('#individual_homework_follow_up_next_session').editableSelect({ filter: false });
        }

        if(full_version_class_name == 'row mt-3' ){
          $('#clients_medical_history').editableSelect({ filter: false });
          $('#clients_current_medications').editableSelect({ filter: false });
          $('#individual_intervention').editableSelect({ filter: false });
          $('#individual_soap_homework').editableSelect({ filter: false });
        }
      });
        /*=========== Intake Form start =======================*/
         //First select box
      $( 'body' ).on( "click", '#clients_health_history_btn', function() {
        var multiselect = $('#clients_health_history').val();
         $('#clients_health_history_div').append('<p class="multiple-tag-history">'+multiselect+' || <span class="close-history"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
       $( 'body' ).on('click','.close-history',function(){
         $(this).parents('.multiple-tag-history').remove();
      });

      //Second Select box
      $( 'body' ).on( "click", '#clients_background_information_btn', function() {
        var multiselect = $('#clients_background_information').val();
        $('#clients_background_information_div').append('<p class="multiple-tag-info">'+multiselect+' || <span class="close-info"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>');
      });
      $( 'body' ).on('click','.close-info',function(){
         $(this).parents('.multiple-tag-info').remove();
      });
       //Third Select box
      $( 'body' ).on( "click", '#metho_collect_information_btn', function() {
        var multiselect = $('#metho_collect_information').val();
        $('#metho_collect_information_div').append('<p class="multiple-tag-metho">'+multiselect+' || <span class="close-metho"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>');
      });
      $( 'body' ).on('click','.close-metho',function(){
         $(this).parents('.multiple-tag-metho').remove();
      });
      /*=========== Intake Form end =======================*/

      /*==================== Soap Autocomplete Start =========================*/
      //First Select box
      $( 'body' ).on( "click", '#individual_client_history_copping_mechanism_btn', function() {
        var multiselect = $('#individual_client_history_copping_mechanism').val();
         $('#individual_client_history_copping_mechanism_div').append('<p class="multiple-tag-history-soap">'+multiselect+' || <span class="close-history-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-history-soap',function(){
         $(this).parents('.multiple-tag-history-soap').remove();
      });
      //Second Select box
      $( 'body' ).on( "click", '#individual_soap_autocomplete_treatment_and_interventions_btn', function() {
        var multiselect = $('#individual_soap_autocomplete_treatment_and_interventions').val();
         $('#individual_soap_autocomplete_treatment_and_interventions_div').append('<p class="multiple-tag-treatment-soap">'+multiselect+' || <span class="close-treatment-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-treatment-soap',function(){
         $(this).parents('.multiple-tag-treatment-soap').remove();
      });
      //third select box
      $( 'body' ).on( "click", '#individual_homework_follow_up_next_session_btn', function() {
        var multiselect = $('#individual_homework_follow_up_next_session').val();
         $('#individual_homework_follow_up_next_session_div').append('<p class="multiple-tag-homework-soap">'+multiselect+' || <span class="close-homework-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-homework-soap',function(){
         $(this).parents('.multiple-tag-homework-soap').remove();
      });
      /*==================== Soap Autocomplete End =========================*/

      /*==================== Soap Full VERSION Start =========================*/
      //First select box
      $( 'body' ).on( "click", '#clients_medical_history_btn', function() {
        var multiselect = $('#clients_medical_history').val();
         $('#clients_medical_history_div').append('<p class="multiple-tag-medical-soap">'+multiselect+' || <span class="close-medical-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-medical-soap',function(){
         $(this).parents('.multiple-tag-medical-soap').remove();
      });
      //Second select box
       $( 'body' ).on( "click", '#clients_current_medications_btn', function() {
        var multiselect = $('#clients_current_medications').val();
         $('#clients_current_medications_div').append('<p class="multiple-tag-current-soap">'+multiselect+' || <span class="close-current-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-current-soap',function(){
         $(this).parents('.multiple-tag-current-soap').remove();
      });
       //Third select box
       $( 'body' ).on( "click", '#individual_intervention_btn', function() {
        var multiselect = $('#individual_intervention').val();
         $('#individual_intervention_div').append('<p class="multiple-tag-intervention-soap">'+multiselect+' || <span class="close-intervention-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-intervention-soap',function(){
         $(this).parents('.multiple-tag-intervention-soap').remove();
      });
       //Fourth select box
       $( 'body' ).on( "click", '#individual_soap_homework_btn', function() {
        var multiselect = $('#individual_soap_homework').val();
         $('#individual_soap_homework_div').append('<p class="multiple-tag-individual-homework-soap">'+multiselect+' || <span class="close-individual-homework-soap"><img width="20px" src="https://cleverclinic.app/assets/img/cross.png"</span></p>'); 
      });
      $( 'body' ).on('click','.close-individual-homework-soap',function(){
         $(this).parents('.multiple-tag-individual-homework-soap').remove();
      });
      /*==================== Soap Full VERSION end =========================*/

       $('body').on('click','#intak_sub',function() {
         var soap_class_name = $('#individual_soap_autocomplete').attr('class');
         var intake_class_name = $('#individual_intake_session').attr('class');
         var full_version_class_name = $('#individual_soap_full_version').attr('class');

        /*=========== Intake Form start =======================*/
        //first div blank
         
        if(intake_class_name == 'row mt-3' && soap_class_name == 'row mt-3 d-none' && full_version_class_name == 'row mt-3 d-none'){
          $('#clients_health_history').val('');
          var history_div = $('#clients_health_history_div').text();
          $('#clients_health_history').val(history_div);

          //second div blank
          $('#clients_background_information').val('');
          var info_div = $('#clients_background_information_div').text();
          $('#clients_background_information').val(info_div);

          //third div blank
          $('#metho_collect_information').val('');
          var metho_div = $('#metho_collect_information_div').text();
          $('#metho_collect_information').val(metho_div);
         }
        /*=========== Intake Form end =======================*/
        /*=============================================================*/
        /*=========== soap Autocompleted Form start =======================*/
        //first div blank
        if(intake_class_name == 'row mt-3 d-none' && soap_class_name == 'row mt-3' && full_version_class_name == 'row mt-3 d-none'){
          $('#individual_client_history_copping_mechanism').val('');
          var history_div_soap = $('#individual_client_history_copping_mechanism_div').text();
          $('#individual_client_history_copping_mechanism').val(history_div_soap);

          //Second div blank
          $('#individual_soap_autocomplete_treatment_and_interventions').val('');
          var treatment_div_soap = $('#individual_soap_autocomplete_treatment_and_interventions_div').text();
          $('#individual_soap_autocomplete_treatment_and_interventions').val(treatment_div_soap);

          //Third div blank
          $('#individual_homework_follow_up_next_session').val('');
          var homework_div_soap = $('#individual_homework_follow_up_next_session_div').text();
          $('#individual_homework_follow_up_next_session').val(homework_div_soap);
        }
        /*=========== soap Autocompleted Form End =======================*/

        /*=========== soap Full Version Form start =======================*/
        if(intake_class_name == 'row mt-3 d-none' && soap_class_name == 'row mt-3 d-none' && full_version_class_name == 'row mt-3'){
          //First div blank
          $('#clients_medical_history').val('');
          var history_div_soap = $('#clients_medical_history_div').text();
          $('#clients_medical_history').val(history_div_soap);

          //Second div blank
          $('#clients_current_medications').val('');
          var treatment_div_soap = $('#clients_current_medications_div').text();
          $('#clients_current_medications').val(treatment_div_soap);

          //Third div blank
          $('#individual_intervention').val('');
          var intervention_div_soap = $('#individual_intervention_div').text();
          $('#individual_intervention').val(intervention_div_soap);

          //Fourth div blank
          $('#individual_soap_homework').val('');
          var homework_div_soap = $('#individual_soap_homework_div').text();
          $('#individual_soap_homework').val(homework_div_soap);
        }
         /*=========== soap Full Version Form end =======================*/
      });

  });
</script>


</html>
