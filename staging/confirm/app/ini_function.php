<?php

class cita

{

    function validar_estado_cita($id_cita,$estado){

        // $id_cita = str_replace(" ","+",$id_cita);

        // $estado  = str_replace(" ","+",$estado);

        // $id_cita = $this->dessemcript($id_cita,"cita");

        // $id_estado = $this->dessemcript($estado,"estado");
        $id_estado=$estado;

        $sql = "SELECT * FROM cita WHERE idcita=$id_cita";

        $this->conexion();

        $res = mysqli_query($this->conexion,$sql);

        while ($fila=mysqli_fetch_array($res)) {
            if($fila['idestado_cita']!=1){
                $co=$this->select_client_info($fila['idpaciente'],$id_estado);

                $co['estado'] = true;

                $co['info']     = "-1";
                

            }else{

                if($this->update_estado_cita($id_cita,$id_estado)){
                    $co=$this->select_client_info($fila['idpaciente'],$id_estado);
                    //$this->calendar_event();
                }else{

                    $co['estado'] = false;

                }

                $co=$this->select_client_info($fila['idpaciente'],$id_estado);

                $sql = "SELECT * FROM v_citas WHERE idcita=$id_cita";
                $this->conexion();
                $res = mysqli_query($this->conexion,$sql);
                while ($calendar_data=mysqli_fetch_array($res)) {
                    $start_date = $calendar_data['fecha_ini'];
                    $end_date = $calendar_data['fecha_fin'];
                    $status = $calendar_data['idestado_cita'];
                    $name = $calendar_data['medico'];
                    $openTokLink = "https://cleverclinic.app/staging/public/?sessionId=".$calendar_data['open_tok_session']."&id=".$id_cita;
                }
                if($status == 2){
                $this->calendar_event($co['mail'],$start_date,$end_date,$name,$openTokLink);
                }
                
            }
        }

        mysqli_close($this->conexion);

        if(isset($co)){return $co;}

    }

    function select_client_info($id_paciente,$id_estado){
        $sql = "SELECT * FROM v_paciente WHERE idpaciente =$id_paciente";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['name'] = utf8_encode($fila['nombre']);
            $co['date'] = utf8_encode($fila['fecha_nacimiento']);
            $co['dir'] = utf8_encode($fila['direccion']);
            $co['tel'] = utf8_encode($fila['telefono']);
            $co['ciudad'] = utf8_encode($fila['ciudad']);
            $co['id_ciudad'] = $fila['idciudad'];
            $co['idpaciente'] = $fila['idpaciente'];
            $co['mail'] = utf8_encode($fila['email']);
            $co['fregistro'] = utf8_encode($fila['fecha_registro']);
            $co['info']     = $id_estado;
            $co['estado'] = true;
        }
        if(isset($co)){return $co;}
    }

    function guardar_datos_cliente($idpaciente,$name,$date,$address,$city,$phone){
        
        $sql = "UPDATE paciente SET nombre='$name', fecha_nacimiento='$date', direccion='$address', telefono='$phone' WHERE idpaciente='$idpaciente'";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function update_estado_cita($id_cita,$id_estado){

        $sql = "UPDATE cita SET idestado_cita=$id_estado WHERE idcita=$id_cita";

        if(mysqli_query($this->conexion,$sql)){
            return true;
        }else{

            return false;

        }

    }
     function calendar_event($email,$start_date,$end_date,$name, $openTokLink = ""){
        $ch = curl_init();

        $arr = explode(' ', $start_date);
        $start_date_new =  $arr[0]."T".$arr[1];
        $arr1 = explode(' ', $end_date);
        $end_date_new =  $arr1[0]."T".$arr1[1];

        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/calendar/v3/calendars/futuretechdotcodotin%40gmail.com/events?maxAttendees=1&sendNotifications=true&key=AIzaSyDVMnc5wmn1YUor6EOy5WvvFiMSg_WFGSw');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
          'summary' => $name,
          'description' => "To start a video call, please click on this link. ".$openTokLink,
          'start' => array(
            'dateTime' =>$start_date_new,
            'timeZone' => 'Asia/Kolkata',
          ),
          'end' => array(
            'dateTime' =>$end_date_new,
            'timeZone' => 'Asia/Kolkata',
          ),
           'recurrence' => array(
                            'RRULE:FREQ=DAILY;COUNT=1'
                           ),
            'attendees' => array(
              array('email' => 'roshni@vkaps.com',
                  'responseStatus' => 'needsAction'),
            ),
             'reminders'=> array(
                'useDefault' => false,
                'overrides' => array(
                array('method' => 'email', 'minutes' => 60),
                array('method' => 'email', 'minutes' => 48 * 60),
              ),
            ),

        )));
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = 'Authorization: Bearer '.$this->accessToken();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        var_dump($result);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    function accessToken() {
         session_start();
         $id = $_SESSION['id_user'];
        $sql = "SELECT calendar_tokan FROM usuario WHERE idusuario='$id'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $tokenJSON = json_decode($fila[0], true);
           if ($fila[0]){
              $access_token = $tokenJSON['access_token'];
            

                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.googleapis.com/oauth2/v4/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "refresh_token=".$access_token."&client_id=853700852482-290ea0qpvm3qr0g7p872stafseb755cn.apps.googleusercontent.com&client_secret=HdzBAPWBp3Yv9bgBwqoaJo_q&grant_type=refresh_token",
                CURLOPT_HTTPHEADER => array(
                       "Cache-Control: no-cache",
                       "Content-Type: application/x-www-form-urlencoded",
                       "Postman-Token: 57d46e6f-d560-4931-a8be-b9dcc1e9698c",
                      ),
                    ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                return $access_token = json_decode($response)->access_token;
            }
            
        }
    }

    protected $conexion;

    function conexion(){

        $user = "salvador_root";
        $pass = "S4lv4d0r.";
        $server = "localhost";
        $db 	= "salvador_haab";
        

        $this->conexion = new mysqli($server,$user,$pass,$db);

        if(!$this->conexion)var_dump("error al conectar a la base de datos");

        

        

    }

    function dessemcript($cadena,$key){

        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

        return $decrypted;  //Devuelve el string desencriptado

    }

}


class reset{
    
    protected $conexion;

    function conexion(){
        $user = "salvador_root";
        $pass = "S4lv4d0r.";
        $server = "localhost";
        $db 	= "salvador_haab";
        $this->conexion = new mysqli($server,$user,$pass,$db);
        if(!$this->conexion)var_dump("error al conectar a la base de datos");
    }

    function reset_password($reset_token,$password){
        $id=$this->get_id($reset_token);
        if($id>0){
            $new_token=md5($id).uniqid();
            $sql = "UPDATE usuario SET reset_token='$new_token', password='$password' WHERE reset_token='$reset_token'";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
        }
        else{
            $co['estado']=false;
        }
        if(isset($co)){return $co;}
    }

    function get_id($reset_token){
        $sql = "SELECT idusuario FROM usuario WHERE reset_token='$reset_token'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        if($res->num_rows==1){
            while($fila=mysqli_fetch_array($res)){
                $co=$fila['idusuario'];
            }
        }
        else{
            $co=0;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}


?>