confirm = {

    url: "app/ini_case.php",

    ini: function () {

        $this = this;

        $this.validar_cita();
        $this.set_data_paciente();
    },
    validar_cita: function () {
        let params = new URLSearchParams(location.search);

        let data = {
            tipo: 1,
            id: params.get('id_cita'),
            estado: params.get("estado")
        }

        $.post($this.url, data, function (json) {

            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }

            if (!json.estado) {
                return false;
            }

            switch (parseInt(json.info)) {

                case 2:

                    $("#title").html("Accepted").addClass("green-text");
                    let str = `Hello <b>${json.name}</b>, thank you for confirming your appointment.
                    Please review your information and click on accept.`;
                    $("#subtitle").html(str);
                    // $("#subtitle").html("This appointment has been acepted");
                    $("#confirm_edit_aptient_name").val(json.name);
                    $("#confirm_edit_aptient_date").val(json.date);
                    $("#confirm_edit_aptient_address").val(json.dir);
                    $("#confirm_edit_aptient_city").val(json.ciudad);
                    $("#confirm_edit_aptient_phone").val(json.tel);
                    $("#confirm_edit_aptient_email").val(json.mail);
                    $("#idpaciente").val(json.idpaciente);
                    console.log(json);
                    break;

                case 4:

                    $("#title").html("Rejected").addClass("red-text");

                    $("#subtitle").html("This appointment has been rejected");

                    break;

                case -1:

                    $("#title").html("Not Available").addClass("blue-text");

                    $("#subtitle").html("This appointment has been previously verified");
                    $("#userValidationForm").hide();
                    break;

            }

        }, "json");

    },
    set_data_paciente: function () {
        $("#form_edit_client").submit(function (e) {
            e.preventDefault();
            let data = {
                tipo: 2,
                info: $("#form_edit_client").serializeArray()
            };
            $.post($this.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $("#title").html("Tank You!").addClass("green-text");
                    let str = `This appointment has been confirmed.`;
                    $("#subtitle").html(str);
                    $("#userValidationForm").hide();
                } else {
                    alert("ERROR");
                }
            }, "json");
        });
    }

};

reset = {
    url: "app/ini_case.php",
    ini: function () {
        $this = this;
        $this.validate_token()
        $this.set_validate_password();
    },
    set_validate_password: function(){
        $('#form_newpassword').unbind("submit").on("submit", function (e) {
            e.preventDefault();
            if($('#newpassword').val()===$('#validatenewpassword').val()){
                $('#validatenewpassword').removeClass('is-invalid');
                $this.reset_passowrd();
            }
            else{
                $('#validatenewpassword').addClass('is-invalid');
            }
        });
    },
    reset_passowrd: function(){
        let params = new URLSearchParams(location.search);
        let data = {
            tipo: 3,
            reset_token: params.get('reset_token'),
            password: $('#newpassword').val()
        }
        $.post($this.url, data, function (json) {
            if (typeof (json) == 'string') {
                console.log(json);
                return false;
            }
            if (json.estado) {
                alert('The password has been successful.');
                document.location.href="/";
                return true;
            }
            else{
                return false;
            }
        }, "json");
    },
    validate_token: function(){
        let params = new URLSearchParams(location.search);
        let data = {
            tipo: 4,
            reset_token: params.get('reset_token')
        }
        $.post($this.url, data, function (json) {
            if (parseInt(json)>0) {
                $('#form_newpassword').show();
                $('#message_error').hide();
                return true;
            }
            else{
                $this.show_error_message();
            }
        }, "json");
    },
    show_error_message: function(){
        $('#form_newpassword').hide();
        $('#message_error').show();
    }
};

(function () {
    $("#acept_terms").addClass('disabled');
    $("#confirm_form").hide();
    $('#terms_acept').change(function(){
        if(this.checked){
            $("#acept_terms").removeClass('disabled');
        }else{
            $("#acept_terms").addClass('disabled');
        }
    });
    $("#acept_terms").click(function(){
        $("#terms").hide();
        $("#confirm_form").show();
        confirm.ini();
    });
})();