(function () {
    if ('Navigator' == navigator.appName) document.forms[0].reset();
    var $id_historial_paciente = null;
    var invoice_data = {};
    var $id_historial;
    var $id_medico_ref = null;
    var $id_tipo_paciente = null;
    var $id_medico_edit_ref = null;
    var $id_cliente_selected = null;
    var $id_city_ref = null;
    var type_session = "";

    $('#mySidenav').bind('clickoutside', function (event) {
        document.getElementById("mySidenav").style.width = "0";
    });

    medicos = {
        ini: function () {
            $this = this;
            $this.t_table_medicos = $("#t_table_medicos").DataTable({
                    "order": [
                        [1, "asc"]
                    ]
                }),
                $this.get_datos_medicos();
            $this.setevents();

        },
        get_lista_de_usuarios: function () {
            let data = {
                tipo: 5,
                view: "medicos",
                id_sucursal: $("#select_sucursales").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_usuarios = json;
                let $array_temp = [];
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });

                $("#form_modal_medico_usuario").typeahead('destroy');
                $('#form_modal_medico_usuario').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#form_modal_medico_usuario").val("");
                            $id_medico_ref = 0;
                            $("#modal_form_new_medico").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_medico_ref = parseInt(e.value);
                            $("#modal_form_new_medico").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });
                $("#form_modal_edit_medico_usuario").typeahead('destroy');
                $('#form_modal_edit_medico_usuario').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#form_modal_edit_medico_usuario").val("");
                            $id_medico_edit_ref = 0;
                            $("#modal_form_edit_medico").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_medico_edit_ref = parseInt(e.value);
                            $("#modal_form_edit_medico").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });
            }, "json");
        },
        update_data_medico: function () {
            var fd = new FormData(document.getElementById("modal_form_edit_medico"));
            fd.append("view", "medicos");
            fd.append("tipo", 4);
            fd.append("id_medico", $this.id_medico.value);
            fd.append("id_usuario_ref", $id_medico_edit_ref);
            $.ajax({
                url: ini.url,
                type: "POST",
                data: fd,
                processData: false, // tell jQuery not to process the data
                contentType: false,
                dataType: "json",
                success: function (json) {
                    if (typeof (json) == "string") {
                        console.log(json);
                        return false;
                    }
                    if (json.estado == true) {
                        $this.get_lista_medicos();
                        ini.modal.close();
                    }
                } // tell jQuery not to set contentType
            });
            /* let data={tipo:4,view:"medicos",info:$("#modal_form_edit_medico").serializeArray(),id_medico:$this.id_medico.value,id_usuario_ref:$id_medico_edit_ref}
             $.post(ini.url, data, function (json) {
             if(typeof(json)=='string'){console.log(json); return false;}
                 if(json.estado){$this.get_lista_medicos(); ini.modal.close();}
             },'json');*/

        },
        editar_medico: function (id) {
            $this.id_medico = {};
            $this.id_medico.value = id;
            Object.freeze($this.id_medico);
            $id_medico_edit_ref = $this.lista_de_medicos[id].iduser;
            $("#form_modal_edit_medico_usuario").val($this.lista_de_usuarios[$this.lista_de_medicos[id].iduser].name);
            $("#form_modal_edit_medico_name").val($this.lista_de_medicos[id].nombre);
            $("#form_modal_edit_medico_bussines_name").val($this.lista_de_medicos[id].puesto);
            $("#form_modal_edit_medico_trecord").val($this.lista_de_medicos[id].record);
            $("#form_modal_edit_medico_serie").val($this.lista_de_medicos[id].serie);
            $("#form_modal_edit_medico_folio").val($this.lista_de_medicos[id].folio);
            $("#checkinEdit").val($this.lista_de_medicos[id].horaEntrada);
            $("#checkoutEdit").val($this.lista_de_medicos[id].horaSalida);
            if ($this.lista_de_medicos[id].image == "") {
                $("#form_modal_edit_medico_image").attr("src", "../files/personal_logo/default.png");
            } else {
                $("#form_modal_edit_medico_image").attr("src", "../files/personal_logo/" + $this.lista_de_medicos[id].image);
            }
            ini.modal.show("modal_edit_medico", $this.update_data_medico, "#modal_form_edit_medico");
        },
        add_medico: function () {
            var fd = new FormData(document.getElementById("modal_form_new_medico"));
            fd.append("view", "medicos");
            fd.append("tipo", 3);
            fd.append("id_sucursal", $("#select_sucursales").val());
            fd.append("id_usuario_ref", $id_medico_ref);
            $.ajax({
                url: ini.url,
                type: "POST",
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (json) {
                    if (json.estado == true) {
                        $this.get_lista_medicos();
                        ini.modal.close();
                    }
                }
            });
        },
        modal_new_medico: function () {
            ini.modal.show("modal_new_medico", $this.add_medico, "#modal_form_new_medico");
            $("#form_modal_medico_image").val("../files/personal_logo/default.png");
            $('#selectSchedule').hide();
        },
        get_datos_medicos: function () {
            data = {
                tipo: 2,
                view: "medicos"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $.each(json.sucursales, function (index, value) {
                    if (!sessionStorage.getItem("medicos_sucursal")) {
                        sessionStorage.setItem("medicos_sucursal", index);
                    }
                    $("#select_sucursales").append("<option value='" + index + "'>" + value + "</option>");
                });
            }, "json").done(function () {
                $("#select_sucursales").val(sessionStorage.getItem("medicos_sucursal"));
                $this.get_lista_medicos();
            });
        },
        get_lista_medicos: function () {
            let data = {
                tipo: 1,
                view: "medicos",
                id_sucursal: sessionStorage.getItem("medicos_sucursal")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_medicos = json;
                $this.t_table_medicos.clear().draw();
                let cont = 0;
                $.each(json, function (index, value) {
                    $this.t_table_medicos.row.add([++cont, value.nombre, value.puesto, value.record, value.horaEntrada, value.horaSalida, "<div class='btn btn-primary btn-small m-0 px-3 py-1 float-right' onclick='medicos.editar_medico(" + index + ")'><i class='fas fa-edit'></i> Edit</div>"]);
                });
                $this.t_table_medicos.draw();
            }, "json").done(function () {
                $this.get_lista_de_usuarios();
            });
        },
        setevents: function () {
            $('#defaultschedule').change(function () {
                if ($('#defaultschedule').is(':checked')) {
                    $('#selectSchedule').hide();
                    $('#checkin').val("08:00");
                    $('#checkout').val("22:00");
                } else {
                    $('#selectSchedule').show();
                }
            });
            $('#checkin').change(function () {
                if (moment($('#checkin').val(), "hh:mm A").isBefore(moment("08:00", "hh:mm A"))) {
                    $('#checkin').val("08:00");
                }
                if (moment($('#checkin').val(), "hh:mm A").isAfter(moment("21:00", "hh:mm A"))) {
                    $('#checkin').val("21:00");
                }
            });
            $('#checkout').change(function () {
                if (moment($('#checkout').val(), "hh:mm A").isBefore(moment("09:00", "hh:mm A"))) {
                    $('#checkout').val("09:00");
                }
                if (moment($('#checkout').val(), "hh:mm A").isAfter(moment("22:00", "hh:mm A"))) {
                    $('#checkout').val("22:00");
                }
            });

            $('#defaultscheduleEdit').change(function () {
                if ($('#defaultscheduleEdit').is(':checked')) {
                    $('#selectScheduleEdit').hide();
                    $('#checkinEdit').val("08:00");
                    $('#checkoutEdit').val("22:00");
                } else {
                    $('#selectScheduleEdit').show();
                }
            });
            $('#checkinEdit').change(function () {
                if (moment($('#checkinEdit').val(), "hh:mm A").isBefore(moment("08:00", "hh:mm A"))) {
                    $('#checkinEdit').val("08:00");
                }
                if (moment($('#checkinEdit').val(), "hh:mm A").isAfter(moment("21:00", "hh:mm A"))) {
                    $('#checkinEdit').val("21:00");
                }
            });
            $('#checkoutEdit').change(function () {
                if (moment($('#checkoutEdit').val(), "hh:mm A").isBefore(moment("09:00", "hh:mm A"))) {
                    $('#checkoutEdit').val("09:00");
                }
                if (moment($('#checkoutEdit').val(), "hh:mm A").isAfter(moment("22:00", "hh:mm A"))) {
                    $('#checkoutEdit').val("22:00");
                }
            });
            $("#select_sucursales").unbind("change").on("change", function () {
                sessionStorage.setItem("medicos_sucursal", $(this).val());
                $this.get_lista_medicos();
            });
            $("[key-change-picture]").unbind("click").on("click", function () {
                $("#change_pickture_input").click();
            });
            $("[key-new-picture]").unbind("click").on("click", function () {
                $("#new_pickture_input").click();
            });

            $("#change_pickture_input").unbind("change").on("change", function () {
                $this.preview_img($(this)[0]);
            });
            $("#new_pickture_input").unbind("change").on("change", function () {
                $this.preview_img_new_medic($(this)[0]);
            });
        },
        preview_img_new_medic: function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#form_modal_medico_image").attr("src", e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        preview_img: function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#form_modal_edit_medico_image").attr("src", e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    }
    pacientes = {
        ini: function () {
            $this = this;
            $this.t_pacientes = $('#t_pacientes').DataTable();
            $this.t_paciente = $('#t_paciente').DataTable();
            $this.t_cliente = $('#t_cliente').DataTable();
            $this.t_lista_citas = $("#t_lista_citas").DataTable({
                "order": [
                    [1, "desc"]
                ]
            });
            $this.countryOption = "";
            $this.setevents();
            $this.get_inputs();
            $this.get_lista_pacientes();
            $this.get_lista_clientes();
            $this.countryCode = [{"name":"Afghanistan (‫افغانستان‬‎)","iso2":"af","dialCode":"93","priority":0,"areaCodes":null},{"name":"Albania (Shqipëri)","iso2":"al","dialCode":"355","priority":0,"areaCodes":null},{"name":"Algeria (‫الجزائر‬‎)","iso2":"dz","dialCode":"213","priority":0,"areaCodes":null},{"name":"American Samoa","iso2":"as","dialCode":"1","priority":0,"areaCodes":null},{"name":"Andorra","iso2":"ad","dialCode":"376","priority":5,"areaCodes":["684"]},{"name":"Angola","iso2":"ao","dialCode":"244","priority":0,"areaCodes":null},{"name":"Anguilla","iso2":"ai","dialCode":"1","priority":0,"areaCodes":null},{"name":"Antigua and Barbuda","iso2":"ag","dialCode":"1","priority":6,"areaCodes":["264"]},{"name":"Argentina","iso2":"ar","dialCode":"54","priority":7,"areaCodes":["268"]},{"name":"Armenia (Հայաստան)","iso2":"am","dialCode":"374","priority":0,"areaCodes":null},{"name":"Aruba","iso2":"aw","dialCode":"297","priority":0,"areaCodes":null},{"name":"Australia","iso2":"au","dialCode":"61","priority":0,"areaCodes":null},{"name":"Austria (Österreich)","iso2":"at","dialCode":"43","priority":0,"areaCodes":null},{"name":"Azerbaijan (Azərbaycan)","iso2":"az","dialCode":"994","priority":0,"areaCodes":null},{"name":"Bahamas","iso2":"bs","dialCode":"1","priority":0,"areaCodes":null},{"name":"Bahrain (‫البحرين‬‎)","iso2":"bh","dialCode":"973","priority":8,"areaCodes":["242"]},{"name":"Bangladesh (বাংলাদেশ)","iso2":"bd","dialCode":"880","priority":0,"areaCodes":null},{"name":"Barbados","iso2":"bb","dialCode":"1","priority":0,"areaCodes":null},{"name":"Belarus (Беларусь)","iso2":"by","dialCode":"375","priority":9,"areaCodes":["246"]},{"name":"Belgium (België)","iso2":"be","dialCode":"32","priority":0,"areaCodes":null},{"name":"Belize","iso2":"bz","dialCode":"501","priority":0,"areaCodes":null},{"name":"Benin (Bénin)","iso2":"bj","dialCode":"229","priority":0,"areaCodes":null},{"name":"Bermuda","iso2":"bm","dialCode":"1","priority":0,"areaCodes":null},{"name":"Bhutan (འབྲུག)","iso2":"bt","dialCode":"975","priority":10,"areaCodes":["441"]},{"name":"Bolivia","iso2":"bo","dialCode":"591","priority":0,"areaCodes":null},{"name":"Bosnia and Herzegovina (Босна и Херцеговина)","iso2":"ba","dialCode":"387","priority":0,"areaCodes":null},{"name":"Botswana","iso2":"bw","dialCode":"267","priority":0,"areaCodes":null},{"name":"Brazil (Brasil)","iso2":"br","dialCode":"55","priority":0,"areaCodes":null},{"name":"British Indian Ocean Territory","iso2":"io","dialCode":"246","priority":0,"areaCodes":null},{"name":"British Virgin Islands","iso2":"vg","dialCode":"1","priority":0,"areaCodes":null},{"name":"Brunei","iso2":"bn","dialCode":"673","priority":11,"areaCodes":["284"]},{"name":"Bulgaria (България)","iso2":"bg","dialCode":"359","priority":0,"areaCodes":null},{"name":"Burkina Faso","iso2":"bf","dialCode":"226","priority":0,"areaCodes":null},{"name":"Burundi (Uburundi)","iso2":"bi","dialCode":"257","priority":0,"areaCodes":null},{"name":"Cambodia (កម្ពុជា)","iso2":"kh","dialCode":"855","priority":0,"areaCodes":null},{"name":"Cameroon (Cameroun)","iso2":"cm","dialCode":"237","priority":0,"areaCodes":null},{"name":"Canada","iso2":"ca","dialCode":"1","priority":0,"areaCodes":null},{"name":"Cape Verde (Kabu Verdi)","iso2":"cv","dialCode":"238","priority":1,"areaCodes":["204","226","236","249","250","289","306","343","365","387","403","416","418","431","437","438","450","506","514","519","548","579","581","587","604","613","639","647","672","705","709","742","778","780","782","807","819","825","867","873","902","905"]},{"name":"Caribbean Netherlands","iso2":"bq","dialCode":"599","priority":0,"areaCodes":null},{"name":"Cayman Islands","iso2":"ky","dialCode":"1","priority":1,"areaCodes":["3","4","7"]},{"name":"Central African Republic (République centrafricaine)","iso2":"cf","dialCode":"236","priority":12,"areaCodes":["345"]},{"name":"Chad (Tchad)","iso2":"td","dialCode":"235","priority":0,"areaCodes":null},{"name":"Chile","iso2":"cl","dialCode":"56","priority":0,"areaCodes":null},{"name":"China (中国)","iso2":"cn","dialCode":"86","priority":0,"areaCodes":null},{"name":"Christmas Island","iso2":"cx","dialCode":"61","priority":0,"areaCodes":null},{"name":"Cocos (Keeling) Islands","iso2":"cc","dialCode":"61","priority":2,"areaCodes":null},{"name":"Colombia","iso2":"co","dialCode":"57","priority":1,"areaCodes":null},{"name":"Comoros (‫جزر القمر‬‎)","iso2":"km","dialCode":"269","priority":0,"areaCodes":null},{"name":"Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)","iso2":"cd","dialCode":"243","priority":0,"areaCodes":null},{"name":"Congo (Republic) (Congo-Brazzaville)","iso2":"cg","dialCode":"242","priority":0,"areaCodes":null},{"name":"Cook Islands","iso2":"ck","dialCode":"682","priority":0,"areaCodes":null},{"name":"Costa Rica","iso2":"cr","dialCode":"506","priority":0,"areaCodes":null},{"name":"Côte d’Ivoire","iso2":"ci","dialCode":"225","priority":0,"areaCodes":null},{"name":"Croatia (Hrvatska)","iso2":"hr","dialCode":"385","priority":0,"areaCodes":null},{"name":"Cuba","iso2":"cu","dialCode":"53","priority":0,"areaCodes":null},{"name":"Curaçao","iso2":"cw","dialCode":"599","priority":0,"areaCodes":null},{"name":"Cyprus (Κύπρος)","iso2":"cy","dialCode":"357","priority":0,"areaCodes":null},{"name":"Czech Republic (Česká republika)","iso2":"cz","dialCode":"420","priority":0,"areaCodes":null},{"name":"Denmark (Danmark)","iso2":"dk","dialCode":"45","priority":0,"areaCodes":null},{"name":"Djibouti","iso2":"dj","dialCode":"253","priority":0,"areaCodes":null},{"name":"Dominica","iso2":"dm","dialCode":"1","priority":0,"areaCodes":null},{"name":"Dominican Republic (República Dominicana)","iso2":"do","dialCode":"1","priority":13,"areaCodes":["767"]},{"name":"Ecuador","iso2":"ec","dialCode":"593","priority":2,"areaCodes":["809","829","849"]},{"name":"Egypt (‫مصر‬‎)","iso2":"eg","dialCode":"20","priority":0,"areaCodes":null},{"name":"El Salvador","iso2":"sv","dialCode":"503","priority":0,"areaCodes":null},{"name":"Equatorial Guinea (Guinea Ecuatorial)","iso2":"gq","dialCode":"240","priority":0,"areaCodes":null},{"name":"Eritrea","iso2":"er","dialCode":"291","priority":0,"areaCodes":null},{"name":"Estonia (Eesti)","iso2":"ee","dialCode":"372","priority":0,"areaCodes":null},{"name":"Ethiopia","iso2":"et","dialCode":"251","priority":0,"areaCodes":null},{"name":"Falkland Islands (Islas Malvinas)","iso2":"fk","dialCode":"500","priority":0,"areaCodes":null},{"name":"Faroe Islands (Føroyar)","iso2":"fo","dialCode":"298","priority":0,"areaCodes":null},{"name":"Fiji","iso2":"fj","dialCode":"679","priority":0,"areaCodes":null},{"name":"Finland (Suomi)","iso2":"fi","dialCode":"358","priority":0,"areaCodes":null},{"name":"France","iso2":"fr","dialCode":"33","priority":0,"areaCodes":null},{"name":"French Guiana (Guyane française)","iso2":"gf","dialCode":"594","priority":0,"areaCodes":null},{"name":"French Polynesia (Polynésie française)","iso2":"pf","dialCode":"689","priority":0,"areaCodes":null},{"name":"Gabon","iso2":"ga","dialCode":"241","priority":0,"areaCodes":null},{"name":"Gambia","iso2":"gm","dialCode":"220","priority":0,"areaCodes":null},{"name":"Georgia (საქართველო)","iso2":"ge","dialCode":"995","priority":0,"areaCodes":null},{"name":"Germany (Deutschland)","iso2":"de","dialCode":"49","priority":0,"areaCodes":null},{"name":"Ghana (Gaana)","iso2":"gh","dialCode":"233","priority":0,"areaCodes":null},{"name":"Gibraltar","iso2":"gi","dialCode":"350","priority":0,"areaCodes":null},{"name":"Greece (Ελλάδα)","iso2":"gr","dialCode":"30","priority":0,"areaCodes":null},{"name":"Greenland (Kalaallit Nunaat)","iso2":"gl","dialCode":"299","priority":0,"areaCodes":null},{"name":"Grenada","iso2":"gd","dialCode":"1","priority":0,"areaCodes":null},{"name":"Guadeloupe","iso2":"gp","dialCode":"590","priority":14,"areaCodes":["473"]},{"name":"Guam","iso2":"gu","dialCode":"1","priority":0,"areaCodes":null},{"name":"Guatemala","iso2":"gt","dialCode":"502","priority":15,"areaCodes":["671"]},{"name":"Guernsey","iso2":"gg","dialCode":"44","priority":0,"areaCodes":null},{"name":"Guinea (Guinée)","iso2":"gn","dialCode":"224","priority":1,"areaCodes":["1481","7781","7839","7911"]},{"name":"Guinea-Bissau (Guiné Bissau)","iso2":"gw","dialCode":"245","priority":0,"areaCodes":null},{"name":"Guyana","iso2":"gy","dialCode":"592","priority":0,"areaCodes":null},{"name":"Haiti","iso2":"ht","dialCode":"509","priority":0,"areaCodes":null},{"name":"Honduras","iso2":"hn","dialCode":"504","priority":0,"areaCodes":null},{"name":"Hong Kong (香港)","iso2":"hk","dialCode":"852","priority":0,"areaCodes":null},{"name":"Hungary (Magyarország)","iso2":"hu","dialCode":"36","priority":0,"areaCodes":null},{"name":"Iceland (Ísland)","iso2":"is","dialCode":"354","priority":0,"areaCodes":null},{"name":"India (भारत)","iso2":"in","dialCode":"91","priority":0,"areaCodes":null},{"name":"Indonesia","iso2":"id","dialCode":"62","priority":0,"areaCodes":null},{"name":"Iran (‫ایران‬‎)","iso2":"ir","dialCode":"98","priority":0,"areaCodes":null},{"name":"Iraq (‫العراق‬‎)","iso2":"iq","dialCode":"964","priority":0,"areaCodes":null},{"name":"Ireland","iso2":"ie","dialCode":"353","priority":0,"areaCodes":null},{"name":"Isle of Man","iso2":"im","dialCode":"44","priority":0,"areaCodes":null},{"name":"Israel (‫ישראל‬‎)","iso2":"il","dialCode":"972","priority":2,"areaCodes":["1624","74576","7524","7924","7624"]},{"name":"Italy (Italia)","iso2":"it","dialCode":"39","priority":0,"areaCodes":null},{"name":"Jamaica","iso2":"jm","dialCode":"1","priority":0,"areaCodes":null},{"name":"Japan (日本)","iso2":"jp","dialCode":"81","priority":4,"areaCodes":["876","658"]},{"name":"Jersey","iso2":"je","dialCode":"44","priority":0,"areaCodes":null},{"name":"Jordan (‫الأردن‬‎)","iso2":"jo","dialCode":"962","priority":3,"areaCodes":["1534","7509","7700","7797","7829","7937"]},{"name":"Kazakhstan (Казахстан)","iso2":"kz","dialCode":"7","priority":0,"areaCodes":null},{"name":"Kenya","iso2":"ke","dialCode":"254","priority":1,"areaCodes":["33","7"]},{"name":"Kiribati","iso2":"ki","dialCode":"686","priority":0,"areaCodes":null},{"name":"Kosovo","iso2":"xk","dialCode":"383","priority":0,"areaCodes":null},{"name":"Kuwait (‫الكويت‬‎)","iso2":"kw","dialCode":"965","priority":0,"areaCodes":null},{"name":"Kyrgyzstan (Кыргызстан)","iso2":"kg","dialCode":"996","priority":0,"areaCodes":null},{"name":"Laos (ລາວ)","iso2":"la","dialCode":"856","priority":0,"areaCodes":null},{"name":"Latvia (Latvija)","iso2":"lv","dialCode":"371","priority":0,"areaCodes":null},{"name":"Lebanon (‫لبنان‬‎)","iso2":"lb","dialCode":"961","priority":0,"areaCodes":null},{"name":"Lesotho","iso2":"ls","dialCode":"266","priority":0,"areaCodes":null},{"name":"Liberia","iso2":"lr","dialCode":"231","priority":0,"areaCodes":null},{"name":"Libya (‫ليبيا‬‎)","iso2":"ly","dialCode":"218","priority":0,"areaCodes":null},{"name":"Liechtenstein","iso2":"li","dialCode":"423","priority":0,"areaCodes":null},{"name":"Lithuania (Lietuva)","iso2":"lt","dialCode":"370","priority":0,"areaCodes":null},{"name":"Luxembourg","iso2":"lu","dialCode":"352","priority":0,"areaCodes":null},{"name":"Macau (澳門)","iso2":"mo","dialCode":"853","priority":0,"areaCodes":null},{"name":"Macedonia (FYROM) (Македонија)","iso2":"mk","dialCode":"389","priority":0,"areaCodes":null},{"name":"Madagascar (Madagasikara)","iso2":"mg","dialCode":"261","priority":0,"areaCodes":null},{"name":"Malawi","iso2":"mw","dialCode":"265","priority":0,"areaCodes":null},{"name":"Malaysia","iso2":"my","dialCode":"60","priority":0,"areaCodes":null},{"name":"Maldives","iso2":"mv","dialCode":"960","priority":0,"areaCodes":null},{"name":"Mali","iso2":"ml","dialCode":"223","priority":0,"areaCodes":null},{"name":"Malta","iso2":"mt","dialCode":"356","priority":0,"areaCodes":null},{"name":"Marshall Islands","iso2":"mh","dialCode":"692","priority":0,"areaCodes":null},{"name":"Martinique","iso2":"mq","dialCode":"596","priority":0,"areaCodes":null},{"name":"Mauritania (‫موريتانيا‬‎)","iso2":"mr","dialCode":"222","priority":0,"areaCodes":null},{"name":"Mauritius (Moris)","iso2":"mu","dialCode":"230","priority":0,"areaCodes":null},{"name":"Mayotte","iso2":"yt","dialCode":"262","priority":0,"areaCodes":null},{"name":"Mexico (México)","iso2":"mx","dialCode":"52","priority":1,"areaCodes":["269","639"]},{"name":"Micronesia","iso2":"fm","dialCode":"691","priority":0,"areaCodes":null},{"name":"Moldova (Republica Moldova)","iso2":"md","dialCode":"373","priority":0,"areaCodes":null},{"name":"Monaco","iso2":"mc","dialCode":"377","priority":0,"areaCodes":null},{"name":"Mongolia (Монгол)","iso2":"mn","dialCode":"976","priority":0,"areaCodes":null},{"name":"Montenegro (Crna Gora)","iso2":"me","dialCode":"382","priority":0,"areaCodes":null},{"name":"Montserrat","iso2":"ms","dialCode":"1","priority":0,"areaCodes":null},{"name":"Morocco (‫المغرب‬‎)","iso2":"ma","dialCode":"212","priority":16,"areaCodes":["664"]},{"name":"Mozambique (Moçambique)","iso2":"mz","dialCode":"258","priority":0,"areaCodes":null},{"name":"Myanmar (Burma) (မြန်မာ)","iso2":"mm","dialCode":"95","priority":0,"areaCodes":null},{"name":"Namibia (Namibië)","iso2":"na","dialCode":"264","priority":0,"areaCodes":null},{"name":"Nauru","iso2":"nr","dialCode":"674","priority":0,"areaCodes":null},{"name":"Nepal (नेपाल)","iso2":"np","dialCode":"977","priority":0,"areaCodes":null},{"name":"Netherlands (Nederland)","iso2":"nl","dialCode":"31","priority":0,"areaCodes":null},{"name":"New Caledonia (Nouvelle-Calédonie)","iso2":"nc","dialCode":"687","priority":0,"areaCodes":null},{"name":"New Zealand","iso2":"nz","dialCode":"64","priority":0,"areaCodes":null},{"name":"Nicaragua","iso2":"ni","dialCode":"505","priority":0,"areaCodes":null},{"name":"Niger (Nijar)","iso2":"ne","dialCode":"227","priority":0,"areaCodes":null},{"name":"Nigeria","iso2":"ng","dialCode":"234","priority":0,"areaCodes":null},{"name":"Niue","iso2":"nu","dialCode":"683","priority":0,"areaCodes":null},{"name":"Norfolk Island","iso2":"nf","dialCode":"672","priority":0,"areaCodes":null},{"name":"North Korea (조선 민주주의 인민 공화국)","iso2":"kp","dialCode":"850","priority":0,"areaCodes":null},{"name":"Northern Mariana Islands","iso2":"mp","dialCode":"1","priority":0,"areaCodes":null},{"name":"Norway (Norge)","iso2":"no","dialCode":"47","priority":17,"areaCodes":["670"]},{"name":"Oman (‫عُمان‬‎)","iso2":"om","dialCode":"968","priority":0,"areaCodes":null},{"name":"Pakistan (‫پاکستان‬‎)","iso2":"pk","dialCode":"92","priority":0,"areaCodes":null},{"name":"Palau","iso2":"pw","dialCode":"680","priority":0,"areaCodes":null},{"name":"Palestine (‫فلسطين‬‎)","iso2":"ps","dialCode":"970","priority":0,"areaCodes":null},{"name":"Panama (Panamá)","iso2":"pa","dialCode":"507","priority":0,"areaCodes":null},{"name":"Papua New Guinea","iso2":"pg","dialCode":"675","priority":0,"areaCodes":null},{"name":"Paraguay","iso2":"py","dialCode":"595","priority":0,"areaCodes":null},{"name":"Peru (Perú)","iso2":"pe","dialCode":"51","priority":0,"areaCodes":null},{"name":"Philippines","iso2":"ph","dialCode":"63","priority":0,"areaCodes":null},{"name":"Poland (Polska)","iso2":"pl","dialCode":"48","priority":0,"areaCodes":null},{"name":"Portugal","iso2":"pt","dialCode":"351","priority":0,"areaCodes":null},{"name":"Puerto Rico","iso2":"pr","dialCode":"1","priority":0,"areaCodes":null},{"name":"Qatar (‫قطر‬‎)","iso2":"qa","dialCode":"974","priority":3,"areaCodes":["787","939"]},{"name":"Réunion (La Réunion)","iso2":"re","dialCode":"262","priority":0,"areaCodes":null},{"name":"Romania (România)","iso2":"ro","dialCode":"40","priority":0,"areaCodes":null},{"name":"Russia (Россия)","iso2":"ru","dialCode":"7","priority":0,"areaCodes":null},{"name":"Rwanda","iso2":"rw","dialCode":"250","priority":0,"areaCodes":null},{"name":"Saint Barthélemy","iso2":"bl","dialCode":"590","priority":0,"areaCodes":null},{"name":"Saint Helena","iso2":"sh","dialCode":"290","priority":1,"areaCodes":null},{"name":"Saint Kitts and Nevis","iso2":"kn","dialCode":"1","priority":0,"areaCodes":null},{"name":"Saint Lucia","iso2":"lc","dialCode":"1","priority":18,"areaCodes":["869"]},{"name":"Saint Martin (Saint-Martin (partie française))","iso2":"mf","dialCode":"590","priority":19,"areaCodes":["758"]},{"name":"Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)","iso2":"pm","dialCode":"508","priority":2,"areaCodes":null},{"name":"Saint Vincent and the Grenadines","iso2":"vc","dialCode":"1","priority":0,"areaCodes":null},{"name":"Samoa","iso2":"ws","dialCode":"685","priority":20,"areaCodes":["784"]},{"name":"San Marino","iso2":"sm","dialCode":"378","priority":0,"areaCodes":null},{"name":"São Tomé and Príncipe (São Tomé e Príncipe)","iso2":"st","dialCode":"239","priority":0,"areaCodes":null},{"name":"Saudi Arabia (‫المملكة العربية السعودية‬‎)","iso2":"sa","dialCode":"966","priority":0,"areaCodes":null},{"name":"Senegal (Sénégal)","iso2":"sn","dialCode":"221","priority":0,"areaCodes":null},{"name":"Serbia (Србија)","iso2":"rs","dialCode":"381","priority":0,"areaCodes":null},{"name":"Seychelles","iso2":"sc","dialCode":"248","priority":0,"areaCodes":null},{"name":"Sierra Leone","iso2":"sl","dialCode":"232","priority":0,"areaCodes":null},{"name":"Singapore","iso2":"sg","dialCode":"65","priority":0,"areaCodes":null},{"name":"Sint Maarten","iso2":"sx","dialCode":"1","priority":0,"areaCodes":null},{"name":"Slovakia (Slovensko)","iso2":"sk","dialCode":"421","priority":21,"areaCodes":["721"]},{"name":"Slovenia (Slovenija)","iso2":"si","dialCode":"386","priority":0,"areaCodes":null},{"name":"Solomon Islands","iso2":"sb","dialCode":"677","priority":0,"areaCodes":null},{"name":"Somalia (Soomaaliya)","iso2":"so","dialCode":"252","priority":0,"areaCodes":null},{"name":"South Africa","iso2":"za","dialCode":"27","priority":0,"areaCodes":null},{"name":"South Korea (대한민국)","iso2":"kr","dialCode":"82","priority":0,"areaCodes":null},{"name":"South Sudan (‫جنوب السودان‬‎)","iso2":"ss","dialCode":"211","priority":0,"areaCodes":null},{"name":"Spain (España)","iso2":"es","dialCode":"34","priority":0,"areaCodes":null},{"name":"Sri Lanka (ශ්‍රී ලංකාව)","iso2":"lk","dialCode":"94","priority":0,"areaCodes":null},{"name":"Sudan (‫السودان‬‎)","iso2":"sd","dialCode":"249","priority":0,"areaCodes":null},{"name":"Suriname","iso2":"sr","dialCode":"597","priority":0,"areaCodes":null},{"name":"Svalbard and Jan Mayen","iso2":"sj","dialCode":"47","priority":0,"areaCodes":null},{"name":"Swaziland","iso2":"sz","dialCode":"268","priority":1,"areaCodes":["79"]},{"name":"Sweden (Sverige)","iso2":"se","dialCode":"46","priority":0,"areaCodes":null},{"name":"Switzerland (Schweiz)","iso2":"ch","dialCode":"41","priority":0,"areaCodes":null},{"name":"Syria (‫سوريا‬‎)","iso2":"sy","dialCode":"963","priority":0,"areaCodes":null},{"name":"Taiwan (台灣)","iso2":"tw","dialCode":"886","priority":0,"areaCodes":null},{"name":"Tajikistan","iso2":"tj","dialCode":"992","priority":0,"areaCodes":null},{"name":"Tanzania","iso2":"tz","dialCode":"255","priority":0,"areaCodes":null},{"name":"Thailand (ไทย)","iso2":"th","dialCode":"66","priority":0,"areaCodes":null},{"name":"Timor-Leste","iso2":"tl","dialCode":"670","priority":0,"areaCodes":null},{"name":"Togo","iso2":"tg","dialCode":"228","priority":0,"areaCodes":null},{"name":"Tokelau","iso2":"tk","dialCode":"690","priority":0,"areaCodes":null},{"name":"Tonga","iso2":"to","dialCode":"676","priority":0,"areaCodes":null},{"name":"Trinidad and Tobago","iso2":"tt","dialCode":"1","priority":0,"areaCodes":null},{"name":"Tunisia (‫تونس‬‎)","iso2":"tn","dialCode":"216","priority":22,"areaCodes":["868"]},{"name":"Turkey (Türkiye)","iso2":"tr","dialCode":"90","priority":0,"areaCodes":null},{"name":"Turkmenistan","iso2":"tm","dialCode":"993","priority":0,"areaCodes":null},{"name":"Turks and Caicos Islands","iso2":"tc","dialCode":"1","priority":0,"areaCodes":null},{"name":"Tuvalu","iso2":"tv","dialCode":"688","priority":23,"areaCodes":["649"]},{"name":"U.S. Virgin Islands","iso2":"vi","dialCode":"1","priority":0,"areaCodes":null},{"name":"Uganda","iso2":"ug","dialCode":"256","priority":24,"areaCodes":["340"]},{"name":"Ukraine (Україна)","iso2":"ua","dialCode":"380","priority":0,"areaCodes":null},{"name":"United Arab Emirates (‫الإمارات العربية المتحدة‬‎)","iso2":"ae","dialCode":"971","priority":0,"areaCodes":null},{"name":"United Kingdom","iso2":"gb","dialCode":"44","priority":0,"areaCodes":null},{"name":"United States","iso2":"us","dialCode":"1","priority":0,"areaCodes":null},{"name":"Uruguay","iso2":"uy","dialCode":"598","priority":0,"areaCodes":null},{"name":"Uzbekistan (Oʻzbekiston)","iso2":"uz","dialCode":"998","priority":0,"areaCodes":null},{"name":"Vanuatu","iso2":"vu","dialCode":"678","priority":0,"areaCodes":null},{"name":"Vatican City (Città del Vaticano)","iso2":"va","dialCode":"39","priority":0,"areaCodes":null},{"name":"Venezuela","iso2":"ve","dialCode":"58","priority":1,"areaCodes":["06698"]},{"name":"Vietnam (Việt Nam)","iso2":"vn","dialCode":"84","priority":0,"areaCodes":null},{"name":"Wallis and Futuna (Wallis-et-Futuna)","iso2":"wf","dialCode":"681","priority":0,"areaCodes":null},{"name":"Western Sahara (‫الصحراء الغربية‬‎)","iso2":"eh","dialCode":"212","priority":0,"areaCodes":null},{"name":"Yemen (‫اليمن‬‎)","iso2":"ye","dialCode":"967","priority":1,"areaCodes":["5288","5289"]},{"name":"Zambia","iso2":"zm","dialCode":"260","priority":0,"areaCodes":null},{"name":"Zimbabwe","iso2":"zw","dialCode":"263","priority":0,"areaCodes":null},{"name":"Åland Islands","iso2":"ax","dialCode":"358","priority":0,"areaCodes":null}];
            /*$.each($this.countryCode, function(i, v){
                $this.countryOption += "<option value=\""+v.dialCode+"\">+"+v.dialCode+" ("+v.name+")</option>";
            });

            $("select[name=modal_new_aptient_phone_code]").html($this.countryOption);*/
        },
        save_new_data_historico: function () {
            let data = {
                tipo: 7,
                view: "pacientes",
                info: $("#form_data_historico").serializeArray(),
                id_historico: $id_historial_paciente
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado == true) {
                    $this.historial_usuario[data.id_historico] = json.historial;
                    $("#form_data_historico_cancel").click();
                }
            }, "json");
        },
        export_file: function (con) {
            let inputs = document.getElementById(con).getElementsByTagName('input');
            let textareas = document.getElementById(con).getElementsByTagName('textarea');
            let selects = document.getElementById(con).getElementsByTagName('select');
            let labels = document.getElementById(con).getElementsByTagName('label');
            const getInputs = (e) => {
                for (const prop in inputs) {
                    if (inputs[prop].name === e && inputs[prop].type !== "file") {
                        return inputs[prop].value;
                    }
                }
                return null;
            };
            const getTextarea = (e) => {
                for (const prop in textareas) {
                    if (textareas[prop].name === e) {
                        return textareas[prop].value;
                    }
                }
                return null;
            };
            const getSelects = (e) => {
                for (const prop in selects) {
                    if (selects[prop].name === e) {
                        return selects[prop].value;
                    }
                }
                return null;
            };
            const getTagName = (e) => {
                for (const prop in labels) {
                    if (labels[prop].htmlFor === e) {
                        return labels[prop].textContent;
                    }
                }
                return null;
            }
            let map = new Map;
            const names = Object.keys($this.historial_to_table);
            map.set("Fecha", $("#historial_fecha").val());
            map.set("Hora", $("#historial_hora").val());
            names.forEach(e => {
                let contenido = getInputs(e);
                if (contenido) {
                    map.set(getTagName(e), contenido);
                }
                contenido = getTextarea(e);
                if (contenido) {
                    map.set(getTagName(e), contenido);
                }
                contenido = getSelects(e);
                if (contenido) {
                    map.set(getTagName(e), contenido);
                }

            });

            columns = JSON.stringify([...map]);

            let data = {
                tipo: 6,
                view: "pacientes",
                datos: columns
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                //window.open("files/patient_file.pdf","_blank");
                var link = document.createElement("a");
                link.download = "patient_file";
                link.href = "files/patient_file.pdf";
                link.click();
            }, "json");
        },

        datos_historial: function (key, con) {
            const table = {
                individual_session_free_version: 'individual_free_version',
                individual_session_handwriting_scan: 'individual_handwriting_scan',
                individual_session_intake: 'individual_intake_session',
                individual_session_soap_autocomplete: 'individual_soap_autocomplete',
                individual_session_soap_full_version: 'individual_soap_full_version',
                individual_session_snapshot: 'individual_snapshot',
                couple_session_clients_information: 'couple_clients_information',
                couple_session_clinical_note_autocomplete: 'couple_clinical_note_autocomplete',
                couple_session_intake: 'couple_intake_session',
                couple_session_free_writing: 'couple_free_writing',
                couple_session_handwriting_scan: 'couple_hand_writing_scan',
                couple_session_soap_full_version: 'couple_soap_full_version',
                couple_session_free_style: 'couple_free_style_couple_session'
            };
            let all_displaynone = () => {
                $('#individual_free_version').addClass('d-none');
                $('#individual_handwriting_scan').addClass('d-none');
                $('#individual_intake_session').addClass('d-none');
                $('#individual_soap_autocomplete').addClass('d-none');
                $('#individual_soap_full_version').addClass('d-none');
                $('#individual_snapshot').addClass('d-none');

                $('#couple_clients_information').addClass('d-none');
                $('#couple_clinical_note_autocomplete').addClass('d-none');
                $('#couple_intake_session').addClass('d-none');
                $('#couple_free_writing').addClass('d-none');
                $('#couple_hand_writing_scan').addClass('d-none');
                $('#couple_soap_full_version').addClass('d-none');
                $('#couple_free_style_couple_session').addClass('d-none');
                type_session = '';
            };
            $links = [];
            $name_file = [];
            let id_form = table[con];
            // console.log(table[con]);
            all_displaynone();
            $("#" + table[con]).removeClass("d-none");
            $("#download_and_export").removeClass("d-none");
            // let tab = Object.keys(table);
            // tab.forEach(function (e) {
            //     if (tab[e] === key) {
            //         console.log(tab[e]);
            //         id_form=tab[e];
            //     }
            // });
            $this.historial_to_table = $this.historial_usuario[key][con];
            $id_historial_paciente = $this.historial_usuario[key][con].idpaciente;
            let name = Object.keys($this.historial_usuario[key][con]);
            $("#editar_datos_historico").removeClass("disabled");
            // let select_forms = document.getElementById(id_form).getElementsByTagName('select');
            let options_session = document.getElementById(id_form).getElementsByTagName('input');
            let interventio = document.getElementsByName('intervention');
            let homework = document.getElementsByName('homework');
            // console.log($(`#individual_intervention`).prop('tagName'));
            name.forEach(function (e) {
                if ($(`[name=${e}]`).prop('tagName') === "SELECT") {
                    for (let prop in homework) {
                        if (homework[prop].tagName === 'SELECT') {
                            $(`select[name=${e}]`).val($this.historial_usuario[key][con][e]);
                        }
                    }
                } else if ($(`input[name=${e}]`).attr('type') !== 'file' && $(`input[name=${e}]`).attr('type') !== 'radio') {
                    $(`input[name=${e}]`).val($this.historial_usuario[key][con][e]);
                    $(`textarea[name=${e}]`).val($this.historial_usuario[key][con][e]);
                    if (e === 'nombre') {
                        $(`input[name=idpaciente]`).val($this.historial_usuario[key][con][e]);
                    } else if (e === 'medico') {
                        $(`input[name=idmedico]`).val($this.historial_usuario[key][con][e]);
                    }
                } else if ($(`input[name=${e}]`).attr('type') === 'radio') {
                    for (let prop in options_session) {
                        if (options_session[prop].type === "radio" && options_session[prop].value === $this.historial_usuario[key][con][e]) {
                            options_session[prop].checked = true;
                        }
                    }
                    for (let prop in interventio) {
                        if (interventio[prop].tagName === 'SELECT') {
                            $(`select[name=${e}]`).val($this.historial_usuario[key][con][e]);
                        }
                    }
                } else {
                    $links.push("files/clinic history/" + $this.historial_usuario[key][con][e]);
                    $name_file.push(e);
                }
            });
            $("#historial_fecha").val(moment($this.historial_usuario[key][con].fecha_ini).format("Y-MM-DD"));
            $("#historial_hora").val(moment($this.historial_usuario[key][con].fecha_ini).format("h:mm A"));
            // $("#file_download").removeClass("disabled").attr("href", "files/clinic history/" + $this.historial_usuario[id].archivo);
            $("#file_export").removeClass("disabled").attr("onclick", "pacientes.export_file('" + table[con] + "')");
            if ($links.length === 0) {
                $('#file_download').addClass('d-none');
            } else {
                $('#file_download').removeClass('d-none');
            }
        },
        ver_historial: function (id) {
            let data = {
                tipo: 4,
                view: "pacientes",
                id_paciente: id
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.historial_usuario = json;
                } else {
                    $this.historial_usuario = null;
                    return false;
                }
            }, "json").done(function () {
                let rows = [];
                const table = {
                    individual_session_free_version: 'individual_free_version',
                    individual_session_handwriting_scan: 'individual_handwriting_scan',
                    individual_session_intake: 'individual_intake_session',
                    individual_session_soap_autocomplete: 'individual_soap_autocomplete',
                    individual_session_soap_full_version: 'individual_soap_full_version',
                    individual_session_snapshot: 'individual_snapshot',
                    couple_session_clients_information: 'couple_clients_information',
                    couple_session_clinical_note_autocomplete: 'couple_clinical_note_autocomplete',
                    couple_session_intake: 'couple_intake_session',
                    couple_session_free_writing: 'couple_free_writing',
                    couple_session_handwriting_scan: 'couple_hand_writing_scan',
                    couple_session_soap_full_version: 'couple_soap_full_version',
                    couple_session_free_style: 'couple_free_style_couple_session'
                };
                // $.each($this.historial_usuario, function (index, value) {
                //     let columns = [];
                //     columns.push(value.nombre);
                //     columns.push(value.fecha_ini);
                //     columns.push("<div class='btn btn-small btn-primary float-right m-0 py-0 px-3' onclick='pacientes.datos_historial(" + index + ")'><i class='fas fa-list'></i></div>");
                //     rows.push(columns);
                // });
                for (const key in $this.historial_usuario) {
                    const info = $this.historial_usuario[key];
                    for (const con in info) {
                        // let name = Object.keys($this.historial_usuario[key][con])
                        let columns = [];
                        let name= table[con];
                        columns.push($this.historial_usuario[key][con].nombre);
                        columns.push($this.historial_usuario[key][con].fecha_ini);
                        columns.push(`<div class='btn btn-small btn-primary float-right m-0 py-0 px-3' onclick='pacientes.datos_historial(${key},"${con}")'><i class='fas fa-list'></i></div>`);
                        rows.push(columns);
                        $("#download_and_export").addClass("d-none");
                        // console.log(name);
                        // name.forEach(function (e) {
                        //     console.log($this.historial_usuario[key][con][e]);
                        // });
                    }
                }
                if (rows.length > 0) {
                    $this.t_lista_citas.clear().rows.add(rows).draw();
                } else {
                    $this.t_lista_citas.clear().draw();
                }
            });
            $(".views_patient").addClass("d-none");
            $("#historial_expedientes").removeClass("d-none");
        },
        show_tabla_pacientes: function () {
            $(".views_patient").addClass("d-none");
            $("#contenedor_lista_pacientes").removeClass("d-none");
        },
        update_data_paciente: function () {
            let data = {
                tipo: 5,
                view: "pacientes",
                info: $("#form_edit_patient").serializeArray(),
                id_paciente: $this.editar_paciente.value,
                id_city: $id_city_ref
            };
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_pacientes();
                    ini.modal.close();
                }
                if (json.registered_telephone) {
                    $('#modal_edit_aptient_phone').addClass('is-invalid');
                    $('#blank_mobile_edit').html('Phone Number already exist.');
                     
                 }
            }, "json");
        },
        update_data_cliente: function () {
            let data = {
                tipo: 9,
                view: "pacientes",
                info: $("#form_edit_cliente").serializeArray(),
                id_paciente: $this.editar_cliente.value,
                id_city: $id_city_ref
            };
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_clientes();
                    ini.modal.close();
                }
            }, "json");
        },
        editar_paciente: function (id_paciente) {
            $("#modal_edit_aptient_name").val($this.lista_pacientes[id_paciente].name);
            $("#modal_edit_aptient_date").val($this.lista_pacientes[id_paciente].date);
            $("#modal_edit_aptient_address").val($this.lista_pacientes[id_paciente].dir);
            $("#modal_edit_aptient_city").val($this.lista_pacientes[id_paciente].ciudad);
            $("#modal_edit_aptient_phone").val($this.lista_pacientes[id_paciente].tel);
            $("#modal_edit_aptient_email").val($this.lista_pacientes[id_paciente].mail);
            $id_city_ref = $this.lista_pacientes[id_paciente].id_ciudad;
            $this.editar_paciente_id = {};
            $this.editar_paciente.value = id_paciente;
            Object.freeze($this.editar_paciente_id);
            ini.modal.show("modal_edit_paciente", $this.update_data_paciente, "#form_edit_patient");
        },
        editar_cliente: function (id_paciente) {
            $("#modal_edit_client_name").val($this.lista_clientes[id_paciente].name);
            $("#modal_edit_client_date").val($this.lista_clientes[id_paciente].date);
            $("#modal_edit_client_address").val($this.lista_clientes[id_paciente].dir);
            $("#modal_edit_client_city").val($this.lista_clientes[id_paciente].ciudad);
            $("#modal_edit_client_phone").val($this.lista_clientes[id_paciente].tel);
            $("#modal_edit_client_email").val($this.lista_clientes[id_paciente].mail);
            $id_city_ref = $this.lista_clientes[id_paciente].id_ciudad;
            $this.editar_cliente.value = id_paciente;
            ini.modal.show("modal_edit_cliente", $this.update_data_cliente, "#form_edit_cliente");
        },
        save_patient: function (info) {
            data = {
                tipo: 2,
                view: "pacientes",
                info: info
            }
            $.post(ini.url, data, function (json) {

                console.log('line 645 '+ JSON.stringify(json));
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_pacientes();
                    $("#form_add_patient")[0].reset();
                    $('#modal_new_aptient_phone').removeClass('is-invalid');
                    ini.modal.close("modal_add_paciente");
                }
                if (json.registered_telephone) {
                    $('#modal_new_aptient_phone').addClass('is-invalid');
                    $('#blank_mobile').html('Phone Number already exist.');
                     
                 }

            }, "json").done(function () {
                $("#form_add_patient").find("[type=submit]").removeAttr("disabled").html("SAVE PATIENT");
                //$("#form_add_patient")[0].reset();
            });
        },
        save_client: function (info) {
            data = {
                tipo: 10,
                view: "pacientes",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_clientes();
                    ini.modal.close("modal_add_cliente");
                }
            }, "json").done(function () {
                $("#form_add_cliente").find("[type=submit]").removeAttr("disabled").html("SAVE CLIENT");
                $("#form_add_cliente")[0].reset();
            });
        },
        setevents: function () {
            $("#form_add_patient").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($(".needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    $info = {
                        name: $("#modal_new_aptient_name").val(),
                        date: $("#modal_new_aptient_date").val(),
                        address: $("#modal_new_aptient_address").val(),
                        city: $id_city_ref,
                        phone: $("#modal_new_aptient_phone").val(),
                        email: $("#modal_new_aptient_email").val(),
                    }
                    $this.save_patient($info);
                }
            });
            $("#form_add_cliente").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($(".needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    $info = {
                        name: $("#modal_new_client_name").val(),
                        date: $("#modal_new_client_date").val(),
                        address: $("#modal_new_client_address").val(),
                        city: $id_city_ref,
                        phone: $("#modal_new_client_phone").val(),
                        email: $("#modal_new_client_email").val(),
                    }
                    $this.save_client($info);
                }
            });
            $("#editar_datos_historico").unbind("click").on("click", function () {
                $("#form_data_historico").find("[type=text],textarea").removeAttr("disabled");
                $("#btn_to_edit").removeClass("d-none");
                $("#file_download").addClass("d-none");
                $("#file_export").addClass("d-none");
                $("#editar_datos_historico").addClass("d-none");
                $("#form_data_historico_save").removeClass("disabled").html(" <i class='fas fa-save'></i> Save");
            });
            $("#form_data_historico_cancel").unbind("click").on("click", function () {
                $this.datos_historial($id_historial_paciente);
                $("#form_data_historico").find("[type=text],textarea").attr("disabled", "disabled");
                $("#btn_to_edit").addClass("d-none");
                $("#file_download").removeClass("d-none");
                $("#file_export").removeClass("d-none");
                $("#editar_datos_historico").removeClass("d-none");
            });
            $("#form_data_historico").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($id_historial_paciente == null) {
                    return false;
                } else {
                    $(this).find("[type=submit]").addClass("disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                    $this.save_new_data_historico();
                }
            });
            // $("#form_data_historico_save").unbind("click").on("click",function(){
            //     if ($id_historial_paciente == null){
            //         return false;
            //     }else{
            //         $(this).addClass("disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
            //         $this.save_new_data_historico();
            //     }
            // })
            $('#file_download').unbind("click").on("click", function () {
                $this.download_files($links, $name_file);
            });
        },
        download_files: function (urls, $name_file) {
            var link = document.createElement('a');

            link.setAttribute('download', null);
            link.style.display = 'none';

            document.body.appendChild(link);

            for (var i = 0; i < urls.length; i++) {
                let ext = urls[i].split(".").pop();
                console.log(urls[i]);
                console.log($name_file[i]);
                link.setAttribute('href', urls[i]);
                link.setAttribute('download', $name_file[i] + "." + ext);
                link.click();
            }

            document.body.removeChild(link);
        },
        get_inputs: function () {
            data = {
                tipo: 3,
                view: "pacientes"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                let html = "";
                // $.each(json.ciudades, function (index, value) {
                //     html += "<option value='" + index + "'>" + value.name + "</option>";
                // });
                // $("#modal_edit_aptient_city").html(html);
                $("#invoice_to_option_sucursal").html(html);
                let $array_temp = [];
                $.each(json.ciudades, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });

                $("#modal_new_aptient_city,#modal_edit_aptient_city,#modal_new_client_city,#modal_edit_client_city").typeahead('destroy');
                $('#modal_new_aptient_city,#modal_edit_aptient_city,#modal_new_client_city,#modal_edit_client_city').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#modal_new_aptient_city,#modal_edit_aptient_city,#modal_new_client_city,#modal_edit_client_city").val("");
                            $id_city_ref = 0;
                            $("#form_add_patient,#form_add_cliente").find(".btn-primary").attr("disabled", "disabled");
                            $("#form_edit_patient,#form_edit_cliente").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_city_ref = parseInt(e.value);
                            $("#form_add_patient,#form_add_cliente").find(".btn-primary").removeAttr("disabled");
                            $("#form_edit_patient,#form_edit_cliente").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });
            }, 'json');
        },
        alta_paciente: function () {
            $("#form_add_patient")[0].reset();
            ini.modal.show("modal_add_paciente");
        },
        alta_cliente: function () {
            $("#form_add_cliente")[0].reset();
            ini.modal.show("modal_add_cliente");
        },
        get_lista_pacientes: function () {
             /* code added by swadhaat 30 jan 2020*/
            var user = 0;
          if (sessionStorage.getItem("login")) {
             user = sessionStorage.getItem("iduser");
            }
            //console.log('login User outside '+ user); 
            let data = {
                tipo: 1,
                view: "pacientes",
                LogedInID: user
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_pacientes = json;
            }, 'json').done(function () {
                $this.llenar_tabla_pacientes();
            });
        },
        get_lista_clientes: function () {
            
            let data = {
                tipo: 8,
                view: "pacientes"
             }
             /* End code added by swadhaat 30 jan 2020*/
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_clientes = json;
                console.log(JSON.stringify($this.lista_clientes));
            }, 'json').done(function () {
                $this.llenar_tabla_clientes();
            });
        },
        llenar_tabla_pacientes: function () {
            let rows = [];
            $.each($this.lista_pacientes, function (index, value) {
                let columns = [];
                columns.push(value.name);
                columns.push(value.date);
                columns.push(value.dir);
                columns.push(value.ciudad);
                columns.push(value.mail);
                columns.push(value.fregistro);
                columns.push("<div class='btn btn-primary btn-sm float-left mr-1 m-0 py-1 px-3' onclick='pacientes.editar_paciente(" + index + ")'><i class='fas fa-edit'></i></div> <div class='btn btn-secondary btn-sm float-left mr-1 m-0 py-1 px-3' onclick='pacientes.ver_historial(" + index + ")'><i class='fas fa-list'></i></div>");
                rows.push(columns);
            });
            $this.t_paciente.clear().rows.add(rows).draw();
        },
        llenar_tabla_clientes: function () {
            let rows = [];
            $.each($this.lista_clientes, function (index, value) {
                let columns = [];
                columns.push(value.name);
                columns.push(value.date);
                columns.push(value.dir);
                columns.push(value.ciudad);
                columns.push(value.mail);
                columns.push(value.fregistro);
                columns.push("<div class='btn btn-primary btn-sm float-left mr-1 m-0 py-1 px-3' onclick='pacientes.editar_cliente(" + index + ")'><i class='fas fa-edit'></i></div>");
                rows.push(columns);
            });
            $this.t_cliente.clear().rows.add(rows).draw();
        },
    }
    citas = {
        calendar: null,
        ini: function () {
            $this = this;
            $('#modal_add_tratment').addClass('d-none');
            $this.inicializar_calendar();
            $this.get_datos_input();
            $this.set_events();
        },
        cancelar_cita: function () {
            if (id_cita != null) {
                let data = {
                    tipo: 8,
                    view: "citas",
                    id_cita: id_cita
                }
                $.post(ini.url, data, function (json) {
                    if (typeof (json) == 'string') {
                        console.log(json);
                        return false;
                    }
                    if (json.estado) {
                        $this.calendar.getEventById(id_cita).remove();
                        $this.calendar.addEvent({
                            id: json.datos_new_cita.id_cita,
                            "title": json.datos_new_cita.nombre,
                            "start": json.datos_new_cita.finicio,
                            "end": json.datos_new_cita.ffinal,
                            "classNames": json.datos_new_cita.classNames,
                            groupId: json.datos_new_cita.estado
                        });
                        ini.modal.close();
                    }
                }, "json");
            }
        },
        save_patient: function (info) {
            data = {
                tipo: 6,
                view: "citas",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_pacientes();
                    $this.cancel_new_user();
                }
            }, "json");
        },
        add_tratment: function () {
            $("#modal_add_tratment").removeClass("d-none");
        },
        cancel_new_user: function () {
            $("#form_add_patient")[0].reset();
            $("#form_add_cita").removeClass("d-none");
            $("#form_add_patient").addClass("d-none");
        },
        add_user: function () {
            $("#form_add_cita").addClass("d-none");
            $("#form_add_patient").removeClass("d-none");
        },
        save_historial_clinico: function () {
            let options_session = document.getElementById(type_session).getElementsByTagName('input');
            let map = new Map;
            let files = [];
            var last_id;
            let exist = true;
            for (let prop in options_session) {
                if (options_session[prop].type === "radio" && options_session[prop].checked === true) {
                    map.set(options_session[prop].name, options_session[prop].value);
                } else if (options_session[prop].type === "file") {
                    if (options_session[prop].files.length !== 0) {
                        let file = new FormData();
                        file.append("view", "citas");
                        file.append("tipo", 9);
                        file.append("table_name", type_session);
                        file.append(options_session[prop].name, options_session[prop].files[0]);
                        files.push(file);
                    } else {
                        exist = false;
                    }
                } else if (options_session[prop].type !== "radio" && options_session[prop].type !== undefined) {
                    map.set(options_session[prop].name, options_session[prop].value);
                }
            }
            options_session = document.getElementById(type_session).getElementsByTagName('textarea');
            for (let prop in options_session) {
                if (options_session[prop].type !== undefined)
                    map.set(options_session[prop].name, options_session[prop].value);
            }
            options_session = document.getElementById(type_session).getElementsByTagName('select');
            for (let prop in options_session) {
                if (options_session[prop].type !== undefined)
                    map.set(options_session[prop].name, options_session[prop].value);
            }
            var fd = new FormData();
            fd.append("view", "citas");
            fd.append("tipo", 7);
            fd.append("id_medico", $("#select_medicos").val());
            fd.append("modal_form_historial_estado", $("#modal_form_historial_estado").val());
            fd.append("modal_form_type_session", $("#modal_form_type_session").val());
            fd.append("id_cita", $this.id_cita);
            fd.append("table_name", type_session);
            let columns = new Map;
            for (var tem of map.keys()) {
                let cap = map.get(tem);
                if (cap !== undefined && tem !== undefined) {
                    columns.set(tem, cap);
                }
            }
            columns = JSON.stringify([...columns]);
            fd.append("columns", columns);
            let updateFiles = (frmd) => {
                frmd.append("last_id", last_id);
                $.ajax({
                    url: ini.url,
                    type: "POST",
                    data: frmd,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (json) {
                        console.log(json);
                        if (json.estado) {
                            // ini.modal.close();
                            $this.get_events_calendar();
                            // $("#modal_form_save_historial")[0].reset();
                            // $("#label_input_file").html("Choose file");
                            $('#modal_form_historial_couple_options option[value=""]').prop('selected', true);
                            $('#message_success').removeClass('d-none');
                        } else {
                            alert(json.error);
                        }
                    }
                });
            };
            if (exist) {
                $.ajax({
                    url: ini.url,
                    type: "POST",
                    data: fd,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (json) {
                        if (json.estado) {
                            last_id = json.last_id;
                            if (files.length) {
                                files.forEach(updateFiles);
                            } else {
                                ini.modal.close();
                                $this.get_events_calendar();
                                $("#modal_form_save_historial")[0].reset();
                                $("#label_input_file").html("Choose file");
                            }
                            let all_displaynone = () => {
                                $('#individual_free_version').addClass('d-none');
                                $('#individual_handwriting_scan').addClass('d-none');
                                $('#individual_intake_session').addClass('d-none');
                                $('#individual_soap_autocomplete').addClass('d-none');
                                $('#individual_soap_full_version').addClass('d-none');
                                $('#individual_snapshot').addClass('d-none');

                                $('#couple_clients_information').addClass('d-none');
                                $('#couple_clinical_note_autocomplete').addClass('d-none');
                                $('#couple_intake_session').addClass('d-none');
                                $('#couple_free_writing').addClass('d-none');
                                $('#couple_hand_writing_scan').addClass('d-none');
                                $('#couple_soap_full_version').addClass('d-none');
                                $('#couple_free_style_couple_session').addClass('d-none');
                                type_session = '';
                            };
                            all_displaynone();
                        } else {
                            alert(json.error);
                            console.log(json.error);
                        }
                    }
                });
            } else {
                alert('Empty file, add a file before saving changes.');
            }
        },
        get_datos_input: function () {
            let data = {
                tipo: 2,
                view: "citas",
                idtipo_usuario: $id_tipo_paciente
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.sucursales = json.sucursales;
                $this.servicios = json.servicio;
                $this.ciudades = json.ciudades;
                $this.llenar_datos_input();
            }, "json").done(function () {
                $this.get_pacientes();
            });
        },
        get_pacientes: function () {
            data = {
                tipo: 3,
                view: "citas"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    $this.pacientes = json.pacientes;
                }
            }, 'json').done(function () {
                $this.llenar_lista_de_pacientes_disponibles();
            });
        },
        llenar_lista_de_pacientes_disponibles: function () {
            let $array_temp = [];
            $.each($this.pacientes, function (index, value) {
                let $mini_array = [];
                $mini_array['id'] = index;
                $mini_array['name'] = value.name;
                $array_temp.push($mini_array);
            });
            $("#alta_cita_paciente").typeahead('destroy');
            $('#alta_cita_paciente').typeahead({
                source: $array_temp,
                display: 'value',
                onSelect: function (e, dato) {
                    if (e.value == "-21") {
                        $("#alta_cita_paciente").val("");
                        $this.temp_user_selected = 0;
                        $("#modal_calendar").find(".btn-primary").attr("disabled", "disabled");
                    } else {
                        $this.temp_user_selected = parseInt(e.value);
                        $("#modal_calendar").find(".btn-primary").removeAttr("disabled");
                    }
                }
            });
        },
        llenar_datos_input: function () {
            let html = "";
            if ($id_tipo_paciente === "3") {
                $('#filters').hide();
                $('#appointmentsTable').removeClass('col-xl-8');
                $('#appointmentsTable').addClass('col-xl-12');
            } else {
                $('#filters').show();
                $('#appointmentsTable').removeClass('col-xl-12');
                $('#appointmentsTable').addClass('col-xl-8');
            }
            $.each($this.sucursales, function (index, value) {
                if (!sessionStorage.getItem("select_clinica")) {
                    sessionStorage.setItem("select_clinica", index);
                }
                html += "<option value='" + index + "'>" + value.name + "</option>";
            });
            $("#select_clinica").html(html);
            $("#select_clinica").val(sessionStorage.getItem("select_clinica"));
            $this.get_doctores();

            html = "";
            $.each($this.servicios, function (index, value) {
                if (value.idmedico === $('#select_medicos').val())
                    html += "<option value='" + index + "'>" + value.name + "</option>";
            });
            $("#alta_cita_servicio").html(html);

            // html = "";
            // $.each($this.ciudades, function (index, value) {
            //     html += "<option value='" + index + "'>" + value.name + "</option>";
            // });
            // $("#modal_new_aptient_city").html(html);
            let $array_temp = [];
            $.each($this.ciudades, function (index, value) {
                let $mini_array = [];
                $mini_array['id'] = index;
                $mini_array['name'] = value.name;
                $array_temp.push($mini_array);
            });

            $("#modal_new_aptient_city").typeahead('destroy');
            $('#modal_new_aptient_city').typeahead({
                source: $array_temp,
                display: 'value',
                onSelect: function (e, dato) {
                    if (e.value == "-21") {
                        $("#modal_new_aptient_city").val("");
                        $id_city_ref = 0;
                        $("#form_add_patient").find(".btn-primary").attr("disabled", "disabled");
                    } else {
                        $id_city_ref = parseInt(e.value);
                        $("#form_add_patient").find(".btn-primary").removeAttr("disabled");
                    }
                }
            });

        },
        get_events_calendar: function () {
            data = {
                tipo: 1,
                view: "citas",
                id_clinica: $("#select_clinica").val(),
                id_medico: $("#select_medicos").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.calendar.removeAllEvents();
                $.each(json, function (index, value) {
                    $this.calendar.addEvent({
                        id: value.id,
                        "title": value.nombre,
                        "start": value.finicio,
                        "end": value.ffinal,
                        "classNames": value.classNames,
                        groupId: value.estado
                    });
                });
                $this.calendar.setOption('minTime', $this.sucursales[$("#select_clinica").val()].medicos[$("#select_medicos").val()].horaEntrada);
                $this.calendar.setOption('maxTime', $this.sucursales[$("#select_clinica").val()].medicos[$("#select_medicos").val()].horaSalida);
                html = "";
                $.each($this.servicios, function (index, value) {
                    if (value.idmedico === $('#select_medicos').val())
                        html += "<option value='" + index + "'>" + value.name + "</option>";
                });
                $("#alta_cita_servicio").html(html);
            }, "json").done(function () {

            });
        },
        set_events: function () {
            $("#inputGroupFile01").unbind("input").on("input", function () {
                ini.convertToBase64();
            })
            $("#select_clinica").unbind("change").on("change", function () {
                sessionStorage.setItem("select_clinica", $(this).val());
                $this.get_doctores();
            });
            $("#select_medicos").unbind("change").on("change", function () {
                sessionStorage.setItem("select_medicos", $(this).val());
                $this.get_events_calendar();
            });
            /*$("#modal_form_save_historial").unbind("submit").on("submit",function(e){
                e.preventDefault();
                let data={
                    id_cita: $this.id_cita,
                    problematica:$("#modal_form_historial_nombre_problematica").val(),
                    observaciones:$("#modal_form_historial_nombre_observacion").val(),
                    tratamiento:$("#modal_form_historial_nombre_tratamiento").val(),
                    notas:$("#modal_form_historial_nombre_notas").val(),
                    estado_cita: $("#modal_form_historial_estado").val(),
                    id_medico: $("#select_medicos").val()

                }
                $this.save_historial_clinico(data);
            });*/
            $("#form_add_patient").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($(".needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    $info = {
                        name: $("#modal_new_aptient_name").val(),
                        date: $("#modal_new_aptient_date").val(),
                        address: $("#modal_new_aptient_address").val(),
                        city: $id_city_ref,
                        phone: $("#modal_new_aptient_phone").val(),
                        email: $("#modal_new_aptient_email").val(),
                    }
                    $this.save_patient($info);
                }
            });
            $("#form_add_cita").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $("#btn_save_new_cita").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                $this.save_cita();
            });
            $('#modal_form_historial_estado').change(function () {
                switch ($('#modal_form_historial_estado').val()) {
                    case '3':
                        $('#select_type_session').removeClass('d-none');
                        switch ($('#modal_form_type_session').val()) {
                            case '0':
                                $('#select_individual_options').removeClass('d-none');
                                $('#select_couple_options').addClass('d-none');
                                break;
                            case '1':
                                $('#select_individual_options').addClass('d-none');
                                $('#select_couple_options').removeClass('d-none');
                                break;
                        }
                        break;
                    default:
                        $('#select_type_session').addClass('d-none');
                        $('#select_individual_options').addClass('d-none');
                        $('#select_couple_options').addClass('d-none');
                        break;
                }
            });
            $('#modal_form_type_session').change(function () {
                switch ($('#modal_form_type_session').val()) {
                    case '0':
                        $('#select_individual_options').removeClass('d-none');
                        $('#modal_form_historial_individual_options option[value=""]').prop('selected', true);
                        $('#select_couple_options').addClass('d-none');
                        break;
                    case '1':
                        $('#select_individual_options').addClass('d-none');
                        $('#select_couple_options').removeClass('d-none');
                        $('#modal_form_historial_couple_options option[value=""]').prop('selected', true);
                        break;
                }
                all_displaynone();

            });
            $('#modal_form_historial_individual_options').change(() => {
                all_displaynone();
                switch ($('#modal_form_historial_individual_options').val()) {
                    case '0':
                        $('#individual_free_version').removeClass('d-none');
                        type_session = 'individual_free_version';
                        break;
                    case '1':
                        $('#individual_handwriting_scan').removeClass('d-none');
                        type_session = 'individual_handwriting_scan';
                        break;
                    case '2':
                        $('#individual_intake_session').removeClass('d-none');
                        type_session = 'individual_intake_session';
                        break;
                    case '3':
                        $('#individual_soap_autocomplete').removeClass('d-none');
                        type_session = 'individual_soap_autocomplete';
                        break;
                    case '4':
                        $('#individual_soap_full_version').removeClass('d-none');
                        type_session = 'individual_soap_full_version';
                        break;
                    case '5':
                        $('#individual_snapshot').removeClass('d-none');
                        type_session = 'individual_snapshot';
                        break;

                }
            });
            $('#modal_form_historial_couple_options').change(() => {
                all_displaynone();
                switch ($('#modal_form_historial_couple_options').val()) {
                    case '0':
                        $('#couple_clients_information').removeClass('d-none');
                        type_session = 'couple_clients_information';
                        break;
                    case '1':
                        $('#couple_clinical_note_autocomplete').removeClass('d-none');
                        type_session = 'couple_clinical_note_autocomplete';
                        break;
                    case '2':
                        $('#couple_intake_session').removeClass('d-none');
                        type_session = 'couple_intake_session';
                        break;
                    case '3':
                        $('#couple_free_writing').removeClass('d-none');
                        type_session = 'couple_free_writing';
                        break;
                    case '4':
                        $('#couple_hand_writing_scan').removeClass('d-none');
                        type_session = 'couple_hand_writing_scan';
                        break;
                    case '5':
                        $('#couple_soap_full_version').removeClass('d-none');
                        type_session = 'couple_soap_full_version';
                        break;
                    case '6':
                        $('#couple_free_style_couple_session').removeClass('d-none');
                        type_session = 'couple_free_style_couple_session';

                        break;

                }
            });
            let all_displaynone = () => {
                $('#individual_free_version').addClass('d-none');
                $('#individual_handwriting_scan').addClass('d-none');
                $('#individual_intake_session').addClass('d-none');
                $('#individual_soap_autocomplete').addClass('d-none');
                $('#individual_soap_full_version').addClass('d-none');
                $('#individual_snapshot').addClass('d-none');

                $('#couple_clients_information').addClass('d-none');
                $('#couple_clinical_note_autocomplete').addClass('d-none');
                $('#couple_intake_session').addClass('d-none');
                $('#couple_free_writing').addClass('d-none');
                $('#couple_hand_writing_scan').addClass('d-none');
                $('#couple_soap_full_version').addClass('d-none');
                $('#couple_free_style_couple_session').addClass('d-none');
                type_session = '';
            };
        },
        get_doctores: function () {
            try {
                let html = "";
                $.each($this.sucursales[sessionStorage.getItem("select_clinica")].medicos, function (index, value) {
                    html += "<option value='" + index + "'>" + value.nombre + "</option>";
                });
                $("#select_medicos").html(html);
                $this.get_events_calendar();
            } catch (error) {
                console.error(error);
                alert('The list of clinics or therapists is empty.');
            }
        },
        inicializar_calendar: function () {
            let calendarEl = document.getElementById('calendario_citas');

            $this.calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list'],
                // themeSystem: "bootstrap",
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'timeGridWeek,timeGridDay,listMonth'
                },
                defaultDate: moment().format("Y-MM-DD"),
                defaultView: 'timeGridWeek',
                weekNumbers: true,
                selectable: true,
                selectMirror: false,
                height: 900,
                aspectRatio: 2,
                select: function (arg) {
                    $this.rango_fechas = arg;
                    $("#alta_cita_date").html("<b>" + moment(arg.start).format("Y-MM-DD") + "</b>");
                    $("#alta_cita_finicio").val(moment(arg.start).format("HH:mm:ss"));
                    $("#alta_cita_ffinal").val(moment(arg.end).format("HH:mm:ss"));
                    ini.modal.show("modal_calendar");
                },
                eventClick: function (info) {
                    $this.id_cita = info.event._def.publicId;
                    $("#modal_form_historial_nombre_paciente").val(info.event._def.title);
                    switch (parseInt(info.event._def.groupId)) {
                        case 1:
                            ini.modal.show("modal_no_confirm");
                            id_cita = info.event._def.publicId;
                            break;
                        case 2:
                            $("#modal_confirm_name_user").html(info.event._def.title);
                            $("#modal_confirm_date_event").html(moment(info.event.start).format("YYYY-MM-DD HH:mm:ss"));
                            $(".clients_name").val(info.event._def.title);
                            $(".current_date").val(moment(info.event.start).format("YYYY-MM-DD"));
                            $(".therapist_name_and_credentials").val($("#select_medicos").val());
                            ini.modal.show("modal_confirm", $this.save_historial_clinico, "#modal_form_save_historial");
                            break;
                        case 3:
                        case 4:
                        case 5:
                            ini.modal.show("modal_ingresar_historial");
                            break;
                    }

                },
                allDaySlot: false,
                minTime: "08:00:00",
                maxTime: "22:00:00",
                // hiddenDays: [ 1, 3, 5 ],
                // weekends: false
            });
            $this.calendar.render();

        },
        save_cita: function () {
            let fi = moment($('#alta_cita_finicio').val(), "LT");
            let ff = moment($('#alta_cita_ffinal').val(), "LT");
            var fstart = moment($this.rango_fechas.start, 'ddd MMM D YYYY HH:mm:ss ZZ');
            var fend = moment($this.rango_fechas.end, 'ddd MMM D YYYY HH:mm:ss ZZ');
            $this.rango_fechas.start = fstart.set({
                'hour': fi.get('hour'),
                'minute': fi.get('minute')
            });
            $this.rango_fechas.end = fend.set({
                'hour': ff.get('hour'),
                'minute': ff.get('minute')
            });
            let data = {
                tipo: 4,
                view: "citas",
                user_mail: $this.pacientes[$this.temp_user_selected].email,
                id_servicio: $("#alta_cita_servicio").val(),
                id_sucursal: $("#select_clinica").val(),
                id_paciente: $this.temp_user_selected,
                id_medico: $("#select_medicos").val(),
                start_date: moment($this.rango_fechas.start).format("Y-MM-DD HH:mm:ss"),
                end_date: moment($this.rango_fechas.end).format("Y-MM-DD HH:mm:ss"),
                name_medic: $('#select_medicos option:selected').html()
            }
            // console.log(fi.get('hour'));
            // console.log(ff.get('minute'));
            // console.log($this.rango_fechas.start);
            // console.log($this.rango_fechas.end);
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    ini.modal.close("modal_calendar");
                    $("#form_add_cita")[0].reset();
                    $this.calendar.addEvent({
                        id: json.event.id,
                        "title": json.event.nombre,
                        "start": json.event.finicio,
                        "end": json.event.ffinal,
                        classNames: "grey darken-3",
                        groupId: 1
                    });
                } else {
                    if (json.traslapeo) {
                        alert("ya existe una cita dentro de esta fecha");
                    }
                }
            }, "json").done(function () {
                $("#btn_save_new_cita").html("SAVE APPOINTMENT");
            });
        }
    }
    usuarios = {
        ini: function () {
            $this = this;
            $this.datatable_usuarios = $('#t_usuarios').DataTable();
            $('.dataTables_length').addClass('bs-select');
            $this.get_datos_input();
            $this.set_events();
        },
        update_data_user: function () {
            console.log('update user fun');
            let data = {
                tipo: 4,
                view: "usuarios",
                info: $("#form_editar_users").serializeArray(),
                id_usuario: $this.id_edit_user
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_de_usuarios();
                    ini.modal.close();
                    $("#form_editar_users")[0].reset();
                    $('#modal_edit_user_email').removeClass('is-invalid');
                    $('#modal_edit_user_mobile').removeClass('is-invalid');
                }
                if (json.registered_mail) {
                    $('#modal_edit_user_email').addClass('is-invalid');
                }
                if (json.registered_mobile) {
                    $('#modal_edit_user_mobile').addClass('is-invalid');
                }
            }, "json").done(function () {
                $("#form_editar_users").find("[type=submit]").removeAttr("disabled").html("SAVE USER");
            });
        },
        editar_usuario: function (id) {
            console.log('Edit user funcition   '+$this.lista_usuarios[id].Mobile_number);
            $this.id_edit_user = id;
            $("#modal_edit_user_name").val($this.lista_usuarios[id].name);
            $("#modal_edit_user_email").val($this.lista_usuarios[id].correo);
            $("#modal_edit_user_username").val($this.lista_usuarios[id].username);
            $("#modal_edit_user_password").val($this.lista_usuarios[id].password);
            $("#modal_edit_typeuser").val($this.lista_usuarios[id].idtipo_usuario);
            $("#modal_edit_user_mobile").val($this.lista_usuarios[id].Mobile_number);
            ini.modal.show("modal_editar_user", $this.update_data_user, "#form_editar_users");
        },
        save_new_user: function (form) {
             console.log('Save New user funcition   ');
            data = {
                tipo: 3,
                view: "usuarios",
                form: form,
                id_clinica: $("#usuarios_option_sucursal").val()
            }
            console.log('URL:__'+ini.url+' DATA__'+data);
            $.post(ini.url, data, function (json) {
                console.log('response___'+ JSON. stringify(json));
                if (typeof (json) == 'string') {
                    console.log('URL:__'+ini.url+' DATA__'+data+' json__'+json);
                    return false;
                }
                //return false;
                if (json.estado) {
                    ini.modal.close("modal_add_new_user");
                    $this.get_lista_de_usuarios();
                    $("#form_add_users")[0].reset();
                    $('#modal_new_user_email').removeClass('is-invalid');
                }
                if (json.registered_mail) {
                    $('#modal_new_user_email').addClass('is-invalid');
                }
                if (json.registered_mobile) {
                    $('#modal_new_user_mobile').addClass('is-invalid');
                }
            }, 'json').done(function () {
                $("#form_add_users").find("[type=submit]").removeAttr("disabled").html("SAVE USER");
            });
        },
        cancel_new_user: function () {
            ini.modal.close("modal_add_new_user");
        },
        add_new_user: function () {
            $("#form_add_users")[0].reset();
            ini.modal.show("modal_add_new_user");
        },
        llenar_inputs: function () {

            console.log('llenar_inputs funcition   ');
            let html = "";
            $.each($this.sucursales, function (index, value) {
                html += "<option value='" + index + "'>" + value + "</option>";
            });
            $("#usuarios_option_sucursal").html(html);
            $("#usuarios_option_sucursal").unbind("change").on("change", function () {
                $this.get_lista_de_usuarios();
            });
            html = "";
            $.each($this.tipo_usuario, function (index, value) {
                html += "<option value='" + index + "'>" + value + "</option>";
            });
            $("#modal_new_typeuser,#modal_edit_typeuser").html(html);
            $this.get_lista_de_usuarios();
        },
        get_datos_input: function () {
            console.log('get_datos_input funcition   ');
            data = {
                tipo: 2,
                view: "usuarios"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.sucursales = json.sucursales;
                $this.tipo_usuario = json.tipo_usuario;
            }, 'json').done(function () {
                $this.llenar_inputs();
            });
        },
        llenar_tabla_usuarios: function () {
            console.log('llenar_tabla_usuarios funcition   ');
            let rows = [];
            $.each($this.lista_usuarios, function (index, value) {
                let columns = [];
                columns.push(value.name);
                columns.push(value.correo);
                columns.push(value.username);
                columns.push(value.sucursal);
                columns.push(value.tipo_usuario);
                //columns.push(value.Mobile_number);
                columns.push("<div class='btn btn-sm float-right btn-primary m-0' onclick='usuarios.editar_usuario(" + index + ")'><i class='fas fa-edit'></i> Edit</div>");
                rows.push(columns);
            });
            $this.datatable_usuarios.clear().rows.add(rows).draw();
        },
        get_lista_de_usuarios: function () {

            console.log('get_lista_de_usuarios funcition   ');
            let data = {
                tipo: 1,
                view: "usuarios",
                idsucursal: $("#usuarios_option_sucursal").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                console.log('userINFO__'+ JSON.stringify(json));
                $this.lista_usuarios = json;
            }, "json").done(function () {
                $this.llenar_tabla_usuarios();
            });
        },
        set_events: function () {
            $("#form_add_users").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $("form_add_users").find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                $this.save_new_user($(this).serializeArray());
            });
        }
    }
    login = {
        ini: function () {
            $this = this;
            $this.set_events();
        },
        validate_login: function () {
            data = {
                tipo: 1,
                view: "login",
                user_name: $("#usuario").val(),
                password: $("#password").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log('json value___'+json);
                    return false;
                }
                if (json) {
                    $id_tipo_paciente = json.idtipo_usuario;
                    console.log('LOGIN METHOD JSON__'+ $id_tipo_paciente);
                    sessionStorage.setItem("iduser", json.idusuario);
                    if (ini.inicializar_menu($id_tipo_paciente)) {
                        sessionStorage.setItem("login", 1);
                        $(".main_navbar").removeClass("d-none");
                        ini.ini_load_view.citas();
                    }
                } else {
                    $("#usuario,#password").addClass("is-invalid");
                    setTimeout(() => {
                        $("#usuario,#password").removeClass("is-invalid");
                    }, 1000);
                }
            }, "json");
        },
       logout: function () {
            data = {
                tipo: 2,
                view: "login"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    sessionStorage.setItem("login", 0);
                    $(".main_navbar").addClass("d-none");
                    ini.ini_load_view.login();
                    sessionStorage.clear();
                }
            }, "json");
        },
        recover_account: function () {
            data = {
                tipo: 4,
                view: "login",
                recoveraccount: $("#recoveraccount").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    alert("The e-mail has been send");
                    document.location.href = "/";
                } else {
                    alert("This email has not been registered.");
                }
            }, "json");
        },
        set_events: function () {
            $("#form_login").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $this.validate_login();
            });
            $("#form_recoveraccount").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $this.recover_account();
            });
         }
    }
    invoice = {
        ini: function () {
            $this = this;
            $this.get_filtros();
            $this.get_datos_input();
            $this.addEvent();
            $this.datatable = $("#t_invoice").DataTable({
                "order": [
                    [0, "desc"]
                ]
            });
        },
        close_view_invoice: function () {
            $id_historial = null;
            $id_historial = null;
            $("#logo_factura").attr("src", "");
            $("#paciente_name").html("");
            $("#id_invoice").html("#");
            $("#date_invoice").html();
            $("#payment_due").html("");
            $("#amount_due").html("");
            $("#session_unit").html("");
            $("#session_total").html("");
            $("#lb_importe").html("");
            $("#cliente_name").html("");
            $("#medic_serie_invoice").html("");
            $("#medic_folio_invoice").html("");
            $("#precio_subtotal_importe").html("");
            $("#precio_importe").html("");
            $("#input_select_cliente").val("");
            //$("#type_of_payment").html();
            $("#precio_total").html("");
            $("#precio_payment").html("");
            $("#amount_total").html("");

            $("#show_invoice").removeClass("d-none");
            $("#load_invoice").addClass("d-none");
        },
        show_invoice: function (id_cita) {
            $id_historial = id_cita;
            invoice_data[$id_historial].fecha=moment().format("Y-MM-DD");
            $("#logo_factura").attr("src", "../files/personal_logo/" + invoice_data[$id_historial].imagen);
            $("#paciente_name").html(invoice_data[$id_historial].nombre);
            $("#id_invoice").html("#" + $id_historial);
            $("#date_invoice").html(invoice_data[$id_historial].fecha);
            $("#payment_due").html(invoice_data[$id_historial].importe);
            $("#amount_due").html(invoice_data[$id_historial].impuesto);
            $("#session_unit").html(invoice_data[$id_historial].importe);
            $("#session_total").html(invoice_data[$id_historial].importe);
            $("#lb_importe").html(invoice_data[$id_historial].impuesto);
            $("#medic_serie_invoice").html(invoice_data[$id_historial].serie);
            $("#medic_folio_invoice").html(invoice_data[$id_historial].folio);
            $("#precio_subtotal_importe").html(invoice_data[$id_historial].importe);
            $("#precio_importe").html((invoice_data[$id_historial].impuesto / 100) * invoice_data[$id_historial].importe);
            //$("#type_of_payment").html();
            $("#precio_total").html(parseFloat((invoice_data[$id_historial].impuesto / 100) * invoice_data[$id_historial].importe) + parseFloat(invoice_data[$id_historial].importe));
            $("#precio_payment").html(parseFloat((invoice_data[$id_historial].impuesto / 100) * invoice_data[$id_historial].importe) + parseFloat(invoice_data[$id_historial].importe));
            $("#amount_total").html(parseFloat((invoice_data[$id_historial].impuesto / 100) * invoice_data[$id_historial].importe) + parseFloat(invoice_data[$id_historial].importe))
            $("#show_invoice").addClass("d-none");
            $("#load_invoice").removeClass("d-none");
        },
        send_invoice: function () {
            if ($id_cliente_selected == null) {
                return false;
            }
            $("#btn_send_invoice").addClass("disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
            let data = {
                tipo: 3,
                view: "invoices",
                idtype_payment: $("#type_payment").val(),
                idestadoinvoice: $("#id_estado_factura").val(),
                idcita: $id_historial,
                info: invoice_data[$id_historial],
                id_cliente: $id_cliente_selected,
                email_cliente: $this.lista_de_clientes[$id_cliente_selected].email,
                client_name: $this.lista_de_clientes[$id_cliente_selected].name
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $id_cliente_selected = null;
                    $this.close_view_invoice();
                    $this.get_lista_citas();
                }
            }, "json").done(function () {
                $("#btn_send_invoice").removeClass("disabled").find(".fa-spinner").remove();
            });
        },
        donwload_invoice: function (id_invoice) {
            let data = {
                tipo: 6,
                view: "invoices",
                id_invoice: id_invoice
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.file != false) {
                    var link = document.createElement("a");
                    link.download = "patient_file";
                    link.href = "files/" + json.file;
                    link.click();
                }

            }, "json");
        },
        get_lista_citas: function () {
            let data = {
                tipo: 2,
                view: "invoices",
                id_medico: $("#select_medicos").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                invoice_data = json;
                $this.datatable.clear();
                $.each(json, function (index, value) {
                    $this.datatable.row.add([index, value.nombre, value.sucursal, value.servicio, value.medico, value.estadofactura, value.buttons]);
                });
                $this.datatable.draw();
            }, "json");
        },
        get_filtros: function () {
            let data = {
                tipo: 1,
                view: "invoices",
                idtipo_usuario: $id_tipo_paciente
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.sucursales = json.sucursales;
                $this.servicios = json.servicio;
                $this.ciudades = json.ciudades;
                $this.llenar_datos_input();
            }, "json");
        },
        llenar_datos_input: function () {
            let html = "";

            if ($id_tipo_paciente === "3")
                $('#filters').hide();
            else
                $('#filters').show();

            $.each($this.sucursales, function (index, value) {
                if (!sessionStorage.getItem("invoice_select_clinica")) {
                    sessionStorage.setItem("invoice_select_clinica", index);
                }
                html += "<option value='" + index + "'>" + value.name + "</option>";
            });
            $("#select_clinica").html(html);
            $("#select_clinica").val(sessionStorage.getItem("invoice_select_clinica"));
            $("#select_clinica").unbind("change").on("change", function () {
                sessionStorage.setItem("invoice_select_clinica", $(this).val());
                $this.get_doctores();
            });
            $this.get_doctores();

        },
        get_doctores: function () {
            let html = "";
            $.each($this.sucursales[sessionStorage.getItem("invoice_select_clinica")].medicos, function (index, value) {
                html += "<option value='" + index + "'>" + value.nombre + "</option>";
            });
            $("#select_medicos").html(html);
            $this.get_lista_citas();
            $("#select_medicos").unbind("change").on("change", function () {
                $this.get_lista_citas();
            });
        },
        add_new_client: function () {
            ini.modal.show("modal_client");
        },
        cancel_new_client: function () {
            $("#add_new_client").addClass("d-none");
            $("#add_new_client")[0].reset();
            $("#select_cliente").removeClass("d-none");
        },
        btn_add_new_client: function () {
            $("#add_new_client").removeClass("d-none");
            $("#select_cliente").addClass("d-none");
        },
        get_datos_input: function () {
            let data = {
                tipo: 4,
                view: "invoices"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_clientes = json.clientes;
                let $array_temp = [];
                $.each(json.clientes, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });
                $("#input_select_cliente").typeahead('destroy');
                $('#input_select_cliente').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#input_select_cliente").val("");
                            $id_cliente_selected = null;
                            $("#select_cliente").find(".btn-primary").attr("disabled", "disabled");
                            $("#btn_send_invoice").attr("disabled", "disabled");
                        } else {
                            $id_cliente_selected = parseInt(e.value);
                            $("#select_cliente").find(".btn-primary").removeAttr("disabled");

                        }
                    }
                });
                let html = "";
                $.each(json.ciudades, function (index, value) {
                    html += "<option value='" + index + "'>" + value.name + "</option>";
                });
                $("#modal_new_client_city").html(html);
                html = "";
                $.each(json.estadofactura, function (index, value) {
                    html += "<option value='" + index + "'>" + value.name + "</option>";
                });
                $("#id_estado_factura").html(html);
                html = "";
                $.each(json.typepayment, function (index, value) {
                    html += "<option value='" + index + "'>" + value.name + "</option>";
                });
                $("#type_payment").html(html);
            }, 'json');
        },
        addEvent: function () {
            $("#add_new_client").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                let data = {
                    tipo: 5,
                    view: "invoices",
                    name: $("#modal_new_client_name").val(),
                    date: $("#modal_new_client_date").val(),
                    address: $("#modal_new_client_address").val(),
                    city: $("#modal_new_client_city").val(),
                    phone: $("#modal_new_client_phone").val(),
                    email: $("#modal_new_client_email").val()
                }
                $.post(ini.url, data, function (json) {
                    if (typeof (json) == 'string') {
                        console.log(json);
                        return false;
                    }
                    if (json.estado) {
                        $id_cliente_selected = json.id_client;

                        $("#btn_send_invoice").removeAttr("disabled");
                        $("#cliente_name").html(data.name);
                        $this.get_datos_input();
                        ini.modal.close();
                    }
                }, "json").done(function () {
                    if ($id_cliente_selected > 0) {
                        $("#cliente_name").html($this.lista_de_clientes[$id_cliente_selected].name);
                        $("#btn_send_invoice").removeAttr("disabled");
                    }

                });
            });
            $("#select_cliente").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $("#cliente_name").html($this.lista_de_clientes[$id_cliente_selected].name);
                $("#btn_send_invoice").removeAttr("disabled");
                ini.modal.close();
            });
        }
    }
    reports = {
        ini: function () {
            $this = this;
            $this.t_reportes_issue_ivoice = $("#t_reportes_issue_ivoice").DataTable({
                "order": [
                    [0, "desc"]
                ]
            });
            $this.t_reportes_client_of_mobth = $("#t_reportes_client_of_mobth").DataTable();
            $this.t_reportes_client_of_month = $("#t_reportes_client_of_month").DataTable({
                "order": [
                    [0, "desc"]
                ]
            });
            $this.get_issue_invoice();
            $this.set_events();
        },
        get_issue_invoice: function () {
            let data = {
                tipo: 1,
                view: "reports"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.t_reportes_client_of_mobth.clear();
                $.each(json.invoice, function (index, value) {
                    $this.t_reportes_issue_ivoice.row.add([index, value.fecha, value.cliente, value.total, value.sucursal]);
                });
                $this.t_reportes_issue_ivoice.draw();

                $this.t_reportes_client_of_mobth.clear();
                $.each(json.client_month, function (index, value) {
                    $this.t_reportes_client_of_mobth.row.add([value.nombre, value.fecha, value.sucursal, value.citas]);
                });
                $this.t_reportes_client_of_mobth.draw();

                $this.t_reportes_client_of_month.clear();
                $.each(json.appointment, function (index, value) {
                    $this.t_reportes_client_of_month.row.add([index, value.fecha, value.paciente, value.sucursal, value.servicio, value.medico, value.estado]);
                });
                $this.t_reportes_client_of_month.draw();
            }, "json");
        },
        set_events: function () {
            $("[select-option-menu]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#" + $(this).attr("key")).removeClass("d-none");
                switch ($(this).attr("key")) {
                    case "boton_usuarios":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        usuarios.ini();
                        break;
                    case "boton_ciudades":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        ciudades.ini();
                        break;
                    case "boton_tipos_terapias":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        servicios.ini();
                        break;
                    case "boton_clinicas":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        clinicas.ini();
                        break;
                    case "boton_medico":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        medicos.ini();
                        break;
                    case "boton_clientes":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        pacientes.ini();
                        break;
                    case "boton_provincia":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        provincia.ini();
                        break;
                }
            });
            $("[close_view_reportes]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#reportes_menu").removeClass("d-none");
            });
        }
    }
    ini = {
        url: "app/ini_case.php",
        ini: function (sub, public) {
            $this = this;
            if(sub != "", public == "public"){
                ini.url = sub+ini.url;
                $this.ini_load_view.publicUrl();
            }else{
                $this.validar_login();
            }
        },
        ini_load_view: {
            login: function () {
                sessionStorage.setItem("view", "login");
                ini.load_view()
            },
            recoveraccount: function () {
                sessionStorage.setItem("view", "recoveraccount");
                ini.load_view()
            },
            OTPLogin: function () {
                sessionStorage.setItem("view", "OTPLogin");
                ini.load_view()
            },
            usuarios: function () {
                sessionStorage.setItem("view", "usuarios");
                ini.load_view();
            },
            pacientes: function () {
                sessionStorage.setItem("view", "pacientes");
                ini.load_view();
            },
            citas: function () {
                sessionStorage.setItem("view", "citas");
                ini.load_view();
            },
            reportes: function () {
                sessionStorage.setItem("view", "reportes");
                ini.load_view();
            },
            facturas: function () {
                sessionStorage.setItem("view", "facturas");
                ini.load_view();
            },
            medicos: function () {
                sessionStorage.setItem("view", "medicos");
                ini.load_view();
            },
            ciudades: function () {
                sessionStorage.setItem("view", "ciudades")
                ini.load_view();
            },
            configuraciones: function () {
                sessionStorage.setItem("view", "configuraciones")
                ini.load_view();
            },
            egresos: function () {
                sessionStorage.setItem("view", "egresos")
                ini.load_view();
            },
            videoConf: function(){
                sessionStorage.setItem("view", "videoConf")
                ini.load_view();
            },
            publicUrl: function(){
                sessionStorage.setItem("view", "public")
                ini.load_view();
            }
        },
        validar_login: function () {
            data = {
                tipo: 3,
                view: "login"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    if (json.estado) {
                        login.logout();
                    } else {
                        $id_tipo_paciente = json.idtipo_usuario;
                        if ($this.inicializar_menu($id_tipo_paciente)) {
                            if (sessionStorage.getItem("view")) {
                                if (sessionStorage.getItem("login")) {
                                    $(".main_navbar").removeClass("d-none");
                                    $this.ini_load_view[sessionStorage.getItem("view")]();
                                    $("#index_navbar").find("[key=" + sessionStorage.getItem("view") + "]").addClass("active");

                                } else {
                                    $this.ini_load_view.login();
                                }

                            } else {
                                $this.ini_load_view.login();
                            }
                        }
                    }
                }
            }, 'json').done(function () {
                $this.set_events();
            });
        },
        inicializar_menu: function (id) {
            switch (parseInt(id)) {
                case 3:
                    // case 3:
                    html = '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="citas" style="color:#4B70C4; font-weight: 400;">APPOINTMENT</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="facturas" style="color:#4B70C4; font-weight: 400;">INVOICES</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="reportes" style="color:#4B70C4; font-weight: 400;">REPORTS</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="pacientes" style="color:#4B70C4; font-weight: 400;">CLIENTS</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="configuraciones" style="color:#4B70C4; font-weight: 400;">SETTINGS</a>' +
                        '<a href="https://cleverclinic.app:2096/" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">E-MAIL</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">FAX</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="videoConf" style="color:#4B70C4; font-weight: 400;">VIDEO</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">SMS</a>';
                    break;

                default:
                    // html = '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="medicos">Therapist</a>' +
                    html = '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="citas" style="color:#4B70C4; font-weight: 400;">APPOINTMENT</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="facturas" style="color:#4B70C4; font-weight: 400;">INVOICES</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="reportes" style="color:#4B70C4; font-weight: 400;">REPORTS</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="pacientes" style="color:#4B70C4; font-weight: 400;">CLIENTS</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="configuraciones" style="color:#4B70C4; font-weight: 400;">SETTINGS</a>' +
                        '<a href="https://cleverclinic.app:2096/" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">E-MAIL</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">FAX</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="videoConf" style="color:#4B70C4; font-weight: 400;">VIDEO</a>' +
                        '<a href="#" class="list-group-item list-group-item-action" style="color:#4B70C4; font-weight: 400;">SMS</a>';
                    // '<a href="#" class="list-group-item list-group-item-action" key-select-menu key="egresos" style="color:#4B70C4; font-weight: 400;">EXPENSES</a>';
                    break;
            }
            $("#menu_inicial").html(html);
            $("#menu_inicial2").html(html);
            ini.set_events();
            return true;
        },
        load_view: function () {
            if ('Navigator' == navigator.appName) document.forms[0].reset();
            console.log("Gaurav", sessionStorage.getItem("view"));
            $("#main_view").load("app/views/" + sessionStorage.getItem("view") + ".html");
        },
        set_events: function () {
            $("[key-select-menu]").unbind("click").on("click", function () {
                $("#index_navbar").find("li").removeClass("active");
                $("#index_navbar").find("[key=" + $(this).attr("key") + "]").addClass("active");
                ini.ini_load_view[$(this).attr("key")]();
            });
        },
        modal: {
            id: null,
            show: function ($modal, $fn, $id_form) {
                let $id = $("#" + $modal);
                ini.id = $modal;
                if (typeof ($fn) == "function") {
                    let $html = $id.find("[type=submit]").html();
                    $id.modal("show").on("hidden.bs.modal", function () {
                        $id.find("form")[0].reset();
                        $id.find("[type=submit]").removeAttr("disabled", "disabled").html($html);
                    });
                    $id.find("form").unbind("submit").on("submit", function (e) {
                        // e.preventDefault();
                        // $id.find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                        // $fn();
                        e.preventDefault();
                        if ($($id_form)[0].checkValidity() === false) {
                            $($id_form).addClass('was-validated');
                        } else {
                            // $id.find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                            $fn();
                        }
                    });
                } else {
                    $id.modal("show");
                }
            },
            close: function () {
                $("#" + ini.id).modal("hide");
                ini.id = null;
            }
        },
        convertToBase64: function () {
            //Read File
            var selectedFile = document.getElementById("inputGroupFile01").files;
            //Check File is not Empty
            if (selectedFile.length > 0) {
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                var base64;
                // Onload of file read the file content
                fileReader.onload = function (fileLoadedEvent) {
                    $("#label_input_file").html(fileToLoad.name);
                    base64 = fileLoadedEvent.target.result;
                    $this.temp_file = {
                        type: fileToLoad.type,
                        base64: base64,
                    }
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);
            }
        }
    }
    /**** codigo nuevo agregado el 21/10/2019 */
    ciudades = {
        ini: function () {
            $this = this;
            $this.get_datos_input();
            $this.t_ciudades = $("#ciudades_lista").DataTable({
                "order": [
                    [2, "asc"]
                ]
            });
            $this.get_lista_de_ciudades();
            $this.set_events();
        },

        save_new_city: function () {
            data = {
                tipo: 3,
                view: "config_ciudad",
                name: $("#add_new_city_name").val(),
                id_estado: $("#add_new_city_province").val()
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado == true) {
                    $this.get_lista_de_ciudades();
                    ini.modal.close();
                    $("#modal_form_new_city")[0].reset();
                } else {
                    if (json.existente == true) {
                        alert("Esta ciudad ya existe");
                    }
                }
            }, 'json');
        },
        add_new_city: function () {
            ini.modal.show("modal_new_city");
        },
        get_datos_input: function () {
            data = {
                tipo: 2,
                view: "config_ciudad"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_paises_estados = json.paises;
                $("#add_new_city_country").html("");
                $.each($this.lista_paises_estados, function (index, value) {
                    $("#add_new_city_country").append("<option value='" + index + "'>" + value.name + "</option>");
                });
            }, "json").done(function () {
                $this.get_lista_estados();
                $("#add_new_city_country").unbind("click").on("click", function () {
                    $this.get_lista_estados();
                });
            });
        },
        get_lista_estados: function () {
            id_pais = $("#add_new_city_country").val();
            $("#add_new_city_province").html("");
            $.each($this.lista_paises_estados[id_pais].estados, function (index, value) {
                $("#add_new_city_province").append("<option value='" + index + "'>" + value + "</option>");
            });
        },
        get_lista_de_ciudades: function () {
            let data = {
                tipo: 1,
                view: "config_ciudad"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.t_ciudades.clear();
                $.each(json, function (index, value) {
                    $this.t_ciudades.row.add([index, value.ciudad, value.estado, value.pais]);
                });
                $this.t_ciudades.draw();
            }, "json");
        },
        set_events: function () {
            $("#add_new_city_name").unbind("keyup").on("keyup", function () {
                if ($(this).val().length > 0) {
                    $("#modal_new_city").find("[type=submit]").removeAttr("disabled");
                } else {
                    $("#modal_new_city").find("[type=submit]").attr("disabled", "disabled");
                }
            });
            $("#modal_new_city").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $this.save_new_city();
            })
        }
    }
    clinicas = {

        ini: function () {
            $this = this;
            $this.set_events();
            $this.t_clinicas = $("#t_clinicas").DataTable({
                "order": [
                    [1, "asc"]
                ]
            });
            $this.get_lista_de_clinicas();
        },
        save_editar_clinic: function () {
            let data = {
                tipo: 3,
                view: "clinicas",
                name_clinic: $("#add_editar_clinic_name").val(),
                idciudad: $id_city_ref,
                id_clinica: $this.id_clinica
            }
            // console.log(data);
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado == true) {
                    $this.get_lista_de_clinicas();
                    ini.modal.close();
                    $("#modal_form_editar_clinic")[0].reset();
                } else {
                    if (json.existente == true) {
                        alert("Esta clinica ya esta registrada");
                    }
                }
            }, "json");
        },
        edit_clinica: function (id_clinica, id_city) {
            $this.id_clinica = id_city;
            $("#add_editar_clinic_name").val($this.clinicas[id_clinica].sucursales[id_city]);
            $("#edit_city_clinic").val($this.clinicas[id_clinica].name);
            $id_city_ref = id_clinica;
            let data = {
                tipo: 4,
                view: "clinicas"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                let $array_temp = [];
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });
                $("#edit_city_clinic").typeahead('destroy');
                $('#edit_city_clinic').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#edit_city_clinic").val("");
                            $id_city_ref = 0;
                            $("#modal_form_editar_clinic").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_city_ref = parseInt(e.value);
                            $("#modal_form_editar_clinic").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });

            }, "json");
            ini.modal.show("modal_editar_clinic");
        },
        add_new_clinica: function () {
            ini.modal.show("modal_new_clinic");
            let data = {
                tipo: 4,
                view: "clinicas"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                let $array_temp = [];
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });
                $("#add_new_city_clinic").typeahead('destroy');
                $('#add_new_city_clinic').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#add_new_city_clinic").val("");
                            $id_city_ref = 0;
                            $("#modal_form_new_clinic").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_city_ref = parseInt(e.value);
                            $("#modal_form_new_clinic").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });

            }, "json");
        },
        get_lista_de_clinicas: function () {
            data = {
                tipo: 1,
                view: "clinicas"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.clinicas = json;
                $("#admin_clini_select_city").html("");
                $.each($this.clinicas, function (index, value) {
                    $("#admin_clini_select_city").append("<option value='" + index + "'>" + value.name + "</option>");
                });
            }, 'json').done(function () {
                $this.show_tabla_clinicas();
            });
        },
        show_tabla_clinicas: function () {
            $this.t_clinicas.clear();
            $.each($this.clinicas, function (index, value) {
                $.each(value.sucursales, function (i, v) {
                    $this.t_clinicas.row.add([i, v, $this.clinicas[index].name, "<div class='btn btn-primary btn-sm float-right' onclick='clinicas.edit_clinica(" + index + "," + i + ")'><i class='fas fa-edit'></i></div>"]);
                });
            });
            $this.t_clinicas.draw();
        },

        save_new_clinic: function () {
            let data = {
                tipo: 2,
                view: "clinicas",
                name_clinic: $("#add_new_clinic_name").val(),
                idciudad: $id_city_ref
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado == true) {
                    $this.get_lista_de_clinicas();
                    ini.modal.close();
                    $("#modal_form_new_clinic")[0].reset();
                } else {
                    if (json.existente == true) {
                        alert("Esta clinica ya esta registrada");
                    }
                }
            }, "json");
        },
        set_events: function () {
            $('#filterClinic').hide();
            $("#add_new_clinic_name").unbind("keyup").on("keyup", function () {
                if ($(this).val().length > 0) {
                    $("#modal_new_clinic").find("[type=submit]").removeAttr("disabled");
                } else {
                    $("#modal_new_clinic").find("[type=submit]").attr("disabled", "disabled");
                }
            });
            $("#add_editar_clinic_name").unbind("keyup").on("keyup", function () {
                if ($(this).val().length > 0) {
                    $("#modal_editar_clinic").find("[type=submit]").removeAttr("disabled");
                } else {
                    $("#modal_editar_clinic").find("[type=submit]").attr("disabled", "disabled");
                }
            });
            $("#modal_form_editar_clinic").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $this.save_editar_clinic();
            });
            $("#modal_form_new_clinic").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $this.save_new_clinic();
            });
        }
    }
    /**** codigo nuevo agregado el 21/10/2019 */
    'SALVADOR ELVIRA'

    configuraciones = {
        ini: function () {
            $this = this;
            $this.boton_ciudades = $("#boton_ciudades").DataTable();
            $this.boton_usuarios = $("#boton_usuarios").DataTable();
            $this.t_reportes_client_of_month = $("#t_reportes_client_of_month").DataTable();
            $this.Llamar_submenu();
            $this.set_events();
        },
        Llamar_submenu: function () {
            let data = {
                tipo: 1,
                view: "reports"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.ciudades_lista.clear();
                $.each(json.ciudad, function (index, value) {
                    $this.boton_ciudades.row.add([index, value.fecha, value.cliente, value.total, value.sucursal]);
                });
                $this.boton_ciudades.draw();

                $this.boton_usuarios.clear();
                $.each(json.usuarios, function (index, value) {
                    $this.boton_usuarios.row.add([value.nombre]);
                });
                $this.boton_usuarios.draw();

                $this.t_reportes_client_of_month.clear();
                $.each(json.appointment, function (index, value) {
                    $this.t_reportes_client_of_month.row.add([index, value.fecha, value.paciente, value.sucursal, value.servicio, value.medico, value.estado]);
                });
                $this.t_reportes_client_of_month.draw();
            }, "json");
        },
        set_events: function () {
            $("[select-option-menu]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#" + $(this).attr("key")).removeClass("d-none");
            });

            $("[close_view_reportes]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#reportes_menu").removeClass("d-none");
            });
        }
    },
    provincia = {
        ini: function () {
            $this = this;
            $this.lista_provincias = null;
            $this.get_citys();
            $this.t_provincia = $('#t_provincia').DataTable({
                "order": [
                    [2, "asc"]
                ]
            });
            $this.get_lista_provincia();
            $this.citys;
            $this.setevents();
        },
        save_provincia: function (info) {
            data = {
                tipo: 2,
                view: "provincia",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_provincia();
                    ini.modal.close("modal_add_new_provincia");
                }
            }, "json").done(function () {
                $("#form_add_province").find("[type=submit]").removeAttr("disabled").html("SAVE PROVINCE");
                $("#form_add_province")[0].reset();
            });
        },
        add_new_provincia: function () {
            $("#form_add_users")[0].reset();
            ini.modal.show("modal_add_new_provincia");
            let html = '';
            $.each($this.citys, function (index, value) {
                html += "<option value='" + value.idpais + "'>" + value.name + "</option>";
            });
            $('#modal_new_aptient_country').html(html);
        },
        get_lista_provincia: function () {
            let data = {
                tipo: 1,
                view: "provincia"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_provincias = json;
            }, 'json').done(function () {
                $this.llenar_tabla_provincias();
            });
            $this.get_citys();
        },
        llenar_tabla_provincias: function () {
            let rows = [];
            $.each($this.lista_provincias, function (index, value) {
                let columns = [];
                columns.push(value.idestado);
                columns.push(value.estado);
                columns.push(value.pais);
                rows.push(columns);
            });
            $this.t_provincia.clear().rows.add(rows).draw();
        },
        get_citys: function () {
            data = {
                tipo: 2,
                view: "config_ciudad"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.citys = json.paises;
            }, "json");
        },
        setevents: function () {
            $("#form_add_province").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $(this).find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                $info = {
                    province: $("#modal_new_aptient_province").val(),
                    country: $("#modal_new_aptient_country").val(),
                    user: sessionStorage.getItem("iduser")
                }
                $this.save_provincia($info);
            });
        }
    },
    servicios = {
        ini: function () {
            $this = this;
            $this.t_servicios = $('#t_servicios').DataTable({
                "order": [
                    [1, "asc"]
                ]
            });
            $this.setevents();
            $this.get_lista_servicios();
        },
        save_service: function (info) {
            data = {
                tipo: 2,
                view: "servicios",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_servicios();
                    ini.modal.close("modal_add_new_service");
                }
            }, "json").done(function () {
                $("#form_add_service").find("[type=submit]").prop("disabled", false).html("SAVE USER");
                $("#form_add_service")[0].reset();
            });
        },
        setevents: function () {
            $("#form_add_service").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                $(this).find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                $info = {
                    service: $("#modal_new_aptient_service").val(),
                    amount: $("#modal_new_aptient_amount").val(),
                    time: $("#modal_new_aptient_time").val(),
                    tax: $("#modal_new_aptient_tax").val(),
                    idmedico: $id_medico_ref,
                    user: sessionStorage.getItem("iduser")
                }
                $this.save_service($info);
            });
            $("#modal_new_aptient_amount,#modal_new_aptient_tax,#modal_edit_amount,#modal_edit_tax").keyup(function () {
                var $this = $(this);
                $this.val($this.val().replace(/[^\d.]/g, ''));
            });
        },
        cancel_new_service: function () {
            ini.modal.close("modal_add_new_service");
        },
        add_new_service: function () {
            $("#form_add_users")[0].reset();
            $this.get_lista_de_usuarios();
            ini.modal.show("modal_add_new_service");
        },
        update_data_service: function () {
            let data = {
                tipo: 3,
                view: "servicios",
                info: $("#form_editar_service").serializeArray(),
                id_servicio: $this.id_edit_servicio,
                idmedico: $id_medico_ref
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    $this.get_lista_servicios();
                    ini.modal.close();
                }
            }, "json");
        },
        editar_servicio: function (id) {
            $this.id_edit_servicio = id;
            $("#modal_edit_service").val($this.lista_servicios[id].servicio);
            $("#modal_edit_amount").val($this.lista_servicios[id].importe);
            $("#modal_edit_time").val($this.lista_servicios[id].duracion);
            $("#modal_edit_tax").val($this.lista_servicios[id].impuesto);
            $("#modal_edit_user").val($this.lista_servicios[id].medico);
            $id_medico_ref = $this.lista_servicios[id].idmedico;
            $this.get_lista_de_usuarios();
            ini.modal.show("modal_editar_service", $this.update_data_service, "#form_editar_service");
        },
        get_lista_servicios: function () {
            let data = {
                tipo: 1,
                view: "servicios"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_servicios = json;
            }, 'json').done(function () {
                $this.llenar_tabla_servicios();
            });
        },
        llenar_tabla_servicios: function () {
            let rows = [];
            $.each($this.lista_servicios, function (index, value) {
                let columns = [];
                columns.push(value.idservicio);
                columns.push(value.servicio);
                columns.push(value.importe);
                columns.push(value.duracion);
                columns.push(value.impuesto);
                columns.push(value.usuario);
                columns.push(value.medico);
                columns.push("<div class='btn btn-sm float-right btn-primary m-0' onclick='servicios.editar_servicio(" + index + ")'><i class='fas fa-edit'></i> Edit</div>");
                rows.push(columns);
            });
            $this.t_servicios.clear().rows.add(rows).draw();
        },
        get_lista_de_usuarios: function () {
            let data = {
                tipo: 1,
                view: "medicos",
                id_sucursal: sessionStorage.getItem("select_clinica")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_usuarios = json;
                let $array_temp = [];
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.nombre;
                    $array_temp.push($mini_array);
                });
                $("#modal_new_aptient_user,#modal_edit_user").typeahead('destroy');
                $('#modal_new_aptient_user,#modal_edit_user').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#modal_new_aptient_user,#modal_edit_user").val("");
                            $id_medico_ref = 0;
                            $("#form_add_service,#form_editar_service").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_medico_ref = parseInt(e.value);
                            $("#form_add_service,#form_editar_service").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });
            }, "json");
        }
    },
    egresos = {
        ini: function () {
            $this = this;
            $this.set_events();
        },
        save_patient: function (info) {
            data = {
                tipo: 2,
                view: "pacientes",
                info: info
            }
            $.post(ini.url, data, function (json) {
                console.log("is mobile registered__"+JSON.stringify(json));
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {

                    $this.get_lista_pacientes();
                    ini.modal.close("modal_add_paciente");
                }

                // if (json.registered_mobile) {
                //     $('#modal_new_aptient_phone').addClass('is-invalid');
                // }

            }, "json").done(function () {
                $("#form_add_users").find("[type=submit]").removeAttr("disabled").html("SAVE PATIENT");
                $("#form_add_users")[0].reset();
            });
        },
        set_events: function () {
            $("[select-option-menu]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#" + $(this).attr("key")).removeClass("d-none");
                switch ($(this).attr("key")) {
                    case "boton_productos":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        productos.ini();
                        break;
                    case "boton_proveedores":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        proveedores.ini();
                        break;
                    case "boton_compras":
                        if ('Navigator' == navigator.appName) document.forms[0].reset();
                        compras.ini();
                        break;
                }
            });
            $("[close_view_reportes]").unbind("click").on("click", function () {
                $(".reportes_show").addClass("d-none");
                $("#reportes_menu").removeClass("d-none");
            });
        }
    },
    productos = {
        ini: function () {
            $this = this;
            $this.t_table_productos = $("#t_productos").DataTable({
                    "order": [
                        [1, "asc"]
                    ],
                    "columnDefs": [{
                        "width": "10%",
                        "targets": 0
                    }]
                }),
                $this.get_lista_productos();
            $this.set_events();
        },
        get_lista_productos: function () {
            let data = {
                tipo: 1,
                view: "productos",
                id_sucursal: sessionStorage.getItem("medicos_sucursal")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_productos = json;
                $this.t_table_productos.clear().draw();
                let cont = 0;
                $.each(json, function (index, value) {
                    $this.t_table_productos.row.add([++cont, value.clave, value.producto, value.costo_ultimo, value.impuesto, value.usuario,
                        `<div class='btn-group'>
                            <button type="button" class="btn btn-primary btn-sm" onclick='productos.editar_producto(${index})'>
                                <i class="fas fa-edit fa-lg"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-sm" onclick='productos.eliminar_producto(${index})'>
                                <i class="far fa-trash-alt fa-lg"></i>
                            </button>
                        </div>`
                    ]);
                });
                $this.t_table_productos.draw();
            }, "json").done(function () {
                // $this.get_lista_de_productos();
            });
        },
        save_product: function (info) {
            data = {
                tipo: 2,
                view: "productos",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_productos();
                    $("#modal_add_product").modal('hide');
                }
            }, "json").done(function () {
                $("#form_add_product").find("[type=submit]").removeAttr("disabled").html("SAVE");
                $(".needs-validation").removeClass('was-validated');
                $("#form_add_product")[0].reset();
            });
        },
        editar_producto: function (id) {
            console.log($this.lista_de_productos[id]);
            $this.id_producto = id;
            $("#modal_edit_product_key").val($this.lista_de_productos[id].clave);
            $("#modal_edit_product_name").val($this.lista_de_productos[id].producto);
            $("#modal_edit_product_cost").val($this.lista_de_productos[id].costo_ultimo);
            $("#modal_edit_product_tax").val($this.lista_de_productos[id].impuesto);
            ini.modal.show("modal_edit_product", $this.update_data_product, "#form_edit_product");
        },
        update_data_product: function () {
            let data = {
                tipo: 3,
                view: "productos",
                info: $("#form_edit_product").serializeArray(),
                id: $this.id_producto
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    $this.get_lista_productos();
                    ini.modal.close();
                }
            }, "json");
        },
        eliminar_producto: function (id) {
            if (confirm("Are you sure you're removing this supplier?")) {
                let data = {
                    tipo: 4,
                    view: "productos",
                    id: id
                }
                $.post(ini.url, data, function (json) {
                    if (typeof (json) == 'string') {
                        console.log(json);
                        return false;
                    }
                    if (json.estado) {
                        $this.get_lista_productos();
                    }
                }, "json");
            }
        },
        set_events: function () {
            $("#form_add_product").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($(".needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    $("#form_add_product").find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                    $info = {
                        key: $("#modal_add_product_key").val(),
                        name: $("#modal_add_product_name").val(),
                        cost: $("#modal_add_product_cost").val(),
                        tax: $("#modal_add_product_tax").val(),
                        user: sessionStorage.getItem("iduser")
                    }
                    $this.save_product($info);
                }
            });
            $("#form_add_product").find("button[type=button]").click(function () {
                $("#form_add_product")[0].reset();
                $(".needs-validation").removeClass('was-validated');
            });
            $("#modal_add_product_cost").keyup(function () {
                var $this = $(this);
                $this.val($this.val().replace(/[^\d.]/g, ''));
            });
        }
    },
    proveedores = {
        ini: function () {
            $this = this;
            $this.t_table_proveedores = $("#t_suppliers").DataTable({
                    "order": [
                        [1, "asc"]
                    ],
                    "columnDefs": [{
                        "width": "10%",
                        "targets": 0
                    }]
                }),
                $this.get_lista_proveedores();
            $this.set_events();
        },
        get_lista_proveedores: function () {
            let data = {
                tipo: 1,
                view: "proveedores",
                id_sucursal: sessionStorage.getItem("medicos_sucursal")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_proveedores = json;
                $this.t_table_proveedores.clear().draw();
                let cont = 0;
                $.each(json, function (index, value) {
                    $this.t_table_proveedores.row.add([value.idproveedor, value.nombre, value.direccion, value.ciudad, value.contacto, value.mail, value.usuario,
                        `<div class='btn-group'>
                        <button type="button" class="btn btn-primary btn-sm"
                        onclick='proveedores.editar_proveedor(${index})'>
                            <i class="fas fa-edit fa-lg"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm"
                        onclick='proveedores.eliminar_proveedor(${index})'>
                            <i class="far fa-trash-alt fa-lg"></i>
                        </button>
                        </div>`
                    ]);
                });
                $this.t_table_proveedores.draw();
            }, "json").done(function () {
                // $this.get_lista_de_productos();
            });
        },
        save_provider: function (info) {
            data = {
                tipo: 2,
                view: "proveedores",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_proveedores();
                    $("#modal_add_providers").modal('hide');
                }
            }, "json").done(function () {
                $("#form_add_providers").find("[type=submit]").removeAttr("disabled").html("SAVE");
                $(".needs-validation").removeClass('was-validated');
                $("#form_add_providers")[0].reset();
            });
        },
        editar_proveedor: function (id) {
            console.log($this.lista_de_proveedores[id]);
            $this.id_proveedor = id;
            $("#modal_edit_provider_nombre").val($this.lista_de_proveedores[id].nombre);
            $("#modal_edit_provider_direccion").val($this.lista_de_proveedores[id].direccion);
            $("#modal_edit_provider_city").val($this.lista_de_proveedores[id].ciudad);
            $("#modal_edit_provider_contacto").val($this.lista_de_proveedores[id].contacto);
            $("#modal_edit_provider_mail").val($this.lista_de_proveedores[id].mail);
            $id_city_ref = $this.lista_de_proveedores[id].idciudad;
            $this.get_cities();
            ini.modal.show("modal_edit_providers", $this.update_data_provider, "#form_edit_providers");
        },
        update_data_provider: function () {
            let data = {
                tipo: 3,
                view: "proveedores",
                info: $("#form_edit_providers").serializeArray(),
                id: $this.id_proveedor,
                idciudad: $id_city_ref
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    $this.get_lista_proveedores();
                    ini.modal.close();
                }
            }, "json");
        },
        eliminar_proveedor: function (id) {
            if (confirm("Are you sure you're removing this product?")) {
                let data = {
                    tipo: 4,
                    view: "proveedores",
                    id: id
                }
                $.post(ini.url, data, function (json) {
                    if (typeof (json) == 'string') {
                        console.log(json);
                        return false;
                    }
                    if (json.estado) {
                        $this.get_lista_proveedores();
                    }
                }, "json");
            }
        },
        get_cities: function () {
            let data = {
                tipo: 4,
                view: "clinicas"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                let $array_temp = [];
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.name;
                    $array_temp.push($mini_array);
                });
                $("#modal_add_provider_city,#modal_edit_provider_city").typeahead('destroy');
                $('#modal_add_provider_city,#modal_edit_provider_city').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#modal_add_provider_city,#modal_edit_provider_city").val("");
                            $id_city_ref = 0;
                            $("#form_add_providers,#form_edit_providers").find(".btn-primary").attr("disabled", "disabled");
                        } else {
                            $id_city_ref = parseInt(e.value);
                            $("#form_add_providers,#form_edit_providers").find(".btn-primary").removeAttr("disabled");
                        }
                    }
                });
            }, "json");
        },
        set_events: function () {
            $("#form_add_providers").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($("#form_add_providers.needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    $("#form_add_providers").find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                    $info = {
                        name: $("#modal_add_provider_nombre").val(),
                        address: $("#modal_add_provider_direccion").val(),
                        city: $id_city_ref,
                        contact: $("#modal_add_provider_contacto").val(),
                        mail: $("#modal_add_provider_mail").val(),
                        user: sessionStorage.getItem("iduser")
                    }
                    $this.save_provider($info);
                }
            });
            $("#form_add_providers").find("button[type=button]").click(function () {
                $("#form_add_providers")[0].reset();
                $(".needs-validation").removeClass('was-validated');
            });
            $("[data-target='#modal_add_providers']").click(() => {
                $this.get_cities();
            });
        },
    },
    compras = {
        ini: function () {
            $this = this;
            $this.t_table_purchase_products = $("#t_purchase_products").DataTable({
                    "order": [
                        [1, "asc"]
                    ],
                    "columnDefs": [{
                        "width": "50px",
                        "targets": 1
                    }, ],
                    "searching": false,
                    "lengthChange": false,
                    "bInfo": false,
                    "paging": false,
                    "ordering": false,
                    "info": false
                }),
                $this.t_to_table_purchase_products = $("#t_to_purchase_products").DataTable({
                    "order": [
                        [1, "asc"]
                    ],
                    "columnDefs": [{
                        "width": "20px",
                        "targets": 0
                    }]
                }),
                // $this.get_lista_proveedores();
                $this.set_events();
            $this.n_row = 0;
        },
        get_lista_proveedores: function () {
            let data = {
                tipo: 1,
                view: "proveedores",
                id_sucursal: sessionStorage.getItem("medicos_sucursal")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_proveedores = json;
                $this.t_table_proveedores.clear().draw();
                let cont = 0;
                $.each(json, function (index, value) {
                    $this.t_table_proveedores.row.add([value.idproveedor, value.nombre, value.direccion, value.ciudad, value.contacto, value.mail, value.usuario,
                        `<div class='btn-group'>
                        <button type="button" class="btn btn-primary btn-sm"
                        onclick='proveedores.editar_proveedor(${index})'>
                            <i class="fas fa-edit fa-lg"></i>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm"
                        onclick='proveedores.eliminar_proveedor(${index})'>
                            <i class="far fa-trash-alt fa-lg"></i>
                        </button>
                        </div>`
                    ]);
                });
                $this.t_table_proveedores.draw();
            }, "json").done(function () {
                // $this.get_lista_de_productos();
            });
        },
        save_provider: function (info) {
            data = {
                tipo: 2,
                view: "proveedores",
                info: info
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json.estado) {
                    $this.get_lista_proveedores();
                    $("#modal_add_providers").modal('hide');
                }
            }, "json").done(function () {
                $("#form_add_providers").find("[type=submit]").removeAttr("disabled").html("SAVE");
                $(".needs-validation").removeClass('was-validated');
                $("#form_add_providers")[0].reset();
            });
        },
        editar_proveedor: function (id) {
            console.log($this.lista_de_proveedores[id]);
            $this.id_proveedor = id;
            $("#modal_edit_provider_nombre").val($this.lista_de_proveedores[id].nombre);
            $("#modal_edit_provider_direccion").val($this.lista_de_proveedores[id].direccion);
            $("#modal_edit_provider_city").val($this.lista_de_proveedores[id].ciudad);
            $("#modal_edit_provider_contacto").val($this.lista_de_proveedores[id].contacto);
            $("#modal_edit_provider_mail").val($this.lista_de_proveedores[id].mail);
            $id_city_ref = $this.lista_de_proveedores[id].idciudad;
            $this.get_cities();
            ini.modal.show("modal_edit_providers", $this.update_data_provider, "#form_edit_providers");
        },
        update_data_provider: function () {
            let data = {
                tipo: 3,
                view: "proveedores",
                info: $("#form_edit_providers").serializeArray(),
                id: $this.id_proveedor,
                idciudad: $id_city_ref
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                if (json) {
                    $this.get_lista_proveedores();
                    ini.modal.close();
                }
            }, "json");
        },
        eliminar_proveedor: function (id) {
            if (confirm("Are you sure you're removing this product?")) {
                let data = {
                    tipo: 4,
                    view: "proveedores",
                    id: id
                }
                $.post(ini.url, data, function (json) {
                    if (typeof (json) == 'string') {
                        console.log(json);
                        return false;
                    }
                    if (json.estado) {
                        $this.get_lista_proveedores();
                    }
                }, "json");
            }
        },
        get_supplier: function () {
            let data = {
                tipo: 1,
                view: "proveedores"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                let $array_temp = [];
                $id_proveedor = 0;
                $.each(json, function (index, value) {
                    let $mini_array = [];
                    $mini_array['id'] = index;
                    $mini_array['name'] = value.nombre;
                    $array_temp.push($mini_array);
                });
                $("#modal_add_shoping_supplier").typeahead('destroy');
                $('#modal_add_shoping_supplier').typeahead({
                    source: $array_temp,
                    display: 'value',
                    onSelect: function (e, dato) {
                        if (e.value == "-21") {
                            $("#modal_add_shoping_supplier").val("");
                            $id_proveedor = 0;
                        } else {
                            $id_proveedor = parseInt(e.value);
                        }
                    }
                });
            }, "json");
        },
        get_typepayment: function () {
            let data = {
                tipo: 5,
                view: "compras"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                // let $array_temp = [];
                // $id_proveedor = 0;
                let html = '';
                $.each(json, function (index, value) {
                    html += `<option value="${index}">${value.name}</option>`
                });
                $('#modal_add_shoping_payment_method').html(html);
            }, "json");
        },
        get_lista_productos: function () {
            let data = {
                tipo: 1,
                view: "productos",
                id_sucursal: sessionStorage.getItem("medicos_sucursal")
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_de_productos = json;
                $this.t_to_table_purchase_products.clear().draw();
                let cont = 0;
                $.each(json, function (index, value) {
                    $this.t_to_table_purchase_products.row.add([++cont, value.clave, value.producto, value.costo_ultimo,
                        `<div class='btn-group'>
                            <button type="button" class="btn btn-primary btn-sm" onclick='compras.add_to_purchase_products(${index})'>
                                <i class="fas fa-check fa-lg"></i>
                            </button>
                        </div>`
                    ]);
                });
                $this.t_to_table_purchase_products.draw();
            }, "json");
        },
        add_to_purchase_products: function (id) {
            $('#modal_show_products').modal('hide');
            $this.t_table_purchase_products.row.add([
                $this.lista_de_productos[id].clave,
                $this.lista_de_productos[id].producto,
                `<input type="number" class="form-control form-control-sm bg-white maspq" style="width: auto;" min="1" id="modal_add_shoping_product_quantity${$this.n_row}" value="1">`,
                $this.lista_de_productos[id].costo_ultimo,
                $this.lista_de_productos[id].costo_ultimo,
                $this.lista_de_productos[id].impuesto, ''
            ]).draw(false);
            $this.set_total();
            $this.n_row++;
        },
        set_events: function () {
            $("#form_add_shoping").unbind("submit").on("submit", function (e) {
                e.preventDefault();
                if ($("#form_add_shoping.needs-validation")[0].checkValidity() === false) {
                    $(".needs-validation").addClass('was-validated');
                } else {
                    console.log($this.t_table_purchase_products.rows().data().toArray());
                    console.log($("#form_add_shoping").serializeArray());
                    // $("#form_add_shoping").find("[type=submit]").attr("disabled", "disabled").append(" <i class='fas fa-spinner fa-spin'></i>");
                    // $info = {
                    //     name: $("#modal_add_provider_nombre").val(),
                    //     address: $("#modal_add_provider_direccion").val(),
                    //     city: $id_city_ref,
                    //     contact: $("#modal_add_provider_contacto").val(),
                    //     mail: $("#modal_add_provider_mail").val(),
                    //     user: sessionStorage.getItem("iduser")
                    // }
                    // $this.save_provider($info);
                }
            });
            $("#form_add_shoping").find("button[type=button]").click(function () {
                if ($(this).attr("id") !== "add_product_onpurchase") {
                    $("#form_add_shoping")[0].reset();
                    $(".needs-validation").removeClass('was-validated');
                    $this.t_table_purchase_products.clear().draw();
                    $this.n_row = 0;
                }
            });
            $("[data-target='#modal_add_shoping']").click(() => {
                $this.get_supplier();
                $this.get_typepayment();
                let date = moment(new Date()).format("YYYY/MM/DD");
                date = date.replace('/', '-');
                date = date.replace('/', '-');
                $('#modal_add_shoping_date').prop("disabled", false);
                $('#modal_add_shoping_date').val(date);
                $('#modal_add_shoping_date').prop("disabled", true);
            });
            $("#add_product_onpurchase").click(() => {
                $this.get_lista_productos();
                $('#modal_show_products').modal('show');
            });

            $('#t_purchase_products tbody').on('change', 'td', function () {
                let avocato = $this.t_table_purchase_products;
                let cell = avocato.cell(this);
                let count = document.getElementById('modal_add_shoping_product_quantity' + cell.index().row).value;
                let amount = avocato.cell({
                    row: cell.index().row,
                    column: 3
                }).data();
                avocato.cell({
                    row: cell.index().row,
                    column: 4
                }).data(count * amount).draw();
                $this.set_total();
            });
            $('#t_purchase_products tbody').on('keyup', 'td', function () {
                let avocato = $this.t_table_purchase_products;
                let cell = avocato.cell(this);
                let count = document.getElementById('modal_add_shoping_product_quantity' + cell.index().row).value;
                let amount = avocato.cell({
                    row: cell.index().row,
                    column: 3
                }).data();
                avocato.cell({
                    row: cell.index().row,
                    column: 4
                }).data(count * amount).draw();
                $this.set_total();
                document.getElementById('modal_add_shoping_product_quantity' + cell.index().row).focus();
            });
            $("#modal_add_shoping_folio_auto,#modal_add_shoping_folio_suplier,#modal_add_shoping_discount").keyup(function () {
                var $this = $(this);
                $this.val($this.val().replace(/[^\d.]/g, ''));
            });
            $('#modal_add_shoping_discount').keyup(function () {
                $this.set_total();
            });
            $('#modal_add_shoping_supplier_serie').keyup(function () {
                var $this = $(this);
                $this.val($this.val().replace(' ', ''));
                $this.val($this.val().toUpperCase());
            });
        },
        set_total: function () {
            let total = 0;
            let impuesto = 0;
            $this.t_table_purchase_products.rows().data().each(function (el, index) {
                total += parseFloat(el[4]);
                impuesto += parseFloat(el[4]) * parseFloat(el[5] / 100);
            });
            $('#modal_add_shoping_import').val(total);
            $('#modal_add_shoping_tax').val(impuesto);
            let descuento = parseFloat($('#modal_add_shoping_discount').val());
            isNaN(descuento) ? descuento = 0 : descuento;
            total = (total + impuesto) - descuento
            $('#modal_add_shoping_total').val(total.toFixed(2));
        }
    },
    openTok = {
        ini: function(){
            var init = 0;
            $this = this;
            $this.t_appointments = $('#t_appointments').DataTable();
            $this.get_lista_cita();
        },
        llenar_tabla_cita: function () {
            let rows = [];
            $.each($this.lista_cita, function (index, value) {
                console.log($this.lista_cita);
                console.log(index);
                let columns = [];
                columns.push(value.nombre);
                columns.push(value.servicio);
                columns.push(value.finicio);
                columns.push(value.ffinal);
                columns.push("<span class='badge badge-light'>"+value.estado_cita+"</span>");
                columns.push("<div class='btn btn-primary btn-sm float-left mr-1 m-0 py-1 px-3' onclick='openTok.lienar_start_Video(\""+ value.open_tok_session+"\", \""+ value.id+"\")'><i class='fas fa-video'></i></div>");
                rows.push(columns);
            });
            $this.t_appointments.clear().rows.add(rows).draw();
        },
        lienar_start_Video: function(sessionId, id){
            $("#cita-list").hide();
            let data = {
                tipo: 3,
                view: "opentok",
                sessionId: sessionId,
                citaId: id
            }
            let publisherId = "";
            let subscribeId = "";
            $.post(ini.url, data, function (json) {
                json = JSON.parse(json);
                if(json.error){
                    $("#cita-list").show();
                    alert(json.message);
                    return;
                }
                var publisher;
                let videoShare = "";
                var connectionCount = 0;
                var apiKey = json.apiKey;
                var sessionId = json.sessionId;
                var token = json.token;
                

                // Handling all of our errors here by alerting them
                function handleError(error) {
                  if (error) {
                    alert(error.message);
                  }
                }

                // (optional) add server code her
                var session = OT.initSession(apiKey, sessionId);

                // Subscribe to a newly created stream
                session.on('streamCreated', function(event) {
                    console.log("streamCreated");
                    console.log("streamCreated", event);
                    var id = "subscriber";
                    if(event.stream.videoType == "screen"){
                        var $div = $("<div>", {id: "screen-share"});
                        $( "#screen-videos" ).append($div);
                        var id = "screen-share";
                        $("#subscriber").appendTo("#videos");
                        $("#screen-videos").show();
                        session.subscribe(event.stream, id, {
                            width: '100%',
                            height: '100%',
                        }, handleError);
                    }else{
                        session.subscribe(event.stream, id, {
                            insertMode: 'append',
                            width: '100%',
                            height: '100%',
                        }, handleError);
                    }
                });

                // Create a publisher
                publisher = OT.initPublisher('publisher', {
                    insertMode: 'append',
                    width: '100%',
                    height: '140px',
                    audioTrack: true,
                    publishAudio: true,
                    publishVideo: true,
                    videoTrack: true,
                    showControls: true
                }, handleError);

                OT.getDevices(function(error, devices) {
                    devices.filter(function(element) {
                        if(element.kind == "videoInput"){
                            $("#stop-camera").show();
                        }
                    });
                });

                // Connect to the session
                session.connect(token, function(error) {
                    // If the connection is successful, initialize a publisher and publish to the session
                    if (error) {
                      handleError(error);
                    } else {
                      var sessionStart = session.publish(publisher, handleError);
                      $("#start-video-share").show();
                      console.log("publis", sessionStart);

                    }
                });

                session.on("streamDestroyed", function (event) {
                    if(event.stream.videoType == "screen"){
                        $("#subscriber").appendTo("#video-conf");
                        $("#screen-videos").hide();
                        $("#stop-video-share").hide();
                        $("#start-video-share").show();
                    }else{
                        console.log(event.reason, event);
                    }
                });

                session.on("sessionDisconnected", function (event) {
                    event.preventDefault();
                    console.log(event.reason);
                });

                $("body").on("click", "#stop-camera", function(){
                    publisher.publishVideo(false);
                    $("#start-camera").show();
                    $("#stop-camera").hide();
                });

                $("body").on("click", "#start-camera", function(){
                    publisher.publishVideo(true);
                    $("#start-camera").hide();
                    $("#stop-camera").show();
                });
        
                $("body").on("click", "#stop-audio", function(){
                    publisher.publishAudio(false);
                    $("#start-audio").show();
                    $("#stop-audio").hide();
                });

                $("body").on("click", "#start-audio", function(){
                    publisher.publishAudio(true);
                    $("#start-audio").hide();
                    $("#stop-audio").show();
                });
                
                $("body").on("click", "#start-video-share", function(){
                    var $div = $("<div>", {id: "screen-share"});
                    $( "#screen-videos" ).append($div);
                    videoShare = OT.initPublisher("screen-share", {
                        width: '100%',
                        height: '100%',
                        videoSource: 'window'
                    }, handleError);
                    session.publish(videoShare, handleError);
                    $("#subscriber").appendTo("#videos");
                    $("#screen-videos").show();
                    $("#stop-video-share").show();
                    $("#start-video-share").hide();

                    videoShare.on('streamDestroyed', function(event) {
                        if (event.reason === 'mediaStopped') {
                            // User clicked stop sharing
                            console.log("mediaStopped");
                            $("#subscriber").appendTo("#video-conf");
                            $("#screen-videos").hide();
                            $("#stop-video-share").hide();
                            $("#start-video-share").show();
                        } else if (event.reason === 'forceUnpublished') {
                            // A moderator forced the user to stop sharing.
                            console.log("forceUnpublished");
                            $("#subscriber").appendTo("#video-conf");
                            $("#screen-videos").hide();
                            $("#stop-video-share").hide();
                            $("#start-video-share").show();
                        }
                    });
                });
                $("body").on("click", "#stop-video-share", function(){
                    session.unpublish(videoShare);
                    $("#stop-video-share").hide();
                    $("#start-video-share").show();
                    $("#subscriber").appendTo("#video-conf");
                    $("#screen-videos").hide();
                });

                $("[key-select-menu]").on("click", function () {
                    session.disconnect();
                });

                $("#end-call").on("click", function () {
                    $rs = confirm("Are you sure you want to end this call?");
                    if($rs){
                        session.disconnect();
                        window.location.href = "/";
                    }
                });
                $("#main-video-conf").show();
            });

        },
        get_lista_cita: function () {
            let data = {
                tipo: 2,
                view: "opentok"
            }
            $.post(ini.url, data, function (json) {
                if (typeof (json) == 'string') {
                    console.log(json);
                    return false;
                }
                $this.lista_cita = json;
            }, 'json').done(function () {
                $this.llenar_tabla_cita();
            });
        }
    },
    public = {
        ini: function(){
            let params = (new URL(document.location)).searchParams;
            let id = params.get("id");
            let sessionId = params.get("sessionId");
            openTok.lienar_start_Video(sessionId, id);
        }
    }
})()