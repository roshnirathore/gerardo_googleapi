<?php
/*ZAPIER EXPERT'S CODE START*/
/*This code for generate the session id and Token for opentok video conferance*/
require "openTok/vendor/autoload.php";
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
class customOpenTok extends ini{
    protected  $init;
    public $apiKey, $apiSecret, $sessionId;
    function __construct() {       
        // $this->apiKey = "46483402";
        // $this->apiSecret = "f4303bf2787600aa352845086a4e14955be59a56";  
        $this->apiKey = "46508382";
        $this->apiSecret = "c6c291025b0b8a1db3d21f1c1088fca809850129"; 
        $this->init = new OpenTok($this->apiKey, $this->apiSecret);
    }
    function generateSession(){
        $opentok = $this->init;
        $session = $opentok->createSession();
        // A session that uses the OpenTok Media Router, which is required for archiving:
        $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));
        // A session with a location hint:
        $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));
        // An automatically archived session:
        $sessionOptions = array(
            'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
        );
        $session = $opentok->createSession($sessionOptions);
        return $this->sessionId = $session->getSessionId();
    }
    function genrateToken($sessionId = ""){
        if(empty($sessionId)){
            $$sessionId = $this->generateSession();
        }else{
            $this->sessionId = $sessionId;
        }
        return $this->init->generateToken($sessionId);        
    }
}
/*ZAPIER EXPERT'S CODE END*/

class clinicas extends ini {
    function save_editar_clinica($name_clinic,$idciudad,$id_clinica){
        if($this->validar_existe_sucursal_editar($name_clinic,$idciudad,$id_clinica)){
            $co['estado'] = false;
            $co['existente'] = true;
        }else{
            $sql = "UPDATE sucursal SET sucursal='".$this->str($name_clinic)."', idciudad=$idciudad WHERE idsucursal=$id_clinica";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
            if(isset($co)){return $co;}
        }
        if(isset($co)){return $co;}
    }
    function validar_existe_sucursal_editar($name_clinic,$idciudad,$id_clinica){
        $sql = "SELECT * FROM sucursal WHERE sucursal='".$this->str($name_clinic)."' AND idciudad=$idciudad AND idsucursal <> $id_clinica";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_clinica($name_clinic,$idciudad){
        if($this->validar_existe_sucursal($name_clinic,$idciudad)){
            $co['estado'] = false;
            $co['existente'] = true;
        }else{
            $sql = "INSERT INTO sucursal VALUES (0,'".$name_clinic."',$idciudad)";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
        }
        if(isset($co)){return $co;}
    }
    function validar_existe_sucursal($name_clinic,$idciudad){
        $sql = "SELECT * FROM sucursal WHERE sucursal='".$this->str($name_clinic)."' AND idciudad=$idciudad";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_de_clinicas(){
        $sql = "SELECT sucursal.idsucursal, sucursal.sucursal, sucursal.idciudad , ciudad.ciudad  FROM (sucursal JOIN ciudad) WHERE ((sucursal.idciudad = ciudad.idciudad)) ;";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idciudad']]['name'] = utf8_encode($fila['ciudad']);
            $co[$fila['idciudad']]['sucursales'][$fila['idsucursal']] = utf8_encode($fila['sucursal']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}
    
class config_ciudades extends ini {
    function save_new_city($name,$estado){
        if($this->validar_existe_ciudad($name,$estado)){
            $co['estado'] = false;
            $co['existente'] = true;
        }else{
            $sql = "INSERT INTO ciudad VALUES (NULL,'".$name."',$estado)";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
        }
        if(isset($co)){return $co;}
    }
    function validar_existe_ciudad($name,$estado){
        $sql = "SELECT * FROM ciudad WHERE ciudad='".$this->str($name)."' AND idestado=$estado";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = true;
        }
        mysqli_close($this->conexion); 
        if(isset($co)){return $co;}
    }
    function get_datos_input(){
        $co['paises'] = $this->get_lista_paises();
        if(isset($co)) return $co;
    }
    function get_lista_paises(){
        $sql = "SELECT * FROM pais";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idpais']]['name']   = utf8_encode($fila['pais']);
            $co[$fila['idpais']]['estados']   = $this->get_lista_estados();
            $co[$fila['idpais']]['idpais']   = $fila['idpais'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_estados(){
        $sql = "SELECT * FROM estado";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idestado']]  = utf8_encode($fila['estado']);
        }
        if(isset($co)){return $co;}
    }
    function get_all_ciudades(){
        $sql = "SELECT pais.idpais AS idpais,
            pais.pais AS pais,
            estado.idestado AS idestado,
            estado.estado AS estado,
            ciudad.idciudad AS idciudad,
            ciudad.ciudad AS ciudad
        FROM
            ((pais
            JOIN estado)
            JOIN ciudad)
        WHERE
            ((estado.idpais = pais.idpais)
                AND (ciudad.idestado = estado.idestado))";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idciudad']]['ciudad']    = utf8_encode($fila['ciudad']);
            $co[$fila['idciudad']]['estado']    = utf8_encode($fila['estado']);
            $co[$fila['idciudad']]['pais']      = utf8_encode($fila['pais']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}
    
class reports extends ini {
    function get_issue_invoices(){
        $co['invoice'] = $this->get_invoice_month();
        $co['client_month'] = $this->get_client_of_month();
        $co['appointment'] = $this->appointment_month();
        $co['closed_invoice'] = $this->closed_invoice();
        $co['raised_invoice'] = $this->raised_invoice();
        if(isset($co)) return $co;
    }

    function raised_invoice(){
        $id_user =  $_SESSION['id_user'];
        $sql = "SELECT * FROM v_paciente WHERE idusuario = '$id_user'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);

        while ($fila1 = mysqli_fetch_array($res)) {
            $idpaciente= $fila1['idpaciente'];
            $pacientesql = "SELECT COUNT(servicio) as session_no,SUM(importe) as amount, SUM(impuesto) as tax ,servicio FROM v_invoice WHERE idpaciente = '$idpaciente'  GROUP BY servicio HAVING COUNT(*) > 1 ";
         
            $pacientres = mysqli_query($this->conexion,$pacientesql);
            //$fila =  mysqli_fetch_array($pacientres);
            while ($fila=mysqli_fetch_array($pacientres)) {
                $co[$idpaciente]['therapy_type'] = utf8_encode($fila['servicio']);
                $co[$idpaciente]['amount'] = utf8_encode($fila['amount']);
                $co[$idpaciente]['tax'] = utf8_encode($fila['tax']);
                $co[$idpaciente]['session_no'] = $fila['session_no'];
                $co[$idpaciente]['idpaciente'] = '<div class="btn btn-primary float-right py-0 px-3" onclick="reports.raised_invoice('.$idpaciente.'); "><i class="fas fa-edit"></i></div>';
            }

        }  
        
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_closed_factura($idcita){
        $sql = "SELECT * FROM v_facturas_emitidas WHERE Cita=$idcita";
        $co['idestado'] = 3;
        $co['estado']   = "pending";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['estado']   = $fila['estado'];
            $co['idestado'] = $fila['idestadofactura'];
            $co['idforma_pago'] = $fila['idforma_pago'];
        }
        if(isset($co)){return $co;}
    }

    function closed_invoice(){
        $id_user =  $_SESSION['id_user'];
        $sql = "SELECT * FROM v_paciente WHERE idusuario = '$id_user'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);

        while ($fila1 = mysqli_fetch_array($res)) {
            $idpaciente= $fila1['idpaciente'];
             $pacientesql = "SELECT * FROM v_invoice WHERE idpaciente = '$idpaciente'";
            $pacientres = mysqli_query($this->conexion,$pacientesql);
           //echo $rowcount=mysqli_num_rows($pacientres);

            while ($fila=mysqli_fetch_array($pacientres)) {
                $id_cita = $fila['idcita'];
                $datos_estado_factura = $this->get_closed_factura($id_cita);
                
                if($datos_estado_factura['idestado'] == 1){

                    $co[$fila['idcita']]['therapy_type'] = utf8_encode($fila['servicio']);
                    $co[$fila['idcita']]['amount'] = utf8_encode($fila['importe']);
                    $co[$fila['idcita']]['tax'] = utf8_encode($fila['impuesto']);
                    $co[$fila['idcita']]['date'] = $fila['fecha_ini'];

                     /*$pacientesql = "SELECT COUNT(servicio) as session_no,SUM(importe) as amount, SUM(impuesto) as tax ,servicio FROM v_invoice WHERE idpaciente = '$idpaciente'  GROUP BY servicio ";
         
                    $pacientres = mysqli_query($this->conexion,$pacientesql);
                
                    while ($fila=mysqli_fetch_array($pacientres)) {
                        $co[$idpaciente]['therapy_type'] = utf8_encode($fila['servicio']);
                        $co[$idpaciente]['amount'] = utf8_encode($fila['amount']);
                        $co[$idpaciente]['tax'] = utf8_encode($fila['tax']);
                        $co[$idpaciente]['session_no'] = $fila['session_no'];
                        
                    }
                    */
                }
                
            }

        }  
         
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function appointment_month(){
        $sql = "SELECT * FROM v_citas_del_mes";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['fecha']       = $fila['Fecha'];
            $co[$fila['idcita']]['paciente']    = utf8_encode($fila['Paciente']);
           
            $co[$fila['idcita']]['sucursal']    = utf8_encode($fila['sucursal']);
            $co[$fila['idcita']]['servicio']    = utf8_encode($fila['servicio']);
            $co[$fila['idcita']]['medico']      = utf8_encode($fila['medico']);
            $co[$fila['idcita']]['estado']      = utf8_encode($fila['estado']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_client_of_month(){
        $sql = "SELECT * FROM v_clientes_del_mes";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        $cont =0;
        while ($fila=mysqli_fetch_array($res)) {
            $co[$cont]['nombre'] = $fila['nombre'];
            $co[$cont]['fecha'] = $fila['Fecha'];
            $co[$cont]['citas'] = $fila['Citas'];
            $co[$cont]['sucursal'] = $fila['Sucursal'];
            $cont= ($cont+1);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_invoice_month(){
        $sql = "SELECT * FROM v_facturas_emitidas";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['Folio']]['fecha'] = $fila['Fecha'];
            $co[$fila['Folio']]['cita'] = $fila['Cita'];
            $co[$fila['Folio']]['cliente'] = $fila['Cliente'];
            $co[$fila['Folio']]['idpaciente'] = $fila['idpaciente'];
            $co[$fila['Folio']]['total'] = $fila['Total'];
            $co[$fila['Folio']]['sucursal'] = $fila['sucursal'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function get_client_details(){
        /*$sql = "SELECT * FROM  v_invoice WHERE idpaciente = '$idpaciente'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['invoice'] = $fila['idcita'];
            $co[$fila['idcita']]['date'] = $fila['fecha_ini'];
            $co[$fila['idcita']]['client_name'] = $fila['nombre'];
            $co[$fila['idcita']]['amount'] = $fila['importe'];
            $co[$fila['idcita']]['tax'] = $fila['impuesto'];
            $id_cita = $fila['idcita'];

            $facturasql = "SELECT * FROM  v_facturas_emitidas WHERE cita = '$id_cita'";
            $facturares = mysqli_query($this->conexion,$facturasql);
            $fila1 =mysqli_fetch_array($facturares);
            if($fila1){
               $co[$fila['idcita']]['invoice_paid_date'] = $fila1['Fecha'];  
           }else{
            $co[$fila['idcita']]['invoice_paid_date'] = '';
           }
           
        }*/

        $id_user =  $_SESSION['id_user'];
        $sql = "SELECT * FROM v_paciente WHERE idusuario = '$id_user'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);

        while ($fila1 = mysqli_fetch_array($res)) {
            $idpaciente= $fila1['idpaciente'];
            $pacientesql = "SELECT * FROM  v_invoice WHERE idpaciente = '$idpaciente'";
         
            $pacientres = mysqli_query($this->conexion,$pacientesql);
      
            while ($fila=mysqli_fetch_array($pacientres)) {
                $co[$fila['idcita']]['invoice'] = $fila['idcita'];
                $co[$fila['idcita']]['date'] = $fila['fecha_ini'];
                $co[$fila['idcita']]['client_name'] = $fila['nombre'];
                $co[$fila['idcita']]['amount'] = $fila['importe'];
                $co[$fila['idcita']]['tax'] = $fila['impuesto'];
                $id_cita = $fila['idcita'];

                $facturasql = "SELECT * FROM  v_facturas_emitidas WHERE cita = '$id_cita'";
                $facturares = mysqli_query($this->conexion,$facturasql);
                $fila2 =mysqli_fetch_array($facturares);

                if($fila2){
                   $co[$fila['idcita']]['invoice_paid_date'] = $fila2['Fecha'];  
               }else{
                $co[$fila['idcita']]['invoice_paid_date'] = '';
               }
            }
        }  
        
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    function raised_invoice_filter($from_date='',$to_date=''){

        $id_user =  $_SESSION['id_user'];
        $sql = "SELECT * FROM v_paciente WHERE idusuario = '$id_user'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);

        while ($fila1 = mysqli_fetch_array($res)) {
            $idpaciente= $fila1['idpaciente'];
            $invoiceSql = "SELECT * FROM  v_invoice WHERE idpaciente = '$idpaciente' AND  DATE(fecha_ini) between '$from_date' and '$to_date'";
            $invoiceRes = mysqli_query($this->conexion,$invoiceSql);

            while ($fila=mysqli_fetch_array($invoiceRes)) {
                $co[$fila['idcita']]['invoice'] = $fila['idcita'];
                $co[$fila['idcita']]['date'] = $fila['fecha_ini'];
                $co[$fila['idcita']]['client_name'] = $fila['nombre'];
                $co[$fila['idcita']]['amount'] = $fila['importe'];
                $co[$fila['idcita']]['tax'] = $fila['impuesto'];
                $id_cita = $fila['idcita'];

                $facturasql = "SELECT * FROM  v_facturas_emitidas WHERE cita = '$id_cita'";
                $facturares = mysqli_query($this->conexion,$facturasql);
                $fila1 =mysqli_fetch_array($facturares);
                if($fila1){
                   $co[$fila['idcita']]['invoice_paid_date'] = $fila1['Fecha'];  
               }else{
                $co[$fila['idcita']]['invoice_paid_date'] = '';
               }
               
            }
        }  
    
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class invoices extends ini {
    function download_invoice_pdf($id_invoice,$cliente_name_edit,$dompdf){
        $info = $this->get_data_only_download_invoice($id_invoice);
        $co['file'] = $this->create_file_pdf($id_invoice,$cliente_name_edit,$dompdf,$info[$id_invoice]);
        if(isset($co)) return $co;
    }
    function get_data_only_download_invoice($id_invoice){
        $sql = "SELECT * FROM v_invoice WHERE idcita=$id_invoice";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['nombre']          = utf8_encode($fila['nombre']);
            $co[$fila['idcita']]['idpaciente']      = utf8_encode($fila['idpaciente']);
            $co[$fila['idcita']]['sucursal']        = utf8_encode($fila['sucursal']);
            //$co[$fila['idcita']]['estado']          = utf8_encode($fila['estado_cita']);
            $co[$fila['idcita']]['servicio']        = utf8_encode($fila['servicio']);
            $co[$fila['idcita']]['medico']          = utf8_encode($fila['medico']);
            //$co[$fila['idcita']]['namebussines']    = utf8_encode($fila['bussines_name']);
            $co[$fila['idcita']]['email']           = utf8_encode($fila['email']);
            $co[$fila['idcita']]['importe']         = utf8_encode($fila['importe']);
            $co[$fila['idcita']]['impuesto']        = utf8_encode($fila['impuesto']);
            $co[$fila['idcita']]['serie']           = utf8_encode($fila['serie']);
            $co[$fila['idcita']]['folio']           = utf8_encode($fila['folio']);
            // $co[$fila['idcita']]['fecha']           = utf8_encode($fila['fecha']);
            $datos_estado_factura                   = $this->get_datos_factura($fila['idcita']);
            $co[$fila['idcita']]['estadofactura']   = $this->get_label_estado_factura($datos_estado_factura);
            if($fila['imagen'] != NULL){
                $co[$fila['idcita']]['imagen']           = utf8_encode($fila['imagen']);    
            }else{
                $co[$fila['idcita']]['imagen']           = "default.png";
            }
            $co[$fila['idcita']]['idestadoinvoice']      = $datos_estado_factura['idestado'];
            $co[$fila['idcita']]['idforma_pago']      = $datos_estado_factura['idforma_pago'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_label_estado_factura($estado_factura){
        switch ($estado_factura['idestado']) {
            case '1':
                return '<span class="badge badge-pill badge-default">'.$estado_factura['estado'].'</span>';
                break;
            case '2':
                return '<span class="badge badge-pill badge-danger">'.$estado_factura['estado'].'</span>';
                break;
            case '3':
                return '<span class="badge badge-light">'.$estado_factura['estado'].'</span>';
                break;
        }
    }
    function get_datos_factura($idcita){
        $sql = "SELECT * FROM v_facturas_emitidas WHERE Cita=$idcita";
        $co['idestado'] = 3;
        $co['estado']   = "pending";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['estado']   = $fila['estado'];
            $co['idestado'] = $fila['idestadofactura'];
            $co['idforma_pago'] = $fila['idforma_pago'];
        }
        if(isset($co)){return $co;}
    }
    function create_file_pdf($id_cita,$cliente_name_edit,$dompdf,$info){
        /*echo "string"."<br>".$cliente_name_edit;
        echo "string1"."<br>".$info['nombre']."string1";*/
        if ($cliente_name_edit != null) {
            $client_name = $cliente_name_edit;
        }else{
             $client_name = $info['nombre'];
        }

        //echo $info['medico']."--".$info['idestadoinvoice']."--".$info['idforma_pago'];
        // $dompdf->loadHtml($this->get_html($id_cita,$info,$info['medico'],$info['idestadoinvoice'],$info['idforma_pago']));
        $dompdf->loadHtml($this->get_html($id_cita,$info,$client_name,$info['idestadoinvoice'],1));

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        //$dompdf->stream();
        $output = $dompdf->output();
        if(file_put_contents('../files/'.$_SESSION['id_user']."_$id_cita".'_invoice_download.pdf', $output)){
            return $_SESSION['id_user']."_$id_cita".'_invoice_download.pdf';
        }else{
            return false;
        }
    }
    function insert_new_cliente($name,$date,$address,$city,$phone,$email){
        $sql = "INSERT INTO cliente VALUES (0,'".$this->str($name)."','$date','".$this->str($address)."',$city,'$phone','".$this->str($email)."',NOW(),".$_SESSION['id_user'].")";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
                $co['id_client'] = mysqli_insert_id($this->conexion);
            }else{
                $co['estado']=false;
            }
            mysqli_close($this->conexion);
            if(isset($co)){return $co;}
    }
    function get_datos_input(){
        $co['clientes'] = $this->get_lista_clientes();
        $co['ciudades'] = $this->get_list_ciudades();
        $co['estadofactura'] = $this->estadofacturas();
        $co['typepayment']  = $this->get_typepayment();
        return $co;
    }
    function get_typepayment(){
        $sql = "SELECT * FROM forma_pago";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idforma_pago']]['name'] = utf8_encode($fila['forma_pago']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function estadofacturas(){
        $sql = "SELECT * FROM estadofactura";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idestadofactura']]['name'] = utf8_encode($fila['estado']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_list_ciudades(){
        $sql = "SELECT * FROM ciudad";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idciudad']]['name'] = utf8_decode($fila['ciudad']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_clientes(){
        $sql = "SELECT * FROM cliente";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcliente']]['name'] = utf8_encode($fila['nombre']);
            $co[$fila['idcliente']]['email'] = utf8_encode($fila['email']);
        }
        mysqli_close($this->conexion);
        if(isset($co)) return $co;
    }
    function send_invoice($id_cita,$dompdf,$mail,$info,$id_cliente,$email_cliente,$client_name,$idestadoinvoice,$idtype_payment,$edit_client_name){
        $file =  $this->generar_pdf($id_cita,$dompdf,$info,$client_name,$idestadoinvoice,$idtype_payment,$edit_client_name);
        
        $co['estado'] = $this->send_invoice_mail($id_cita,$file,$mail,$email_cliente,$info);
        if($co['estado']==true){
            $co['estado_factura'] = $this->save_registro_factura($id_cita,$info,$id_cliente,$idestadoinvoice,$idtype_payment);
        }
        
        return $co;
    }
    function save_registro_factura($id_cita,$info,$id_cliente,$idestadoinvoice,$idtype_payment){
        if($this->existe_registro_factura($id_cita)){
            $importe  = $info['importe'];
            $impuesto = $info['impuesto'];
            $iva      = ($impuesto/100) * $importe;
            $total      = ($iva+$importe);
            $sql = "INSERT INTO factura VALUES (0,'".$info['fecha']."','".$info['serie']."',".$info['folio'].",$id_cita,$id_cliente,".$info['idpaciente'].",".$_SESSION['id_user'].",$idtype_payment,$total,$idestadoinvoice)";
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $co['estado_registro_reporte'] = 1;
                $co['estado_detalle_factura'] = $this->insert_detalle_factura($info['servicio'],1,$total,mysqli_insert_id($this->conexion));
            }else{
                $co['estado_registro_reporte']=0;
            }
            mysqli_close($this->conexion);
            if(isset($co)){return $co;}
        }else{
            $co['estado_registro_reporte'] = 2;
        }
        if(isset($co)) return $co;
    }
    function insert_detalle_factura($concepto,$cantidad,$precio,$id_factura){
        $sql = "INSERT INTO detallefactura VALUES (0,'".$this->str($concepto)."',$cantidad,$precio,$id_factura)";
        if(mysqli_query($this->conexion,$sql)){
            $co=1;
        }else{
            $co=0;
        }
        if(isset($co)){return $co;}
    }
    function existe_registro_factura($id_cita){
        $sql = "SELECT COUNT(*) AS COUNT FROM factura WHERE idcita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            if($fila['COUNT']>0){
                $co = false;
            }else{
                $co = true;
            }
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function exite_registro_factura($id_cita){
    }
    function send_invoice_mail($id_cita,$file,$mail,$user_email,$info){
       
        try{
            // $mail->SMTPDebug = 1;
            $mail->isSMTP();
            $mail->Host = "mail.cleverclinic.app";
            $mail->SMTPAuth= true;
            $mail->SMTPSecure = 'ssl';

            $mail->Username = "support@cleverclinic.app";
            $mail->Password = "S4lv4d0r.";

            $mail->Port     = 465;

            $mail->setFrom("support@cleverclinic.app");


            $mail->addAddress($user_email);

            $mail->CharSet = "UTF-8";
            $mail->Subject = "Clever clinic INVOICE";
            // $mail->AddAttachment("../files/Brochure.pdf", 'invoice.pdf'); 
            $mail->Body     = '<table width="100%" style="width:100%;border-spacing:0px" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td><br></td>
                                            <td align="center" width="600">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0px" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:center;font-family:arial,sans-serif;font-size:27px;line-height:27px;font-weight:bold!important;text-transform:uppercase"><h3><b>'.$info['medico'].'</b></h3></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="m_-6181852313919251156smallDevices" style="padding:0px 0px 0px 0px">
                                                                <table width="100%" style="width:100%;border-spacing:0px" border="0" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="text-align:center; padding:20px 40px;font-family:arial,sans-serif;color:#000000;font-size:20px;line-height:28px"><p style="color:#000000">Quintero Psychotherapy professional corporation</p>
                                                                                <p style="margin:10px auto 0px auto">Invoice for $<b>'.round((($info['impuesto']*$info['importe'])/100) + $info['importe']).'</b> due by '.date("M d, Y", strtotime($info['fecha'])).'.</p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>    
                                                        <tr>
                                                            <td>
                                                                <center style="margin:0px 0px 20px 0px"></center>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td><br></td>
                                        </tr>
                                    </tbody>
                                </table>';
            $mail->IsHTML(true);
            //$mail->AddAttachment("../files/19_101_invoice_download.pdf");
           
            if($mail->send()){
                return true;
            }else{
                return false;
            }
        } catch(Exception $ex){
            return $ex->getMessage(); 
        }
    }
    function generar_pdf($id_cita,$dompdf,$info,$client_name,$idestadoinvoice,$idtype_payment,$edit_client_name){
        if ($client_name != $edit_client_name) {
           $client_name = $edit_client_name;
            
        }
        $dompdf->loadHtml($this->get_html($id_cita,$info,$client_name,$idestadoinvoice,$idtype_payment));

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        //$dompdf->stream();
        $output = $dompdf->output();
        if(file_put_contents('../files/Brochure.pdf', $output)){
            return $output;
        }else{
        return false;
        }
    }
    function get_html($id_cita,$info,$client_name,$idestadoinvoice,$idtype_payment){
        $importe = $info['importe'];
        $impuesto = $info['impuesto'];
        $iva      = ($impuesto/100) * $importe;
        $total      = ($iva+$importe);
        // $date = date("Y-m-d",strtotime($info['fecha']));
        // $date = date("Y-m-d",strtotime($this->get_date($id_cita)));
        $date = isset($info['fecha'])?date("Y-m-d",strtotime($info['fecha'])):date("Y-m-d",strtotime($this->get_date($id_cita)));
        $date2 = isset($info['fecha'])?date("M d, Y",strtotime($info['fecha'])):date("M d, Y",strtotime($this->get_date($id_cita)));
        $client = $this->get_name_client($id_cita);
       // $client = ($client!==null)?$client:$client_name;
        $client = $client_name;
        $html = $this->get_style();
        $html .='<html>
            <table style="width:100%;">
                <tr>
                    <td style="text-align:left; width:40%;">
                        <img src="../files/personal_logo/'.$this->get_personal_logo($info['imagen']).'" style="width:300px;">
                    </td>
                    <td style="text-align:right;">
                        <h2 class="wv-heading--title">INVOICE</h2>
                        <div class="wv-heading--subtitle">BN: <b>'.$info['serie'].'</b></div>
                        <span><b>Quintero Psychotherapy professional corporation</b></span>
                
                        <div class="contemporary-template__header__info__address">
                            1436 Royal York Rd.<br>
                            Suite 401<br>
                            Etobicoke,
                            ON M9P3A7<br>
                            Canada<br>
                            <br>
                            <b>'.$info['folio'].'</b><br>
                            <span class="wrappable">gerardoquintero.com<br></span>
                        </div>
                    </td>
                </tr>
            </table>
            <hr>
            <table style="width:100%;">
                <tr>
                    <td style="text-align:left; width:50%;">
                        <label style="color:#cfd8dc">BILL TO</label><br>
                        <label><b>'.$client.'</b></label><br>
                        <label style="color:#cfd8dc">Patient</label><br>
                        <label><b>'.$info['nombre'].'</b></label><br>
                        '.$this->get_status_invoice($idestadoinvoice).'
                    </td>
                    <td class="text-right">
                        <table class="table table-sm ">
                            <tr>
                                <td><b>Invoice Number:</b></td>
                                <td style="text-align:right;">#'.$id_cita.'</td>
                            </tr>
                            <tr>
                                <td><b>Invoice Date:</b></td>
                                <td style="text-align:right;">'.$date.'</td>
                            </tr>
                            <tr>
                                <td><b>Payment Due:</b></td>
                                <td style="text-align:right;">$'.$importe.'</td>
                            </tr>
                            <tr>
                                <td><b>Amount Due (CAD)</b></td>
                                <td style="text-align:right;"><b>$'.$impuesto.'</b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr>
            <table class="table table-sm" style="width:100%;">
                <tr style="background-color:peru; color:white;">
                    <td style="border:0px;">Items</td>
                    <td style="border:0px;">Quantity</td>
                    <td style="border:0px;">Price</td>
                    <td style="border:0px;">Amount</td>
                </tr>
                    <tr>
                        <td><b>Therapy session</b></td>
                        <td><b>1</b></td>
                        <td>$'.$importe.'</td>
                        <td style="text-align:right;">$'.$importe.'</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td ><b>Subtotal:</b><br>Hst '.$impuesto.'% (78760 7928):</td>
                        <td style="text-align:right;">$'.$importe.'<br>$'.$iva.'</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><b>Total:</b><br>Payment on '.$date2.' using a '.$this->get_type_payment($idtype_payment).':</td>
                        <td style="text-align:right;">$'.$total.'<br>$'.$total.'</td>
                    </tr>
                
                <tr>
                    <td></td>
                    <td></td>
                    <td><b>Amount Due (CAD) :</b></td>
                    <td style="text-align:right;">$'.$total.'</td>
                </tr>
            </table>
        </html>';
        return $html;
    }
    function get_name_client($id_cita){
        $sql = "SELECT * FROM v_facturas_emitidas WHERE Cita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = utf8_encode($fila['Cliente']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_date($id_cita){
        $sql = "SELECT * FROM v_facturas_emitidas WHERE Cita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = utf8_encode($fila['Fecha']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_type_payment($id){
        $sql = "SELECT * FROM forma_pago WHERE idforma_pago=$id";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        
        while ($fila=mysqli_fetch_array($res)) {
            $co = $fila['forma_pago'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_status_invoice($status){
        switch ($status) {
            case '1':
                return '<div style="padding:5px; background-color:#58A25A; color:white; width:100px; text-align:center; border-radius: 5px 5px 5px 5px;"><b>PAID</b></div>';
                break;
            case '2':
                return '<div style="padding:5px; background-color:#892A2A; color:white; width:100px; text-align:center; border-radius: 5px 5px 5px 5px;"><b>cancelled</b></div>';
                break;
            case '3':
                return '<div style="padding:5px; background-color:#959595; color:white; width:100px; text-align:center; border-radius: 5px 5px 5px 5px;"><b>pending</b></div>';
                break;
        }
    }
    function get_style(){
        return "<style>
                html {
                font-family: sans-serif;
                line-height: 1.15;
                -webkit-text-size-adjust: 100%;
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                }
                --font-family-sans-serif: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
                --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, 'Liberation Mono', 'Courier New', monospace;
                .card {
                position: relative;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid rgba(0, 0, 0, 0.125);
                border-radius: 0.25rem;
                }

                .card > hr {
                margin-right: 0;
                margin-left: 0;
                }

                .card > .list-group:first-child .list-group-item:first-child {
                border-top-left-radius: 0.25rem;
                border-top-right-radius: 0.25rem;
                }

                .card > .list-group:last-child .list-group-item:last-child {
                border-bottom-right-radius: 0.25rem;
                border-bottom-left-radius: 0.25rem;
                }

                .card-body {
                -ms-flex: 1 1 auto;
                flex: 1 1 auto;
                padding: 1.25rem;
                }

                .card-title {
                margin-bottom: 0.75rem;
                }

                .card-subtitle {
                margin-top: -0.375rem;
                margin-bottom: 0;
                }

                .card-text:last-child {
                margin-bottom: 0;
                }

                .card-link:hover {
                text-decoration: none;
                }.text-right {
            .text-align: right !important;
            }
            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col,
            .col-auto, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm,
            .col-sm-auto, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md,
            .col-md-auto, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg,
            .col-lg-auto, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl,
            .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            }

            .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%;
            }

            .col-auto {
            -ms-flex: 0 0 auto;
            flex: 0 0 auto;
            width: auto;
            max-width: 100%;
            }

            .col-1 {
            -ms-flex: 0 0 8.333333%;
            flex: 0 0 8.333333%;
            max-width: 8.333333%;
            }

            .col-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
            }

            .col-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
            }

            .col-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
            }

            .col-5 {
            -ms-flex: 0 0 41.666667%;
            flex: 0 0 41.666667%;
            max-width: 41.666667%;
            }

            .col-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
            }

            .col-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
            }

            .col-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
            }

            .col-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
            }

            .col-10 {
            -ms-flex: 0 0 83.333333%;
            flex: 0 0 83.333333%;
            max-width: 83.333333%;
            }

            .col-11 {
            -ms-flex: 0 0 91.666667%;
            flex: 0 0 91.666667%;
            max-width: 91.666667%;
            }

            .col-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
            }

            .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
            }.table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            }

            .table th,
            .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
            }

            .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
            }

            .table tbody + tbody {
            border-top: 2px solid #dee2e6;
            }

            .table-sm th,
            .table-sm td {
            padding: 0.3rem;
            }

            .table-bordered {
            border: 1px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
            border: 1px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
            border-bottom-width: 2px;
            }

            .table-borderless th,
            .table-borderless td,
            .table-borderless thead th,
            .table-borderless tbody + tbody {
            border: 0;
            }

            .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
            }

            .table-hover tbody tr:hover {
            color: #212529;
            background-color: rgba(0, 0, 0, 0.075);
            }

            .table-primary,
            .table-primary > th,
            .table-primary > td {
            background-color: #b8daff;
            }

            .table-primary th,
            .table-primary td,
            .table-primary thead th,
            .table-primary tbody + tbody {
            border-color: #7abaff;
            }

            .table-hover .table-primary:hover {
            background-color: #9fcdff;
            }

            .table-hover .table-primary:hover > td,
            .table-hover .table-primary:hover > th {
            background-color: #9fcdff;
            }

            .table-secondary,
            .table-secondary > th,
            .table-secondary > td {
            background-color: #d6d8db;
            }

            .table-secondary th,
            .table-secondary td,
            .table-secondary thead th,
            .table-secondary tbody + tbody {
            border-color: #b3b7bb;
            }

            .table-hover .table-secondary:hover {
            background-color: #c8cbcf;
            }

            .table-hover .table-secondary:hover > td,
            .table-hover .table-secondary:hover > th {
            background-color: #c8cbcf;
            }

            .table-success,
            .table-success > th,
            .table-success > td {
            background-color: #c3e6cb;
            }

            .table-success th,
            .table-success td,
            .table-success thead th,
            .table-success tbody + tbody {
            border-color: #8fd19e;
            }

            .table-hover .table-success:hover {
            background-color: #b1dfbb;
            }

            .table-hover .table-success:hover > td,
            .table-hover .table-success:hover > th {
            background-color: #b1dfbb;
            }

            .table-info,
            .table-info > th,
            .table-info > td {
            background-color: #bee5eb;
            }

            .table-info th,
            .table-info td,
            .table-info thead th,
            .table-info tbody + tbody {
            border-color: #86cfda;
            }

            .table-hover .table-info:hover {
            background-color: #abdde5;
            }

            .table-hover .table-info:hover > td,
            .table-hover .table-info:hover > th {
            background-color: #abdde5;
            }

            .table-warning,
            .table-warning > th,
            .table-warning > td {
            background-color: #ffeeba;
            }

            .table-warning th,
            .table-warning td,
            .table-warning thead th,
            .table-warning tbody + tbody {
            border-color: #ffdf7e;
            }

            .table-hover .table-warning:hover {
            background-color: #ffe8a1;
            }

            .table-hover .table-warning:hover > td,
            .table-hover .table-warning:hover > th {
            background-color: #ffe8a1;
            }

            .table-danger,
            .table-danger > th,
            .table-danger > td {
            background-color: #f5c6cb;
            }

            .table-danger th,
            .table-danger td,
            .table-danger thead th,
            .table-danger tbody + tbody {
            border-color: #ed969e;
            }

            .table-hover .table-danger:hover {
            background-color: #f1b0b7;
            }

            .table-hover .table-danger:hover > td,
            .table-hover .table-danger:hover > th {
            background-color: #f1b0b7;
            }

            .table-light,
            .table-light > th,
            .table-light > td {
            background-color: #fdfdfe;
            }

            .table-light th,
            .table-light td,
            .table-light thead th,
            .table-light tbody + tbody {
            border-color: #fbfcfc;
            }

            .table-hover .table-light:hover {
            background-color: #ececf6;
            }

            .table-hover .table-light:hover > td,
            .table-hover .table-light:hover > th {
            background-color: #ececf6;
            }

            .table-dark,
            .table-dark > th,
            .table-dark > td {
            background-color: #c6c8ca;
            }

            .table-dark th,
            .table-dark td,
            .table-dark thead th,
            .table-dark tbody + tbody {
            border-color: #95999c;
            }

            .table-hover .table-dark:hover {
            background-color: #b9bbbe;
            }

            .table-hover .table-dark:hover > td,
            .table-hover .table-dark:hover > th {
            background-color: #b9bbbe;
            }

            .table-active,
            .table-active > th,
            .table-active > td {
            background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover {
            background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover > td,
            .table-hover .table-active:hover > th {
            background-color: rgba(0, 0, 0, 0.075);
            }

            .table .thead-dark th {
            color: #fff;
            background-color: #343a40;
            border-color: #454d55;
            }

            .table .thead-light th {
            color: #495057;
            background-color: #e9ecef;
            border-color: #dee2e6;
            }

            .table-dark {
            color: #fff;
            background-color: #343a40;
            }

            .table-dark th,
            .table-dark td,
            .table-dark thead th {
            border-color: #454d55;
            }

            .table-dark.table-bordered {
            border: 0;
            }

            .table-dark.table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(255, 255, 255, 0.05);
            }

            .table-dark.table-hover tbody tr:hover {
            color: #fff;
            background-color: rgba(255, 255, 255, 0.075);
            }</style>";
    }
    function get_citas_of_medical($id_medico){
        $sql = "SELECT * FROM v_invoice WHERE idmedico=$id_medico";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['nombre']          = utf8_encode($fila['nombre']);
            $co[$fila['idcita']]['idpaciente']      = utf8_encode($fila['idpaciente']);
            $co[$fila['idcita']]['sucursal']        = utf8_encode($fila['sucursal']);
            //$co[$fila['idcita']]['estado']          = utf8_encode($fila['estado_cita']);
            $co[$fila['idcita']]['servicio']        = utf8_encode($fila['servicio']);
            $co[$fila['idcita']]['medico']          = utf8_encode($fila['medico']);
            //$co[$fila['idcita']]['namebussines']    = utf8_encode($fila['bussines_name']);
            $co[$fila['idcita']]['email']           = utf8_encode($fila['email']);
            $co[$fila['idcita']]['importe']         = utf8_encode($fila['importe']);
            $co[$fila['idcita']]['impuesto']        = utf8_encode($fila['impuesto']);
            $co[$fila['idcita']]['serie']           = utf8_encode($fila['serie']);
            $co[$fila['idcita']]['folio']           = utf8_encode($fila['folio']);
            // $co[$fila['idcita']]['fecha']           = utf8_encode($fila['fecha']);
            $pacienteId = $fila['idpaciente'];
            $sql2 = "SELECT * FROM v_paciente WHERE idpaciente=$pacienteId";
            $this->conexion();
            $res2 = mysqli_query($this->conexion,$sql2);
            $paciente = mysqli_fetch_array($res2);
            
            $datos_estado_factura                   = $this->get_datos_factura($fila['idcita']);

            $co[$fila['idcita']]['estadofactura']   = $this->get_label_estado_factura($datos_estado_factura);
            if($fila['imagen'] != NULL){
                $co[$fila['idcita']]['imagen']           = utf8_encode($fila['imagen']);    
            }else{
                $co[$fila['idcita']]['imagen']           = "default.png";
            }

            if($datos_estado_factura['idestado'] == 3){
                //$co[$fila['idcita']]['buttons']         = "<div class='btn btn-primary float-right py-0 px-3' onclick='invoice.show_invoice(".$fila['idcita'].",'".$paciente['direccion']."','".$paciente['ciudad']."'); '><i class='fas fa-edit'></i></div>";
                $address = $paciente['direccion'];
                $city = $paciente['ciudad'];
                $co[$fila['idcita']]['buttons'] = '<div class="btn btn-primary float-right py-0 px-3" onclick="invoice.show_invoice('.$fila['idcita'].','.trim("'$address'").','.trim("'$city'").'); "><i class="fas fa-edit"></i></div>';
            }
            else{
                $co[$fila['idcita']]['buttons']         = "<div class='btn btn-unique float-right py-0 px-3' onclick='invoice.donwload_invoice(".$fila['idcita']."); '><i class='fas fa-download'></i></div>";
            }
           
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_datos_iniciales($idtipo_usuario){
    
        $extend_cita = new psii_citas();
        if($idtipo_usuario == 3){
            $co['sucursales'] = $extend_cita->get_list_sucursales_medico();
        }else{
            $co['sucursales'] = $extend_cita->get_list_sucursales();
        }   
        $co['servicio'] = $extend_cita->get_list_servicio();
        $co['ciudades'] = $extend_cita->get_list_ciudades();
        if(isset($co)) return $co;
    }
    function get_personal_logo($imagen){
        $co = "default.png";
        if($imagen != null){
            $co = $imagen;
        }
        return $co;
    }
}  

class medicos extends ini {
    function update_data_medic($name,$bussines,$record,$id_medico,$id_usuario,$serie,$folio,$checkinEdit,$checkoutEdit,$terms_conditions,$file,$type,$mobile_number,$pdf_file,$pdf_type){
        $terms_conditions = str_replace("'","\'",$terms_conditions);
        if(!empty($pdf_type)){
            $pdf_type = explode("/",$pdf_type);
            $pdf_name_file = uniqid().".".$pdf_type[1];
            move_uploaded_file($pdf_file,"../files/therapist_pdf/$pdf_name_file");
        }else{
            $pdf_name_file = 'No File';
        }
        if(!empty($type)){
            $type = explode("/",$type);
            $name_file = uniqid().".".$type[1];
            if(move_uploaded_file($file,"../files/personal_logo/$name_file")){
                 $sql = "UPDATE medico SET idusuario=$id_usuario, folio=$folio, serie='".$this->str($serie)."', medico='".$this->str($name)."', bussines_name='".$this->str($bussines)."', therapeutic_record='".$this->str($record)."', imagen='$name_file', horaEntrada='$checkinEdit', horaSalida='$checkoutEdit', terms_conditions='$terms_conditions', Mobile_number = '".$mobile_number."', editable_pdf='$pdf_name_file' WHERE idmedico=$id_medico";
                $co["file"]="1";
            }
        }
        else{
            $sql = "UPDATE medico SET idusuario=$id_usuario, folio=$folio, serie='".$this->str($serie)."', medico='".$this->str($name)."', bussines_name='".$this->str($bussines)."', therapeutic_record='".$this->str($record)."', horaEntrada='$checkinEdit', horaSalida='$checkoutEdit', terms_conditions='$terms_conditions', Mobile_number = '".$mobile_number."', editable_pdf='$pdf_name_file' WHERE idmedico=$id_medico";
            $co["file"]=0;
        }
        
        if($this->mobile_exists_edit_therepist($mobile_number, $id_medico))
        { 
                $co['estado']=false;
                $co['registered_mobile']=true;
                return $co;
        }
        else
        {
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                    $co['estado']=true;
                    return $co;
                    
            }
            else{
                    $co['error']='UPDATE ERROR ->'.mysqli_error($this->conexion);
                    $co['query']=$sql;
                    return $co;
                    
                }
        }
        
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function mobile_exists_edit_therepist($mobile_number, $id){
         $sql="SELECT Mobile_number FROM medico WHERE Mobile_number='$mobile_number' AND idmedico != '".$id."'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        mysqli_close($this->conexion);
        if(mysqli_num_rows($res)>0) {
           return true;
        }
        else{
           return false;
        }
    }
    function add_new_medic($name,$bussines,$record,$id_sucursal, $id_usuario_ref,$serie,$folio,$checkin,$checkout,$terms_conditions,$file,$type,$mobile_number){
        $type = explode(".",$type);
        $name_file = uniqid().".".end($type);
        $co['estado']=false;
        $this->conexion();
        $terms_conditions = str_replace("'","\'",$terms_conditions);
        if(move_uploaded_file($file,"../files/personal_logo/$name_file")){
            $sql = "INSERT INTO medico VALUES (0,'".$this->str($name)."',". $id_usuario_ref.",'".$this->str($bussines)."','".$this->str($record)."',$id_sucursal,'".$this->str($serie)."', $folio,'$checkin', '$checkout','$name_file','$terms_conditions','".$mobile_number."')";
            $co["file"]="1";
        }else{
            $sql = "INSERT INTO medico VALUES (0,'".$this->str($name)."',". $id_usuario_ref.",'".$this->str($bussines)."','".$this->str($record)."',$id_sucursal,'".$this->str($serie)."', $folio,'$checkin', '$checkout','default.png','$terms_conditions','".$mobile_number."')";
            $co["file"]=0;
        }
        /* Add Code Check alredy exist Mobile number*/
           if($this->mobile_exists_add_therepist($mobile_number, $id_medico))
        { 
                $co['estado']=false;
                $co['registered_mobile']=true;
                return $co;
        }
        else
        {
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                    $co['estado']=true;
                    return $co;
                    
            }
            else{
                    $co['error']='UPDATE ERROR ->'.mysqli_error($this->conexion);
                    $co['query']=$sql;
                    return $co;
                    
                }
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    /* add code by swadha 29_jan_2020*/
     function mobile_exists_add_therepist($mobile_number)
     {
         $sql="SELECT Mobile_number FROM medico WHERE Mobile_number='$mobile_number'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        mysqli_close($this->conexion);
        if(mysqli_num_rows($res)>0) {
           return true;
        }
        else{
           return false;
        }
     }
    /* add code by swadha 29_jan_2020*/
    function get_datos_medicos(){
        $co['sucursales'] = $this->get_lista_sucursales();
        if(isset($co)) return $co;
    }
    function get_lista_sucursales(){
        $sql = "SELECT * FROM sucursal";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila["idsucursal"]] = utf8_encode($fila["sucursal"]);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_medicos($id_sucursal){
       $sql = "SELECT * FROM v_medicos WHERE idsucursal=$id_sucursal";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idmedico']]['nombre'] = utf8_encode($fila['medico']);
            $co[$fila['idmedico']]['puesto'] = utf8_encode($fila['bussines_name']);
            $co[$fila['idmedico']]['sucursal'] = utf8_encode($fila['sucursal']);
            $co[$fila['idmedico']]['record'] = utf8_encode($fila['therapeutic_record']);
            $co[$fila['idmedico']]['serie'] = utf8_encode($fila['serie']);
            $co[$fila['idmedico']]['Mobile_number'] = utf8_encode($fila['Mobile_number']);
            $co[$fila['idmedico']]['folio'] = $fila['folio'];
            $co[$fila['idmedico']]['horaEntrada'] = $fila['horaEntrada'];
            $co[$fila['idmedico']]['horaSalida'] = $fila['horaSalida'];
            $co[$fila['idmedico']]['image'] = utf8_encode($fila['imagen']);
            $co[$fila['idmedico']]['iduser'] = $fila['idusuario'];
            $co[$fila['idmedico']]['terms_conditions'] = $fila['terms_conditions'];
        }
        
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_usuarios($id_sucursal){
        $sql = "SELECT * FROM v_usuario WHERE idsucursal=$id_sucursal";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idusuario']]['name'] = utf8_encode($fila['usuario']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}
class pacientes extends ini{
    function update_data_historico($problematica,$observacion,$tratamiento,$notas,$id_historico){
        $sql = "UPDATE historial SET problematica='".$this->str($problematica)."', observaciones='".$this->str($observacion)."', tratamiento='".$this->str($tratamiento)."', notas='".$this->str($notas)."'  WHERE idhistorial=$id_historico";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
            $co['historial'] = $this->get_expediente($id_historico);
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_expediente($id_historial){
        $sql = "SELECT * FROM v_historial WHERE idhistorial=$id_historial";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['nombre'] = utf8_encode($fila['nombre']);
            $co['fecha'] = utf8_encode($fila['fecha_ini']);
            $co['archivo'] = utf8_encode($fila['archivo']);
            $co['problematica'] = utf8_encode($fila['problematica']);
            $co['observaciones'] = utf8_encode($fila['observaciones']);
            $co['tratamiento'] = utf8_encode($fila['tratamiento']);
            $co['notas'] = utf8_encode($fila['notas']);
            $co['idmedico'] = utf8_encode($fila['idmedico']);
            $co['idsucursal'] = utf8_encode($fila['idsucursal']);
        }
        if(isset($co)){return $co;}
    }
    function export_file_patient($datos,$dompdf){
        $this->create_pdf($datos,$dompdf);
    }
    function create_pdf($datos,$dompdf){
        // $dompdf->loadHtml($this->get_html($datos));
        $dompdf->loadHtml($this->get_forms($datos));

        $dompdf->render();

        $output = $dompdf->output();
        if(file_put_contents('../files/patient_file.pdf', $output)){
            return $output;
        }else{
        return false;
        }
    }
    function get_html($datos){
        $invoice =  new invoices();
        $html = $invoice->get_style();
        $html .= '<table style="width:100%;">
            <tr>
                <td class="text-right">
                    <table class="table table-sm ">
                        <tr>
                            <td><b>Date:<br> '.$datos['fecha'].'</b></td>
                            <td><b>Time:<br> '.$datos['hora'].'</b></td>
                        </tr>
                        <tr>
                            <td><b>Problematic:</b><br>'.$datos['problematica'].'</td>
                            <td style="text-align:right;"></td>
                        </tr>
                        <tr>
                            <td><b>Observation:</b><br>'.$datos['observacion'].'</td>
                            <td style="text-align:right;"></td>
                        </tr>
                        <tr>
                            <td><b>Treatment:</b><br>'.$datos['tratamiento'].'</td>
                            <td style="text-align:right;"></td>
                        </tr>
                        <tr>
                            <td><b>Notes:</b><br>'.$datos['notas'].'</td>
                            <td style="text-align:right;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';
        return $html;
    }
    function get_forms($datos){
        $datos=json_decode($datos,true);
        $invoice =  new invoices();
        $html = $invoice->get_style();
        $html .= "<table style='width:100%;'>
            <tr>
                <td class='text-right'>
                    <br><br><br>
                    <br><br><br>
                    <br><br><br>
                    <table class='table table-sm'>
                    <tr>
                            <td><b>Date:<br>".$datos[0][1]."</b></td>
                            <td><b>Time:<br>".$datos[1][1]."</b></td>
                        </tr>";
                    for($i=2;$i<count($datos);$i++){
                        $html .="<tr>
                            <td><b>".$datos[$i][0]."</b><br>".$datos[$i][1]."</td>
                            <td style='text-align:right;'></td>
                        </tr>";
                    }
                        $html.="
                    </table>
                </td>
            </tr>
        </table>";
        return $html;
    }
    function update_data_paciente($name,$date,$address,$city,$phone,$mail,$id_paciente,$id_city,$file,$type){

        if(!empty($type)){
           $type = explode("/",$type);
            $name_file = uniqid().".".end($type);
            move_uploaded_file($file,"../files/client_editable_pdf/$name_file"); 
        }else{
            $name_file = 'No File';
        }
      
        $sql = "UPDATE paciente SET nombre='".$this->str($name)."', fecha_nacimiento='".$this->str($date)."', direccion='".$this->str($address)."', idciudad=$id_city, telefono='".$this->str($phone)."', email='".$this->str($mail)."', modal_edit_aptient_pdf='".$this->str($name_file)."' WHERE idpaciente=$id_paciente";
         
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function update_data_cliente($name,$date,$address,$city,$phone,$mail,$id_paciente,$id_city){
        $sql = "UPDATE cliente SET nombre='".$this->str($name)."', fecha_nacimiento='".$this->str($date)."', direccion='".$this->str($address)."', idciudad=$id_city, telefono='".$this->str($phone)."', email='".$this->str($mail)."' WHERE idcliente=$id_paciente";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_de_expedientes($id_user){
        $table = array(
            'individual_session_free_version',
            'individual_session_handwriting_scan',
            'individual_session_intake',
            'individual_session_soap_autocomplete',
            'individual_session_soap_full_version',
            'individual_session_snapshot',
            'couple_session_clients_information',
            'couple_session_clinical_note_autocomplete',
            'couple_session_intake',
            'couple_session_free_writing',
            'couple_session_handwriting_scan',
            'couple_session_soap_full_version',
            'couple_session_free_style'
        );
        $co['estado']=false;
        $this->conexion();
        foreach ($table as $key) {
            $sql = "SELECT * FROM $key WHERE idpaciente=$id_user";
            $res = mysqli_query($this->conexion,$sql);
            if($res->num_rows>0){
                while($fila=mysqli_fetch_assoc($res)){
                    $names = array_keys($fila);
                    $sql = "select `paciente`.`idpaciente` AS `idpaciente`,
                    `paciente`.`nombre` AS `nombre`,
                    `cita`.`idcita` AS `idcita`,
                    `cita`.`fecha_ini` AS `fecha_ini`,
                    `medico`.`idmedico` AS `idmedico`,
                    `medico`.`medico` AS `medico`,";
                    for($i=0;$i<count($names);$i++){
                        $sql.="$key.$names[$i] AS $names[$i],";
                    }
                    $sql.="`sucursal`.`idsucursal` AS `idsucursal`,
                    `sucursal`.`sucursal` AS `sucursal` 
                    from ((((`cita` join `paciente`) join `medico`) join $key) join `sucursal`) where ((`paciente`.`idpaciente` = `cita`.`idpaciente`) and (`cita`.`idmedico` = `medico`.`idmedico`) and (`cita`.`idcita` = $key.idcita) and (`cita`.`idsucursal` = `sucursal`.`idsucursal`))";
                }
                array_push($co,$this->crear_lista_de_expedientes($sql,$key));
                $co['estado']=true;
            }
        }
        mysqli_close($this->conexion);
        return $co;
    }
    function crear_lista_de_expedientes($sql,$key){
        $res = mysqli_query($this->conexion,$sql);
        if($res->num_rows>0){
            while($fila=mysqli_fetch_assoc($res)){
                $names = array_keys($fila);
                for($i=0;$i<count($names);$i++){
                    $co[$key][$names[$i]]=$fila[$names[$i]];
                }
            }
        }
        return $co;
    }
    // function get_lista_de_expedientes($id_user){
    //     $sql = "SELECT * FROM v_historial WHERE idpaciente=$id_user";
    //     $this->conexion();
    //     $res = mysqli_query($this->conexion,$sql);
    //     while ($fila=mysqli_fetch_array($res)) {
    //         $co[$fila['idhistorial']]['nombre'] = utf8_encode($fila['nombre']);
    //         $co[$fila['idhistorial']]['fecha'] = utf8_encode($fila['fecha_ini']);
    //         $co[$fila['idhistorial']]['archivo'] = utf8_encode($fila['archivo']);
    //         $co[$fila['idhistorial']]['problematica'] = utf8_encode($fila['problematica']);
    //         $co[$fila['idhistorial']]['observaciones'] = utf8_encode($fila['observaciones']);
    //         $co[$fila['idhistorial']]['tratamiento'] = utf8_encode($fila['tratamiento']);
    //         $co[$fila['idhistorial']]['notas'] = utf8_encode($fila['notas']);
    //         $co[$fila['idhistorial']]['idmedico'] = utf8_encode($fila['idmedico']);
    //         $co[$fila['idhistorial']]['idsucursal'] = utf8_encode($fila['idsucursal']);
    //     }
    //     mysqli_close($this->conexion);
    //     if(isset($co)){return $co;}
    // }
    function get_inputs(){
        $citas = new psii_citas();

        $co['ciudades'] = $citas->get_list_ciudades();
        if(isset($co)) return $co;
    }
   
    function get_lista_de_pacientes($user_id){ 
        $sql = "SELECT * FROM v_paciente WHERE idusuario='$user_id'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idpaciente']]['name'] = utf8_encode($fila['nombre']);
            $co[$fila['idpaciente']]['date'] = utf8_encode($fila['fecha_nacimiento']);
            $co[$fila['idpaciente']]['dir'] = utf8_encode($fila['direccion']);
            $co[$fila['idpaciente']]['tel'] = utf8_encode($fila['telefono']);
            $co[$fila['idpaciente']]['ciudad'] = utf8_encode($fila['ciudad']);
            $co[$fila['idpaciente']]['id_ciudad'] = $fila['idciudad'];
            $co[$fila['idpaciente']]['mail'] = utf8_encode($fila['email']);
            $co[$fila['idpaciente']]['fregistro'] = utf8_encode($fila['fecha_registro']);
            //$co[$file['idpaciente']]['therapist_pdf_name'] = $this->get_pdf_name($user_id);

            $sql1 = "SELECT * FROM medico WHERE idusuario='$user_id'";
            $res1 = mysqli_query($this->conexion,$sql1);
            $fila1=mysqli_fetch_array($res1);
            $co[$fila['idpaciente']]['editable_pdf'] = utf8_encode($fila1['editable_pdf']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_de_clientes(){
        $sql = "SELECT * FROM v_cliente";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcliente']]['name'] = utf8_encode($fila['nombre']);
            $co[$fila['idcliente']]['date'] = utf8_encode($fila['fecha_nacimiento']);
            $co[$fila['idcliente']]['dir'] = utf8_encode($fila['direccion']);
            $co[$fila['idcliente']]['tel'] = utf8_encode($fila['telefono']);
            $co[$fila['idcliente']]['ciudad'] = utf8_encode($fila['ciudad']);
            $co[$fila['idcliente']]['id_ciudad'] = $fila['idciudad'];
            $co[$fila['idcliente']]['mail'] = utf8_encode($fila['email']);
            $co[$fila['idcliente']]['fregistro'] = utf8_encode($fila['fecha_registro']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_clients_information(){
        $sql = "SELECT * FROM clients_information";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idpaciente']]['date'] = utf8_encode($fila['date']);
            $co[$fila['idpaciente']]['have_electronic_file'] = utf8_encode($fila['have_electronic_file']);
            $co[$fila['idpaciente']]['clients_name'] = utf8_encode($fila['clients_name']);
            $co[$fila['idpaciente']]['childs_name'] = utf8_encode($fila['childs_name']);
            $co[$fila['idpaciente']]['date_birth'] = utf8_encode($fila['date_birth']);
            $co[$fila['idpaciente']]['date_birth_of_child'] = utf8_encode($fila['date_birth_of_child']);
            $co[$fila['idpaciente']]['address'] = utf8_encode($fila['address']);
            $co[$fila['idpaciente']]['city'] = utf8_encode($fila['city']);
            $co[$fila['idpaciente']]['postal_code'] = utf8_encode($fila['postal_code']);
            $co[$fila['idpaciente']]['phone_number'] = utf8_encode($fila['phone_number']);
            $co[$fila['idpaciente']]['email'] = utf8_encode($fila['email']);
            $co[$fila['idpaciente']]['clients_substitute_decision_maker'] = utf8_encode($fila['clients_substitute_decision_maker']);
            $co[$fila['idpaciente']]['gender'] = utf8_encode($fila['gender']);
            $co[$fila['idpaciente']]['relationship'] = utf8_encode($fila['relationship']);
            $co[$fila['idpaciente']]['living_circumstances'] = utf8_encode($fila['living_circumstances']);
            $co[$fila['idpaciente']]['education'] = utf8_encode($fila['education']);
            $co[$fila['idpaciente']]['employment'] = utf8_encode($fila['employment']);
            $co[$fila['idpaciente']]['benefits_to_cover_session'] = utf8_encode($fila['benefits_to_cover_session']);
            $co[$fila['idpaciente']]['insurances_name'] = utf8_encode($fila['insurances_name']);
            $co[$fila['idpaciente']]['amount_they_cover_per_year'] = $fila['amount_they_cover_per_year'];
            $co[$fila['idpaciente']]['referral'] = utf8_encode($fila['referral']);
            $co[$fila['idpaciente']]['referral_information'] = utf8_encode($fila['referral_information']);
            $co[$fila['idpaciente']]['referral_phone_number'] = utf8_encode($fila['referral_phone_number']);
            $co[$fila['idpaciente']]['referral_address'] = utf8_encode($fila['referral_address']);
            $co[$fila['idpaciente']]['family_physician'] = utf8_encode($fila['family_physician']);
            $co[$fila['idpaciente']]['family_member'] = utf8_encode($fila['family_member']);
            $co[$fila['idpaciente']]['emergency_contact'] = utf8_encode($fila['emergency_contact']);
            $co[$fila['idpaciente']]['receive_encrypted_mail_or_text'] = utf8_encode($fila['receive_encrypted_mail_or_text']);
            $co[$fila['idpaciente']]['how_do_you_know_about'] = utf8_encode($fila['how_do_you_know_about']);
            $co[$fila['idpaciente']]['editable_pdf'] = utf8_encode($fila['editable_pdf']);
             /*$id_paciente = $fila['idpaciente'];

            $sql_paciente = "SELECT idusuario FROM paciente WHERE idpaciente = '$id_paciente'"; 
            $res_paciente = mysqli_query($this->conexion,$sql_paciente);
            $fila_paciente =mysqli_fetch_array($res_paciente);
            $user_id = $fila_paciente["idusuario"];
            
            $sql_medico= "SELECT editable_pdf FROM medico WHERE idusuario = '$user_id'"; 
            $res_medico = mysqli_query($this->conexion,$sql_medico);
            $fila_medico =mysqli_fetch_array($res_medico);
            $editable_file_name = $fila_medico["editable_pdf"];*/

        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_patient($name,$date,$address,$city,$phone,$email){

        $sql = "INSERT INTO paciente VALUES (0,'".$this->str($name)."','$date','".$this->str($address)."',$city,'$phone','".$this->str($email)."',NOW(),".$_SESSION['id_user'].",null)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_client($name,$date,$address,$city,$phone,$email){
        $sql = "INSERT INTO cliente VALUES (0,'".$this->str($name)."','$date','".$this->str($address)."',$city,'$phone','".$this->str($email)."',NOW(),".$_SESSION['id_user'].")";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}
    
class usuarios extends ini{
    function update_data_usuarios($name,$email,$mobile_number,$username,$password,$typeuser,$id_usuario){
        $reset_token=md5($id_usuario).uniqid();
        $sql = "UPDATE usuario SET nombre='".$this->str($name)."', correo='$email', Mobile_number='".$mobile_number."', usuario='".$this->str($username)."', password='".$this->str($password)."', idtipo_usuario=$typeuser, reset_token='$reset_token' WHERE idusuario=$id_usuario";
        // if($this->if_email_exists($email)){
        //     $co['estado']=false;
        //     $co['registered_mail']=true;
        //     return $co;
        // }
         if($typeuser == '3')
         {
            if($this->if_mobile_exists_edit($mobile_number, $id_usuario))
            { 
                $co['estado']=false;
                $co['registered_mobile']=true;
                return $co;
            }
         }
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_user($name,$email,$mobile_number,$username,$password,$typeuser,$id_clinica){
        $sql = "INSERT INTO usuario VALUES (0, '$email','".$this->str($name)."','".$this->str($username)."','".$this->str($password)."',$id_clinica,$typeuser,'',NULL,'".$mobile_number."')";
        
        if($typeuser != 3)
        {
           if($this->if_email_exists($email)){
            $co['estado']=false;
            $co['registered_mail']=true;
            return $co;
            } 
        }
        else
        { 
            if(($this->if_email_exists($email)) && (!$this->if_mobile_exists($mobile_number))){
             //echo 'hello only mail exist ';
             $co['estado']=false;
             $co['registered_mail']=true;
             return $co;
            }
            elseif(($this->if_email_exists($email)) && ($this->if_mobile_exists($mobile_number))){

              //echo 'mail and mobile number both are exist ';
             $co['estado']=false;
             $co['registered_mail']=true;
             $co['registered_mobile']=true;
             return $co;
            }
            elseif((!$this->if_email_exists($email)) && ($this->if_mobile_exists($mobile_number))){

              //echo 'only mobile number both are exist ';
             $co['estado']=false;
             $co['registered_mobile']=true;
             return $co;
            }
        }

        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $last_id=mysqli_insert_id($this->conexion);
            $reset_token=md5($last_id).uniqid();
            $sql= "UPDATE usuario SET reset_token='$reset_token' WHERE idusuario=$last_id";
            if(mysqli_query($this->conexion,$sql)){
                $co['estado']=true;
            }
            else{
                $co['estado']=false;
                $co['error']='UPDATE ERROR ->'.mysqli_error($this->conexion);
            }
        }
        else{
            $co['estado']=false;
            $co['error']='UPDATE ERROR ->'.mysqli_error($this->conexion);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function if_email_exists($email){
        $sql="SELECT correo FROM usuario WHERE correo='$email'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        mysqli_close($this->conexion);
        if(mysqli_num_rows($res)>0) {
            return true;
        }
        else{
            return false;
        }
    }
     /* Code Added By swadha at 27-jan-2020*/
       function if_mobile_exists($mobile_number){
        $sql="SELECT Mobile_number FROM usuario WHERE Mobile_number='$mobile_number'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        mysqli_close($this->conexion);
        if(mysqli_num_rows($res)>0) {
           return true;
        }
        else{
           return false;
        }
    }
     function if_mobile_exists_edit($mobile_number, $id){
         $sql="SELECT Mobile_number FROM usuario WHERE Mobile_number='$mobile_number' AND idusuario != '".$id."'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        mysqli_close($this->conexion);
        if(mysqli_num_rows($res)>0) {
           return true;
        }
        else{
           return false;
        }
    }
    /* End Code Added By swadha at 27-jan-2020*/
    function get_datos_input(){
        $co['tipo_usuario'] = $this->get_lista_tipo_usuarios();
        $co['sucursales'] = $this->get_lista_sucursales();
        if(isset($co)) return $co;
    }
    function get_lista_tipo_usuarios(){
        $sql = "SELECT * FROM tipo_usuario";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila["idtipo_usuario"]] = utf8_encode($fila["tipo_usuario"]);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_sucursales(){
        $sql = "SELECT * FROM sucursal";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila["idsucursal"]] = utf8_encode($fila["sucursal"]);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_de_usuarios($idsucursal){
        $sql = "SELECT * FROM v_usuario WHERE idsucursal=$idsucursal";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idusuario']]['name'] = utf8_encode($fila['nombre']);
            $co[$fila['idusuario']]['username'] = utf8_encode($fila['usuario']);
            $co[$fila['idusuario']]['password'] = utf8_encode($fila['password']);
            $co[$fila['idusuario']]['sucursal'] = utf8_encode($fila['sucursal']);
            $co[$fila['idusuario']]['tipo_usuario'] = utf8_encode($fila['tipo_usuario']);
            $co[$fila['idusuario']]['idtipo_usuario'] = utf8_encode($fila['idtipo_usuario']);
            $co[$fila['idusuario']]['Mobile_number'] = utf8_encode($fila['Mobile_number']);
            $co[$fila['idusuario']]['correo'] = utf8_encode($fila['correo']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class ciudades extends ini {
    function get_list_ciudades(){

    }
}

class psii_citas extends ini {
    function __construct(){
        $this->conexion();
    }
    
    function cancelar_cita($id_cita){
        $sql = "UPDATE cita SET idestado_cita=4 WHERE idcita=$id_cita";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
            $co['datos_new_cita'] = $this->get_datos_new_cita($id_cita);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    /*ZAPIER EXPERT'S CODE START*/

    function get_datos_using_date($date){
        $sql = "SELECT * FROM v_citas WHERE DATE(fecha_ini)='$date' AND estado_cita = 'CONFIRMED'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['id']               = $fila['idcita'];
            $co[$fila['idcita']]['telefono']         = $fila['telefono'];
            $co[$fila['idcita']]['nombre']           = utf8_encode($fila['nombre']);
            $co[$fila['idcita']]['finicio']          = utf8_encode($fila['fecha_ini']);
            $co[$fila['idcita']]['ffinal']           =  utf8_encode($fila['fecha_fin']);
            $co[$fila['idcita']]['servicio']         =  utf8_encode($fila['servicio']);
            $co[$fila['idcita']]['estado_cita']      =  utf8_encode($fila['estado_cita']);
            $co[$fila['idcita']]['classNames']       =  $this->get_color_status_event($fila['idestado_cita']);
            $co[$fila['idcita']]['estado']           =  $fila['idestado_cita'];
            $co[$fila['idcita']]['open_tok_session'] =  $fila['open_tok_session'];
            $co[$fila['idcita']]['currentTime']      =  date("Y-m-d h:i:s a")." ".date_default_timezone_get();
        }
        if(isset($co)){return $co;}
    }

    /*ZAPIER EXPERT'S CODE END*/

    function get_datos_new_cita($id_cita){
        $sql = "SELECT * FROM v_citas WHERE idcita=$id_cita";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila = mysqli_fetch_array($res)) {
            $co['id']               = $fila['idcita'];
            $co['nombre']           = utf8_encode($fila['nombre']);
            $co['finicio']          = utf8_encode($fila['fecha_ini']);
            $co['ffinal']           =  utf8_encode($fila['fecha_fin']);
            $co['classNames']       =  $this->get_color_status_event($fila['idestado_cita']);
            $co['estado']           =  $fila['idestado_cita'];
            $co['open_tok_session'] =  $fila['open_tok_session'];
        }
        if(isset($co)){return $co;}
    }
    function save_new_patient($name,$date,$address,$city,$phone,$email){
        $sql = "INSERT INTO paciente VALUES (0,'".$this->str($name)."','$date','".$this->str($address)."',$city,'$phone','".$this->str($email)."',NOW(),".$_SESSION['id_user'].",null)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_historial_clinico2($id_estado_cita,$id_cita,$notas,$observaciones,$problematica,$tratamiento,$file,$id_medico,$type){
        if($this->validar_si_existe_historial_clinico($id_cita)){
            return false;
        }else{
            $datos_citas = $this->get_datos_cita($id_cita);
            $type = explode("/",$file['type']);
            $name_file = uniqid().".".$type[1];
            $sql = "INSERT INTO historial VALUES (0,'".date("Y-m-d H:m:s")."',".$datos_citas['paciente'].",".$_SESSION['id_user'].",'".$this->str($problematica)."','".$this->str($observaciones)."','".$this->str($tratamiento)."','".$this->str($notas)."','".$name_file."',".$id_medico.",$id_cita)";
            $co['estado']=false;
            $this->conexion();
            if(mysqli_query($this->conexion,$sql)){
                $this->update_estado_cita($id_cita,$id_estado_cita);
                if(isset($file['base64'])){
                    if(move_uploaded_file("../files/$name_file",$file)){
                        $co["file"]="1";
                    }
                    //$this->upload_file($file['base64'],$name_file);
                }

                $co['estado']=true;
            }
            mysqli_close($this->conexion);
            if(isset($co)){return $co;}
        }
    }
    function get_client_cita($id_cita){
        $sql = "SELECT idpaciente FROM cita WHERE idcita=$id_cita";
        // $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        $idpaciente=0;
        while ($fila=mysqli_fetch_array($res)) {
            $idpaciente=$fila['idpaciente'];
        }
        return $idpaciente;
    }
    function save_files_of_forms($table_name,$last_id,$file){
        $table = array(
            'individual_free_version'=> 'individual_session_free_version',
            'individual_handwriting_scan'=> 'individual_session_handwriting_scan',
            'individual_intake_session'=> 'individual_session_intake',
            'individual_soap_autocomplete'=> 'individual_session_soap_autocomplete',
            'individual_soap_full_version'=> 'individual_session_soap_full_version',
            'individual_snapshot'=> 'individual_session_snapshot',
            'couple_clients_information'=> 'couple_session_clients_information',
            'couple_clinical_note_autocomplete'=> 'couple_session_clinical_note_autocomplete',
            'couple_intake_session'=> 'couple_session_intake',
            'couple_free_writing'=> 'couple_session_free_writing',
            'couple_hand_writing_scan'=> 'couple_session_handwriting_scan',
            'couple_soap_full_version'=> 'couple_session_soap_full_version',
            'couple_free_style_couple_session'=> 'couple_session_free_style'
        );
        $nombre_campo = array_keys($file);
        for($i=0;$i<count($file);$i++){
            $name=$file[$nombre_campo[$i]]['name'];
            $type=$file[$nombre_campo[$i]]['type'];
            $tmp=$file[$nombre_campo[$i]]['tmp_name'];
            $type = explode("/",$type);
            $name_file = uniqid().".".end($type);
            $co['estado']=false;
            if(move_uploaded_file($tmp,"../files/clinic history/$name_file")){
                $this->conexion();
                $co["file"]="1";
                $sql = "UPDATE $table[$table_name] SET $nombre_campo[$i]='$name_file' WHERE id=$last_id";
                if(mysqli_query($this->conexion,$sql)){
                    $co['estado']=true;
                }
                else{
                    $co['estado']=false;
                    $co['error']=mysqli_error($this->conexion);
                }
            }else{
                $co["file"]=0;
                $co['estado']=false;
            }
        }
        return $co;
    }
    function save_historial_clinico( $id_medico, $modal_form_historial_estado, $modal_form_type_session, $id_cita, $table_name, $columns){
        $table = array(
            'individual_free_version'=> 'individual_session_free_version',
            'individual_handwriting_scan'=> 'individual_session_handwriting_scan',
            'individual_intake_session'=> 'individual_session_intake',
            'individual_soap_autocomplete'=> 'individual_session_soap_autocomplete',
            'individual_soap_full_version'=> 'individual_session_soap_full_version',
            'individual_snapshot'=> 'individual_session_snapshot',
            'couple_clients_information'=> 'couple_session_clients_information',
            'couple_clinical_note_autocomplete'=> 'couple_session_clinical_note_autocomplete',
            'couple_intake_session'=> 'couple_session_intake',
            'couple_free_writing'=> 'couple_session_free_writing',
            'couple_hand_writing_scan'=> 'couple_session_handwriting_scan',
            'couple_soap_full_version'=> 'couple_session_soap_full_version',
            'couple_free_style_couple_session'=> 'couple_session_free_style'
        );
        // return false;
        $co['estado']=false;
        $this->conexion();
        $this->update_estado_cita($id_cita,$modal_form_historial_estado);

        $aiudaa = json_decode($columns,true);
        foreach ($aiudaa as $key => $value) {
            if ($value[0] == "couple_terms_conditions_free_version") {
                $aiudaa[$key][1] = base64_encode($value[1]);
            }

            if ($value[0] == "terms_conditions_free_version") {
                $aiudaa[$key][1] = base64_encode($value[1]);
            }
        }


        $sql = "INSERT INTO $table[$table_name] (type_session,";
        for($i=0;$i<count($aiudaa);$i++){
            $sql.=($i+1==count($aiudaa))?$aiudaa[$i][0]:$aiudaa[$i][0]." ,";
        }
        $sql.=",idcita) VALUES ($modal_form_type_session,";
        for($i=0;$i<count($aiudaa);$i++){
            if($aiudaa[$i][0]=='idpaciente'){
                $aiudaa[$i][1]=$this->get_client_cita($id_cita);
            }
            if(is_numeric($aiudaa[$i][1])){
                $sql.=($i+1==count($aiudaa))?$aiudaa[$i][1]:$aiudaa[$i][1]." ,";
            }
            else{
                $sql.=($i+1==count($aiudaa))?'"'.$aiudaa[$i][1].'"':'"'.$aiudaa[$i][1].'"'.',';
            }
        }
        $sql.=",$id_cita)";
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
            $co['query']=$sql;
            $co['last_id']=mysqli_insert_id($this->conexion);
            mysqli_close($this->conexion);
            return $co;
        }
        else{
            $co['estado']=false;
            $co['error']=mysqli_error($this->conexion);
            mysqli_close($this->conexion);
            return $co;
        }
    }
    // function save_historial_clinico($id_estado_cita,$id_cita,$notas,$observaciones,$problematica,$tratamiento,$file,$id_medico,$type){
    //     if($this->validar_si_existe_historial_clinico($id_cita)){
    //         $datos_citas = $this->get_datos_cita($id_cita);
    //         $type = explode(".",$type);
    //         $name_file = uniqid().".".end($type);

    //         $sql = "INSERT INTO historial VALUES (0,'".date("Y-m-d H:m:s")."',".$datos_citas['paciente'].",".$_SESSION['id_user'].",'".$this->str($problematica)."','".$this->str($observaciones)."','".$this->str($tratamiento)."','".$this->str($notas)."','$name_file',".$id_medico.",$id_cita)";
    //         $co['estado']=false;
    //         $this->conexion();
    //         if(mysqli_query($this->conexion,$sql)){
    //             $this->update_estado_cita($id_cita,$id_estado_cita);
    //             if(move_uploaded_file($file,"../files/clinic history/$name_file")){
    //                 $co["file"]="1";
    //             }else{
    //                 $co["file"]=0;
    //             }

    //             $co['estado']=true;
    //         }
    //         mysqli_close($this->conexion);
    //         if(isset($co)){return $co;}
    //     }
    // }
    function update_estado_cita($id_cita,$estado){
        $sql = "UPDATE cita SET idestado_cita=$estado WHERE idcita=$id_cita";
        mysqli_query($this->conexion,$sql);
    }
    function get_datos_cita($id_cita){
        $sql = "SELECT * FROM v_citas WHERE idcita=$id_cita";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['paciente'] = $fila['idpaciente'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function validar_si_existe_historial_clinico($id_cita){
        $sql = "SELECT * FROM historial WHERE id_cita=$id_cita";
        $this->conexion();
        $co=true;
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co=true;
        }
        if($co==true){
            $sql = "DELETE FROM historial WHERE id_cita=$id_cita";
            if(mysqli_query($this->conexion,$sql)){
                $co = true;
            }else{
                $co = false;
            }
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function add_new_date($id_sucursal,$id_paciente,$id_medico,$start_date,$end_date,$id_servicio,$mail,$user_mail,$name_medic,$open_tok_sesson = ""){
       
        if($this->validar_traslapeo_citas($id_medico,$id_sucursal,$start_date,$end_date)){
            $co['estado']=false;
            $co['traslapeo']=true;
            return $co;
        }
        $sql = "INSERT INTO cita VALUES (0,$id_paciente,'$start_date',$id_sucursal,1,$id_servicio,$id_medico,'$end_date','$open_tok_sesson')";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
            $id_cita = mysqli_insert_id($this->conexion);
            $co['send_mail'] = $this->send_mail($id_cita,$start_date,$mail,$user_mail,$name_medic,$end_date);
            $co['event'] = $this->get_only_one_event($id_cita);
        }else{
            $co['estado']=false;
            $co['error']='INSERT ERROR ->'.mysqli_error($this->conexion);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_only_one_event($id_event){
        $sql = "SELECT * FROM v_citas WHERE idcita=$id_event";
        $res = mysqli_query($this->conexion,$sql);
        $cont =1;
        while ($fila=mysqli_fetch_array($res)) {
            $co['id']= $fila['idcita'];
            $co['nombre']= utf8_encode($fila['nombre']);
            $co['finicio']= utf8_encode($fila['fecha_ini']);
            $co['ffinal']=  utf8_encode($fila['fecha_fin']);
            $co['classNames']= $this->get_color_status_event($fila['idestado_cita']);
            $co['estado']=  $fila['idestado_cita'];
        }
        if(isset($co)){return $co;}
    }
    function validar_traslapeo_citas($id_medico,$id_sucursal,$start_date,$end_date){
        $sql = "SELECT * FROM cita WHERE (idsucursal=$id_sucursal AND idmedico=$id_medico) AND ((fecha_ini >= '$start_date' AND fecha_ini < '$end_date') OR ('$start_date' >= fecha_ini AND '$end_date' <= fecha_fin) OR (fecha_fin > '$start_date' AND fecha_fin <= '$end_date')) AND idestado_cita<>4";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_pacientes(){
        $id = $_SESSION['id_user'];
        $sql = "SELECT * FROM paciente WHERE idusuario = '$id'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co['pacientes'][$fila['idpaciente']]['name']= utf8_encode($fila['nombre']);
            $co['pacientes'][$fila['idpaciente']]['email']= utf8_encode($fila['email']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_datos_input($idtipo_usuario){
        if($idtipo_usuario == 3){
            $co['sucursales'] = $this->get_list_sucursales_medico();
        }else{
            $co['sucursales'] = $this->get_list_sucursales();
        }   
        $co['servicio'] = $this->get_list_servicio();
        $co['ciudades'] = $this->get_list_ciudades();
        if(isset($co)) return $co;
    }
    function get_list_sucursales_medico(){
        if(isset($_SESSION['id_user'])){
            $sql = "SELECT * FROM v_medicos WHERE idusuario=".$_SESSION['id_user'];
            $this->conexion();
            $res = mysqli_query($this->conexion, $sql);
            while ($fila = mysqli_fetch_array($res)) {
                $co[$fila['idsucursal']]['name'] = utf8_decode($fila['sucursal']);
                $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['nombre'] = utf8_decode($fila['medico']);
                $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['horaEntrada'] = utf8_decode($fila['horaEntrada']);
                $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['horaSalida'] = utf8_decode($fila['horaSalida']);
            }
            mysqli_close($this->conexion);
        }
        else{
            $co['empty_session']=true;
        }
        if (isset($co)) {
            return $co;
        }
    }
    function get_list_ciudades(){ 
        $sql = "SELECT * FROM ciudad";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idciudad']]['name'] = utf8_decode($fila['ciudad']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_list_servicio(){
        $id = $_SESSION['id_user'];
        $sql = "SELECT * FROM servicio WHERE idusuario = '$id'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idservicio']]['name'] = utf8_decode($fila['servicio']);
            $co[$fila['idservicio']]['idmedico'] = $fila['idmedico'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_list_medicos(){
        $sql = "SELECT * FROM medico";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idmedico']]['name'] = utf8_decode($fila['medico']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_list_sucursales(){
        $sql = "SELECT * FROM v_medicos";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idsucursal']]['name'] = utf8_decode($fila['sucursal']);
            $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['nombre'] = utf8_decode($fila['medico']);
            $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['horaEntrada'] = utf8_decode($fila['horaEntrada']);
            $co[$fila['idsucursal']]['medicos'][$fila['idmedico']]['horaSalida'] = utf8_decode($fila['horaSalida']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function get_lista_de_eventos($id_clinica,$id_medico){
        $sql = "SELECT * FROM v_citas WHERE idsucursal=$id_clinica AND idmedico=$id_medico";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        $cont =1;
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['id']= $fila['idcita'];
            $co[$fila['idcita']]['nombre']= utf8_encode($fila['nombre']);
            $co[$fila['idcita']]['finicio']= utf8_encode($fila['fecha_ini']);
            $co[$fila['idcita']]['ffinal']=  utf8_encode($fila['fecha_fin']);
            $co[$fila['idcita']]['medico']=  utf8_encode($fila['medico']);
            $co[$fila['idcita']]['classNames']=  $this->get_color_status_event($fila['idestado_cita']);
            $co[$fila['idcita']]['estado']=  $fila['idestado_cita'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    /*ZAPIER EXPERT'S CODE START*/

    function get_latest_eventos(){
        $sql = "SELECT * FROM `v_citas` WHERE `fecha_fin` > now() AND `open_tok_session` != '' ORDER BY `fecha_ini` DESC";
        $res = mysqli_query($this->conexion,$sql);
        $cont =1;
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idcita']]['id']= $fila['idcita'];
            $co[$fila['idcita']]['nombre']= utf8_encode($fila['nombre']);
            $co[$fila['idcita']]['finicio']= utf8_encode($fila['fecha_ini']);
            $co[$fila['idcita']]['ffinal']=  utf8_encode($fila['fecha_fin']);
            $co[$fila['idcita']]['servicio']=  utf8_encode($fila['servicio']);
            $co[$fila['idcita']]['estado_cita']=  utf8_encode($fila['estado_cita']);
            $co[$fila['idcita']]['classNames']=  $this->get_color_status_event($fila['idestado_cita']);
            $co[$fila['idcita']]['estado']=  $fila['idestado_cita'];
            $co[$fila['idcita']]['open_tok_session']=  $fila['open_tok_session'];
            $co[$fila['idcita']]['currentTime']=  date("Y-m-d h:i:s a")." ".date_default_timezone_get();
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }

    /*ZAPIER EXPERT'S CODE END*/
    
    function get_color_status_event($id_estado_cita){
        switch ($id_estado_cita) {
            case "1":
                return "grey darken-3";
            case "2":
                return "";
            case "3":
                return "green darken-3";
            case "4":
                return "orange darken-3";
            case "5":
                return "red darken-3";
        }
    }
}

class login extends ini {
    function validar_login($usuario,$password){
        $sql = "SELECT * FROM usuario WHERE usuario='$usuario' AND password='$password'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $_SESSION['id_user'] = $fila['idusuario'];
            $_SESSION['idtipo_usuario'] = $fila['idtipo_usuario'];
            $co['estado'] = true;
            $co['idtipo_usuario'] = $fila['idtipo_usuario'];
            $co['idusuario'] = $fila['idusuario'];
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    } 
    function logout(){
        session_destroy();
        return true;
    } 
    function validar_login_old(){
        if(!isset($_SESSION['id_user'])){
            $co['estado']=true;
        }else{
            $co['estado'] = false;
            $co['idtipo_usuario'] = $_SESSION['idtipo_usuario'];
        }
        if(isset($co)) return $co;
    }
    function recover_password($email,$mail){
        $sql = "SELECT reset_token FROM usuario WHERE correo='$email'";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        if($res->num_rows==1){
            while($fila=mysqli_fetch_array($res)){
                $co['msj_state']=$this->recover_account($fila['reset_token'],$email,$mail);
                $co['estado']=true;
            }
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}  

class provincia extends ini {
    function get_lista_de_provincia(){
        $sql = "SELECT e.idestado AS idestado,e.estado AS estado,p.pais AS pais FROM estado AS e INNER JOIN pais AS p ON e.idpais=p.idpais";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idestado']]['idestado'] = utf8_encode($fila['idestado']);
            $co[$fila['idestado']]['estado'] = utf8_encode($fila['estado']);
            $co[$fila['idestado']]['pais'] = utf8_encode($fila['pais']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_provincia($province,$country,$user){
        $sql = "INSERT INTO estado VALUES (0,'".$this->str($province)."',$country)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class servicios extends ini {
    function get_lista_servicios(){
        $sql = "SELECT s.idservicio AS idservicio, s.servicio AS servicio, s.importe AS importe, s.duracion AS duracion, u.usuario AS usuario, s.impuesto AS impuesto, u.idusuario AS idusuario, m.medico AS medico, m.idmedico AS idmedico FROM servicio AS s INNER JOIN usuario AS u  ON s.idusuario=u.idusuario INNER JOIN medico AS m ON s.idmedico = m.idmedico";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idservicio']]['idservicio'] = utf8_decode($fila['idservicio']);
            $co[$fila['idservicio']]['servicio'] = utf8_decode($fila['servicio']);
            $co[$fila['idservicio']]['importe'] = utf8_decode($fila['importe']);
            $co[$fila['idservicio']]['duracion'] = utf8_decode($fila['duracion']);
            $co[$fila['idservicio']]['impuesto'] = utf8_decode($fila['impuesto']);
            $co[$fila['idservicio']]['usuario'] = utf8_decode($fila['usuario']);
            $co[$fila['idservicio']]['idusuario'] = utf8_decode($fila['idusuario']);
            $co[$fila['idservicio']]['medico'] = utf8_decode($fila['medico']);
            $co[$fila['idservicio']]['idmedico'] = utf8_decode($fila['idmedico']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_service($service,$amount,$time,$tax,$user,$idmedico){
        $sql = "INSERT INTO servicio VALUES (0,'".$this->str($service)."',$amount,$time,$user,$tax,$idmedico)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function update_data_servicios($servicio,$importe,$duracion,$impuesto,$id_servicio,$idmedico){
        $sql = "UPDATE servicio SET servicio='".$this->str($servicio)."', importe=$importe, duracion=$duracion, impuesto=$impuesto, idmedico=$idmedico WHERE idservicio=$id_servicio";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class productos extends ini {
    function get_lista_productos(){
        $sql = "SELECT p.idproducto AS idproducto, p.clave AS clave, p.producto AS producto, 
        p.costo_ultimo AS costo_ultimo, p.impuesto AS impuesto, p.idusuario AS idusuario, u.usuario AS usuario FROM producto AS p JOIN usuario AS u WHERE p.idusuario=u.idusuario";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idproducto']]['idproducto'] = utf8_decode($fila['idproducto']);
            $co[$fila['idproducto']]['clave'] = utf8_decode($fila['clave']);
            $co[$fila['idproducto']]['producto'] = utf8_decode($fila['producto']);
            $co[$fila['idproducto']]['costo_ultimo'] = utf8_decode($fila['costo_ultimo']);
            $co[$fila['idproducto']]['impuesto'] = utf8_decode($fila['impuesto']);
            $co[$fila['idproducto']]['idusuario'] = utf8_decode($fila['idusuario']);
            $co[$fila['idproducto']]['usuario'] = utf8_decode($fila['usuario']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_product($key,$name,$cost,$tax,$user){
        $sql = "INSERT INTO producto VALUES (0,'$key','$name',$cost,$tax,$user)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
            $co['error']=$this->conexion->error;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function update_product($clave,$producto,$costo_ultimo,$impuesto,$id){
        $sql = "UPDATE producto SET clave='$clave', producto='$producto', costo_ultimo=$costo_ultimo, impuesto=$impuesto WHERE idproducto=$id";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function delete_product($id){
        $sql = "DELETE FROM producto WHERE idproducto=$id";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class proveedores extends ini {
    function get_lista_proveedores(){
        $sql = "SELECT p.idproveedor AS idproveedor, 
                p.nombre AS nombre, 
                p.direccion AS direccion, 
                p.contacto AS contacto, 
                p.mail AS mail, 
                c.idciudad AS idciudad,
                c.ciudad AS ciudad, 
                p.idusuario AS idusuario, 
                u.usuario AS usuario 
                FROM proveedor AS p JOIN usuario AS u ON p.idusuario=u.idusuario JOIN ciudad AS c ON p.idciudad = c.idciudad WHERE p.idusuario=u.idusuario";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idproveedor']]['idproveedor'] = $fila['idproveedor'];
            $co[$fila['idproveedor']]['nombre'] = utf8_decode($fila['nombre']);
            $co[$fila['idproveedor']]['direccion'] = utf8_decode($fila['direccion']);
            $co[$fila['idproveedor']]['idciudad'] = $fila['idciudad'];
            $co[$fila['idproveedor']]['ciudad'] = utf8_decode($fila['ciudad']);
            $co[$fila['idproveedor']]['contacto'] = utf8_decode($fila['contacto']);
            $co[$fila['idproveedor']]['mail'] = utf8_decode($fila['mail']);
            $co[$fila['idproveedor']]['idusuario'] = $fila['idusuario'];
            $co[$fila['idproveedor']]['usuario'] = utf8_decode($fila['usuario']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function save_new_proveedor($name,$address,$city,$contact,$mail,$user){
        $sql = "INSERT INTO proveedor VALUES (0,'$name','$address',$city,'$contact','$mail',$user)";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function update_proveedores($nombre,$direccion,$contacto,$mail,$id,$id_ciudad){
        $sql = "UPDATE proveedor SET nombre='$nombre', direccion='$direccion', idciudad=$id_ciudad, contacto='$contacto', mail='$mail' WHERE idproveedor=$id";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
    function delete_proveedores($id){
        $sql = "DELETE FROM proveedor WHERE idproveedor=$id";
        $this->conexion();
        if(mysqli_query($this->conexion,$sql)){
            $co['estado']=true;
        }
        else{
            $co['estado']=false;
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}

class compras extends ini {
    function save_new_compra($info){
        // $sql = "INSERT INTO compra VALUES (0,$info['date'],$info['folio_auto'],$info['folio_supplier'],$info['supplier_serie'],$info['import'],$info['tax'],$info['discount'],$info['total'],$info['supplier'],$info['user'],$info['payment_method']);"
        // $sql = "INSERT INTO proveedor VALUES (0,'$name','$address',$city,'$contact','$mail',$user)";
        // $this->conexion();
        // if(mysqli_query($this->conexion,$sql)){
        //  $co['estado']=true;
        // }else{
        //  $co['estado']=false;
        // }
        // mysqli_close($this->conexion);
      
        // echo $sql;
        if(isset($co)){return $co;}
    }
    function get_typepayment(){
        $sql = "SELECT * FROM forma_pago";
        $this->conexion();
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co[$fila['idforma_pago']]['name'] = utf8_encode($fila['forma_pago']);
        }
        mysqli_close($this->conexion);
        if(isset($co)){return $co;}
    }
}


        // INI
class ini {
    protected  $conexion;
    function conexion(){
        $user = "salvador_root";
        $pass = "S4lv4d0r.";

        //$user = "root";
        //$pass = "root";

        $server = "localhost";
        // $db  = "haab_test";
        $db     = "salvador_haab";
        
        $this->conexion = new mysqli($server,$user,$pass,$db);
        if(!$this->conexion)var_dump("error al conectar a la base de datos");
        
        
    }
    function send_mail($id_cita,$start_date,$mail,$user_mail,$name_medic,$end_date){
    // function send_mail($id_cita,$start_date,$mail,$user_mail,$name_medic){
        $name_paciente = $this->get_name_paciente($id_cita);
        $arr = explode(' ', $start_date);
        $start_date_new =  $arr[0]."T".$arr[1];
        $start_date_new = str_replace(':', '', str_replace('-', '', $start_date_new))."Z";

        $arr1 = explode(' ', $end_date);
        $end_date_new =  $arr1[0]."T".$arr1[1];
        $end_date_new = str_replace(':', '', str_replace('-', '', $end_date_new))."Z";
       try{
                $mail->isSMTP();
                $mail->Host = "mail.cleverclinic.app";
                $mail->SMTPAuth= true;
                $mail->SMTPSecure = 'ssl';

                $mail->Username = "support@cleverclinic.app";
                    $mail->Password = "S4lv4d0r.";

                    $mail->Port     = 465;

                    $mail->setFrom("support@cleverclinic.app");

                $mail->addAddress($user_mail);
                $mail->CharSet = "UTF-8";
                $mail->Subject = "Clever clinic appointment";
                $mail->Body     = '<table width="100%" style="width:100%;border-spacing:0px" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td><br></td>
                            <td align="center" width="600">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0px" bgcolor="#ffffff">
                                    <tbody>
                                        <tr>
                                            <!-- <td align="center"><b><h1>'.$name_medic.'</h1></b></td> -->
                                            <td aling="center">You have an appointment with <b>'.$name_medic.' on <b>'.$start_date.'</b></b> 
                                            
                                            </td>
                                        </tr>
                                        <tr>
                                            <td aling="center">
                                            Before your session, please fill out your contact information if you read and accept the consent form we will create a confidential clinical file form. 
                                            </td>
                                        </tr>
                                        <!-- *************************** FORMULARIO VIEJO ADALID ************************
                                        <tr>
                                            <td class="m_-6181852313919251156smallDevices" style="padding:0px 0px 0px 0px">
                                                <table width="100%" style="width:100%;border-spacing:0px" border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding:20px 40px;font-family:arial,sans-serif;color:#000000;font-size:20px;line-height:28px"><p style="color:#000000">Hi '.$name_paciente.'.</p>
                                                                <p style="margin:10px auto 0px auto">You hava a reservation at the '.date("Y-m-d h:i A",strtotime($start_date)).', pleace confirm the reservations in the folowing buttons.</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>    
                                        -->
                                        <tr>
                                            <td>
                                                <center style="margin:0px 0px 20px 0px">
                                                    <a href="https://cleverclinic.app/confirm/index.html?id_cita='.$id_cita.'&estado=2&user_id='.$_SESSION['id_user'].'" style="background-color:#3ca564;border-radius:100px;color:#ffffff;display:inline-block;font-family:arial,sans-serif;font-size:20px;line-height:38px;text-align:center;text-decoration:none;width:230px;font-weight:Regular;margin:0px 0px 10px 0px;border:2px solid #3ca564;padding:0px 20px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://listen.deezer.com/wf/click?upn%3DEUZXdKOlbsc2kqBOeTIxno5Z6RFz7zmaKd44AI-2FH6aamiToQjcAim9cmYZyVFnpz_SSPOwhgwRoZceh3F3VqdR2vokdvkoRH5VaXH-2Bb5ARj0akydFGYOmeOEAUZyvxzeyp-2FIQA4gEBiWy-2Fv-2F1tbVEDcCDd0d0nVMcgKpKMsABsK3B1yvB6u3XbdMK-2F8pXOTv7-2BQMhqqOcrZp3dWOlK8GJGRAPEPXjhAk-2BdCSgB-2BhfHi3YhmnfWipfM-2BGWUPJuqBn07B2rgyNKi9Qwfq69dha4RWBZwgpx7OXDNpc7IVyH-2FHV2TUY8iZ1fUnSzysDhCox9jDTmEk-2Ft7GZMFXD3EvgG5ijlb6iEdwxL8qSJTAIKeHB41kTon1Uns2-2BqO2xugheDB6oN-2BWjqmBaP3Fou8-2B1qw8QPZ0NGhWotH2hX8dWXWMs3-2FlX3OHQ2UsUcdn5CxwY1R4yql6CE1-2BX0YrXqd7c9lWDNNVTylRqF6t52GC2JZCloiD3hck6MaX2LJYl6kR-2FyS-2B-2FY7vjWoVq2pgBKjmbKUa0xRo-2BwNhzncapR6jSQ9epx08Dt4WWoEdS-2B3V-2BSmf-2FbhOmczTB9W01kylLOMg-2Bvz1D-2BUjIB5oNN6y3cvhrlInyR-2FSpy3dD7HOvId33NeIWRbOU98BTSf0MqnSIQNf8d5oZUMwMEb0SsYog8gpnYP8U-3D&amp;source=gmail&amp;ust=1565755778467000&amp;usg=AFQjCNEctmtl99zWLj1AKjLodhvrcmsE7A">CONFIRM</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <center style="margin:0px 0px 20px 0px">
                                                    <a href="https://cleverclinic.app/confirm/index.html?id_cita='.$id_cita.'&&estado=4" style="background-color:#f5e447;border-radius:100px;color:#1e1e24;display:inline-block;font-family:arial,sans-serif;font-size:20px;line-height:38px;text-align:center;text-decoration:none;width:230px;font-weight:Regular;margin:0px 0px 10px 0px;border:2px solid #f5e447;padding:0px 20px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://listen.deezer.com/wf/click?upn%3Di-2BuFz6uhAEYV9yY1fh4bqLVb4nXn0GnDSSxxFpgM7Eh6rcce4zPMhiOo6E8zsShwTo2XkPc0DL-2FRp7687-2BmzlXSgWoaOrzpJhfao7dIYrQg5YrQmnxmgsQ31NDM-2ByGbHB8z1vqxi0THd9tz0w9nAiqtZTUqgk6W0EMozG7YuWAsRmsLIdBUI6gwjbQKrA2u8LeKLsDjQbyi-2FNyQvhkocH-2FWqkjWHsUSVcrMgM0q-2BiujhWs1JjXjaW3-2BbVYy6fRtM_SSPOwhgwRoZceh3F3VqdR2vokdvkoRH5VaXH-2Bb5ARj0akydFGYOmeOEAUZyvxzeyp-2FIQA4gEBiWy-2Fv-2F1tbVEDcCDd0d0nVMcgKpKMsABsK3B1yvB6u3XbdMK-2F8pXOTv7-2BQMhqqOcrZp3dWOlK8GJGRAPEPXjhAk-2BdCSgB-2BhfHi3YhmnfWipfM-2BGWUPJuqBn07B2rgyNKi9Qwfq69dha4RWBZwgpx7OXDNpc7IVyH-2FHV2TUY8iZ1fUnSzysDhCox9jDTmEk-2Ft7GZMFXD3EvgG5ijlb6iEdwxL8qSJTAIKeHB41kTon1Uns2-2BqO2xugheDB6oN-2BWjqmBaP3Fou8-2B1qw8QPZ0NGhWotH2hX8dWXWMs3-2FlX3OHQ2UsUcdn5CxwY1R4yql6CE1-2BX0YrXqd7c9lQCxNKtoSqI5Y9cE8gm3XbjIzc0NK1Ww8Fm6iNeVPqUdHg3rD2LkA-2F9v50Zk-2BLTX4HnJaCo3ztdNxTkjNFEoLiRbcKXon5zL-2Fat6FbagUDyHzIxtdcpXNYjB9wqJbC5z91ZzETZ0VjVM45ozVlLkjwKKzn2UcnM5qwYcbPgi-2B-2Bqt50bsM347vA7VphehZBZqti31Wgc4shr2Mwr9dmrOT6o-3D&amp;source=gmail&amp;ust=1565755778467000&amp;usg=AFQjCNGJ44nj5RKcYt-eS9WvyNQZNfM2VA">DECLINE</a>
                                                </center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td><br></td>
                        </tr>
                    </tbody>
                </table>';
                $mail->IsHTML(true);
                $sendMail = $mail->send();
                if($sendMail){
                    return "mensaje enviado";
                }else{
                    return "$mail->ErrorInfo";
                }
        } catch(Exception $ex){
           
        }
    }
    function recover_account($reset_token,$user_mail,$mail){
        try{
            $mail->isSMTP();

            $mail->Host = "mail.cleverclinic.app";
            $mail->SMTPAuth= true;
            $mail->SMTPSecure = 'ssl';

            $mail->Username = "support@cleverclinic.app";
            $mail->Password = "S4lv4d0r.";

            $mail->Port     = 465;

            $mail->setFrom("support@cleverclinic.app");
            
            $mail->addAddress($user_mail);
            $mail->CharSet = "UTF-8";
            $mail->Subject = "Clever clinic, recover your account";
            $mail->Body     = '<table width="100%" style="width:100%;border-spacing:0px" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td><br></td>
                        <td align="center" width="600">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0px" bgcolor="#ffffff">
                                <tbody>
                                    <tr>
                                        <td aling="center">
                                        To reset your account please click on the following link:<a href="https://cleverclinic.app/confirm/reset.html?reset_token='.$reset_token.'" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://listen.deezer.com/wf/click?upn%3DEUZXdKOlbsc2kqBOeTIxno5Z6RFz7zmaKd44AI-2FH6aamiToQjcAim9cmYZyVFnpz_SSPOwhgwRoZceh3F3VqdR2vokdvkoRH5VaXH-2Bb5ARj0akydFGYOmeOEAUZyvxzeyp-2FIQA4gEBiWy-2Fv-2F1tbVEDcCDd0d0nVMcgKpKMsABsK3B1yvB6u3XbdMK-2F8pXOTv7-2BQMhqqOcrZp3dWOlK8GJGRAPEPXjhAk-2BdCSgB-2BhfHi3YhmnfWipfM-2BGWUPJuqBn07B2rgyNKi9Qwfq69dha4RWBZwgpx7OXDNpc7IVyH-2FHV2TUY8iZ1fUnSzysDhCox9jDTmEk-2Ft7GZMFXD3EvgG5ijlb6iEdwxL8qSJTAIKeHB41kTon1Uns2-2BqO2xugheDB6oN-2BWjqmBaP3Fou8-2B1qw8QPZ0NGhWotH2hX8dWXWMs3-2FlX3OHQ2UsUcdn5CxwY1R4yql6CE1-2BX0YrXqd7c9lWDNNVTylRqF6t52GC2JZCloiD3hck6MaX2LJYl6kR-2FyS-2B-2FY7vjWoVq2pgBKjmbKUa0xRo-2BwNhzncapR6jSQ9epx08Dt4WWoEdS-2B3V-2BSmf-2FbhOmczTB9W01kylLOMg-2Bvz1D-2BUjIB5oNN6y3cvhrlInyR-2FSpy3dD7HOvId33NeIWRbOU98BTSf0MqnSIQNf8d5oZUMwMEb0SsYog8gpnYP8U-3D&amp;source=gmail&amp;ust=1565755778467000&amp;usg=AFQjCNEctmtl99zWLj1AKjLodhvrcmsE7A">HERE</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td aling="center">
                                        If it was not you who requested to recover the account, ignore this message.
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td><br></td>
                    </tr>
                </tbody>
            </table>';
            $mail->IsHTML(true);
            if($mail->send()){
                return "mensaje enviado";
            }else{
                return "$mail->ErrorInfo";
            }
        } catch(Exception $ex){
           
        }
    }
    function get_name_paciente($id_cita){
        $sql = "SELECT * FROM v_citas WHERE idcita=$id_cita";
        $res = mysqli_query($this->conexion,$sql);
        while ($fila=mysqli_fetch_array($res)) {
            $co = utf8_encode($fila['nombre']);
        }
        if(isset($co)){return $co;}
    }
    function encript($cadena,$key){
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
        return $encrypted; //Devuelve el string encriptado   
    }
    function str($a){
        $a = trim($a);
        $a = str_replace("'", "''", $a);
        $a = utf8_decode($a);
        return $a;
    }
    function upload_file($encoded_string,$name_file){
        $target_dir = '../files/'; // add the specific path to save the file
        $decoded_file = base64_decode($encoded_string); // decode the file
        $mime_type = finfo_buffer(finfo_open(), $decoded_file, FILEINFO_MIME_TYPE); // extract mime type
        $extension = $this->mime2ext($mime_type); // extract extension from mime type
        $file_dir = $target_dir . $name_file;
        file_put_contents($file_dir, $decoded_file); // save
    }
    /*
    to take mime type as a parameter and return the equivalent extension
    */
    function mime2ext($mime){
        $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp",
        "image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp",
        "image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp",
        "application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg",
        "image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],
        "wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],
        "ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg",
        "video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],
        "kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],
        "rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application",
        "application\/x-jar"],"zip":["application\/x-zip","application\/zip",
        "application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],
        "7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],
        "svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],
        "mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],
        "webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],
        "pdf":["application\/pdf","application\/octet-stream"],
        "pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],
        "ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office",
        "application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],
        "xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],
        "xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel",
        "application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],
        "xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo",
        "video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],
        "log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],
        "wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],
        "tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop",
        "image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],
        "mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar",
        "application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40",
        "application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],
        "cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary",
        "application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],
        "ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],
        "wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],
        "dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php",
        "application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],
        "swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],
        "mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],
        "rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],
        "jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],
        "eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],
        "p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],
        "p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
        $all_mimes = json_decode($all_mimes,true);
        foreach ($all_mimes as $key => $value) {
            if(array_search($mime,$value) !== false) return $key;
        }
        return false;
    }
}
    
?>