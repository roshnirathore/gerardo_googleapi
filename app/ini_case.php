<?php

ini_set("display_errors",1);
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Dompdf\Dompdf;

    include "ini_function.php"; 
    
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 1 Jul 2000 05:00:00 GMT"); // Fecha en el pasado

    switch ($_POST['view']) {
         case 'clinicas': 
            switch ($_POST['tipo']) {
                case '1':
                    $clinicas = new clinicas();
                    $datos_json 	= json_encode($clinicas->get_lista_de_clinicas());
                    echo $datos_json;
                    break;
                case '2':
                    $clinicas = new clinicas();
                    $datos_json 	= json_encode($clinicas->save_new_clinica($_POST['name_clinic'],$_POST['idciudad']));
                    echo $datos_json;
                    break;
                case '3':
                    $clinicas = new clinicas();
                    $datos_json 	= json_encode($clinicas->save_editar_clinica($_POST['name_clinic'],$_POST['idciudad'],$_POST['id_clinica']));
                    echo $datos_json;
                    break;
                case '4':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->get_list_ciudades());
                    echo $datos_json;
                    break;
                default:
                    # code...
                    break;
            }
            break;
        case "config_ciudad":
            switch ($_POST['tipo']) {
                 case '1':
                    $config_ciudades = new config_ciudades();
                    $datos_json 	= json_encode($config_ciudades->get_all_ciudades());
                    echo $datos_json;
                    break;
                case '2':
                    $config_ciudades = new config_ciudades();
                    $datos_json 	= json_encode($config_ciudades->get_datos_input());
                    echo $datos_json;
                    break;
                case '3':
                    $config_ciudades = new config_ciudades();
                    $datos_json 	= json_encode($config_ciudades->save_new_city($_POST['name'],$_POST['id_estado']));
                    echo $datos_json;
                    break;
            }
        break;
        case "reports":
            switch ($_POST['tipo']) {
                case '1':
                    $reports = new reports();
                    $datos_json 	= json_encode($reports->get_issue_invoices());
                    echo $datos_json;
                    break;
                case '2':
                    $reports = new reports();
                    $datos_json 	= json_encode($reports->get_client_of_month());
                    echo $datos_json;
                    break;

                case '3':
                    $reports = new reports();
                    $datos_json     = json_encode($reports->get_raised_invoice_client_details());
                    echo $datos_json;
                    break;

                case '4':
                    $reports = new reports();
                    $datos_json     = json_encode($reports->raised_invoice_filter($_POST['from_date'],$_POST['to_date']));
                    echo $datos_json;
                    break;

                case '5': 
                    $reports = new reports();
                    $datos_json     = json_encode($reports->get_closed_invoice_client_details());
                    echo $datos_json;
                    break;

                case '6':
                    $reports = new reports();
                    $datos_json     = json_encode($reports->closed_invoice_filter($_POST['from_date'],$_POST['to_date']));
                    echo $datos_json;
                    break; 

                /*case '7':
                    $reports = new reports();
                    $datos_json     = json_encode($reports->client_total_invoices());
                    echo $datos_json;
                    break;
*/
            }
            break;
        case "medicos":
            switch($_POST['tipo']){
                case '1':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->get_lista_medicos($_POST['id_sucursal']));
                    echo $datos_json;
                    break;
                case '2':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->get_datos_medicos());
                    echo $datos_json;
                    break;
                case '3':
                    $medicos = new medicos();
                    
                    // $datos_json 	= json_encode($medicos->add_new_medic($_POST['form_modal_medico_name'],$_POST['form_modal_medico_bussines_name'],$_POST['form_modal_medico_trecord'],$_POST['id_sucursal'],$_POST['id_usuario_ref'],$_POST['form_modal_medico_serie'],$_POST['form_modal_medico_folio'],
                    //     $_POST['checkin'],$_POST['checkout'],$_POST['terms_conditions'],
                    //     $_FILES["new_pickture_input"]["tmp_name"],$_FILES["new_pickture_input"]["name"]));

                     $datos_json    = json_encode($medicos->add_new_medic($_POST['form_modal_medico_name'],$_POST['form_modal_medico_bussines_name'],$_POST['form_modal_medico_trecord'],$_POST['id_sucursal'],$_POST['id_usuario_ref'],$_POST['form_modal_medico_serie'],$_POST['form_modal_medico_folio'], $_POST['checkin'],$_POST['checkout'],$_POST['terms_conditions'],$_FILES["new_pickture_input"]["tmp_name"],$_FILES["new_pickture_input"]["name"],$_POST['form_modal_mobile_number']));
                    echo $datos_json;
                    break;
                case '4':
                    $medicos = new medicos();
                    //print_r($_POST);die;
                    // $datos_json 	= json_encode($medicos->update_data_medic($_POST['form_modal_edit_medico_name'],
                    // $_POST['form_modal_edit_medico_bussines_name'],$_POST['form_modal_edit_medico_trecord'],$_POST['id_medico'], 
                    // $_POST['id_usuario_ref'],$_POST['form_modal_edit_medico_serie'],$_POST['form_modal_edit_medico_folio'],
                    // $_POST['checkinEdit'],$_POST['checkoutEdit'],$_POST['terms_conditions'],
                    // $_FILES["change_pickture_input"]["tmp_name"],$_FILES["change_pickture_input"]["type"]));
                    $datos_json     = json_encode($medicos->update_data_medic($_POST['form_modal_edit_medico_name'],
                    $_POST['form_modal_edit_medico_bussines_name'],$_POST['form_modal_edit_medico_trecord'],$_POST['id_medico'], 
                    $_POST['id_usuario_ref'],$_POST['form_modal_edit_medico_serie'],$_POST['form_modal_edit_medico_folio'],
                    $_POST['checkinEdit'],$_POST['checkoutEdit'],$_POST['terms_conditions'],
                    $_FILES["change_pickture_input"]["tmp_name"],$_FILES["change_pickture_input"]["type"],$_POST['form_modal_edit_mobile_number'],$_FILES["editable_pdf"]["tmp_name"],$_FILES["editable_pdf"]["type"]));
                    echo $datos_json;
					break;
				case '5':
					$medicos = new medicos();
                    
					$datos_json 	= json_encode($medicos->get_lista_usuarios($_POST['id_sucursal']));
					echo $datos_json;
					break;
            }
            break;
        case 'login':
            switch ($_POST['tipo']) {
                case '1':
                    $login = new login();
                    $datos_json 	= json_encode($login->validar_login($_POST['user_name'],$_POST['password']));
                    echo $datos_json;
                    break;
                case "2":
                    $login = new login();
                    $datos_json 	= json_encode($login->logout());
                    echo $datos_json;
                    break;
				case "3":
                    $login = new login();
                    $datos_json 	= json_encode($login->validar_login_old());
                    echo $datos_json;
                    break;
				case "4":
                    $login = new login();
                    require '../assets/php/PHPMailer-master/src/Exception.php';
                    require '../assets/php/PHPMailer-master/src/PHPMailer.php';
                    require '../assets/php/PHPMailer-master/src/SMTP.php';
                    $mail = new PHPMailer();
                    $datos_json 	= json_encode($login->recover_password($_POST['recoveraccount'],$mail));
                    echo $datos_json;
                    break;
            }
            break;
        
        case "citas":
            switch ($_POST['tipo']) {
                case '1':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->get_lista_de_eventos($_POST['id_clinica'],$_POST['id_medico']));
                    echo $datos_json;
                    break;
                case '2':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->get_datos_input($_POST['idtipo_usuario']));
                    echo $datos_json;
                    break;
                case '3':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->get_pacientes());
                    echo $datos_json;
                    break;
                case '4':
                    /*ZAPIER EXPERT'S CODE START*/
                    if($_POST['video_conf'] == 1){
                        $opentok = new customOpenTok();
                        $sessionId = "";
                        $apiKey = $opentok->apiKey;
                        $sessionId = $opentok->generateSession();
                    }else{
                        $sessionId = "";
                    }
                    /*ZAPIER EXPERT'S CODE END*/

                    require '../assets/php/PHPMailer-master/src/Exception.php';
                    require '../assets/php/PHPMailer-master/src/PHPMailer.php';
                    require '../assets/php/PHPMailer-master/src/SMTP.php';
                    $mail = new PHPMailer();
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->add_new_date($_POST['id_sucursal'],$_POST['id_paciente'],$_POST['id_medico'],$_POST['start_date'],$_POST['end_date'],$_POST['id_servicio'],$mail,$_POST['user_mail'],$_POST['name_medic'],$sessionId));
                    echo $datos_json;
                    break; 
                case '5':
                    $psii_citas = new psii_citas();
                    //$datos_json 	= json_encode($psii_citas->save_historial_clinico($_POST['temp_data']['estado_cita'],$_POST['temp_data']['id_cita'],$_POST['temp_data']['notas'],$_POST['temp_data']['observaciones'],$_POST['temp_data']['problematica'],$_POST['temp_data']['tratamiento'],$_POST['temp_file'],$_POST['temp_data']['id_medico']));
                    echo $datos_json;
                    break;
                case '6':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->save_new_patient($_POST['info']['name'],$_POST['info']['date'],$_POST['info']['address'],$_POST['info']['city'],$_POST['info']['phone'],$_POST['info']['email']));
                    echo $datos_json;
                    break;
                case '7':
                    $psii_citas = new psii_citas();
                    // var_dump($_POST);
                    // $datos_json 	= json_encode($psii_citas->save_historial_clinico($_POST['modal_form_historial_estado'],$_POST['id_cita'],$_POST['modal_form_historial_nombre_notas'],$_POST['modal_form_historial_nombre_observacion'],$_POST['modal_form_historial_nombre_problematica'],$_POST['modal_form_historial_nombre_tratamiento'],$_FILES["inputGroupFile01"]["tmp_name"],$_POST['id_medico'],$_FILES["inputGroupFile01"]["name"]));
                    $datos_json 	= json_encode(
                        $psii_citas->save_historial_clinico(
                            $_POST['id_medico'],
                            $_POST['modal_form_historial_estado'],
                            $_POST['modal_form_type_session'],
                            $_POST['id_cita'],
                            $_POST['table_name'],
                            $_POST['columns']));
                    echo $datos_json;
                    break;
                case '8':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode($psii_citas->cancelar_cita($_POST['id_cita']));
                    echo $datos_json;
                    break;
                case '9':
                    $psii_citas = new psii_citas();
                    $datos_json 	= json_encode(
                        $psii_citas->save_files_of_forms(
                            $_POST['table_name'],
                            $_POST['last_id'],
                            $_FILES));
                    echo $datos_json;
                    break;
                case '10':
                    $psii_citas = new psii_citas();
                    $datos_json     = json_encode($psii_citas->appoinment_status($_POST['id_cita'],$_POST['appointment_status']));
                    echo $datos_json;
                    break;
                default:
                    # code...
                    break;
            }
            break;
        case 'usuarios':
            switch ($_POST['tipo']) {
                case '1':
                    $usuarios = new usuarios();
                    $datos_json 	= json_encode($usuarios->get_lista_de_usuarios($_POST['idsucursal']));
                    echo $datos_json;
                    break;
                case '2':
                    $usuarios = new usuarios();
                    $datos_json 	= json_encode($usuarios->get_datos_input());
                    echo $datos_json;
                    break;
                case '3':
                    $usuarios = new usuarios();
                    //print_r($_POST);
                    //die;
                    // $datos_json 	= json_encode($usuarios->save_new_user($_POST['form'][0]['value'],$_POST['form'][1]['value'],$_POST['form'][2]['value'],$_POST['form'][3]['value'],$_POST['form'][4]['value'],$_POST['id_clinica']));
                    $datos_json     = json_encode($usuarios->save_new_user($_POST['form'][0]['value'],$_POST['form'][1]['value'],$_POST['form'][2]['value'],$_POST['form'][3]['value'],$_POST['form'][4]['value'],$_POST['form'][5]['value'],$_POST['id_clinica'],''));
                    echo $datos_json;
                    break;
                case '4':
                    $usuarios = new usuarios();
                    $form = $_POST['info'];
                    // $datos_json 	= json_encode($usuarios->update_data_usuarios($form[0]['value'],$form[1]['value'],$form[2]['value'],$form[3]['value'],$form[4]['value'],$_POST['id_usuario']));
                    $datos_json     = json_encode($usuarios->update_data_usuarios($form[0]['value'],$form[1]['value'],$form[2]['value'],$form[3]['value'],$form[4]['value'],$form[5]['value'],$_POST['id_usuario']));
                    echo $datos_json;
                    break;
                default:
                    # code...
                    break;
            }
            break;
        case "pacientes":
            switch ($_POST['tipo']) {
                case '1':
                    $id = $_SESSION['id_user'];
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->get_lista_de_pacientes($id));
                    echo $datos_json;
                    break;
                case '2':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->save_new_patient($_POST['info']['name'],$_POST['info']['date'],$_POST['info']['address'],$_POST['info']['city'],$_POST['info']['phone'],$_POST['info']['email']));
                    echo $datos_json;
                    break;
                case '3':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->get_inputs());
                    echo $datos_json;
                    break;
                case '4':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->get_lista_de_expedientes($_POST['id_paciente']));
                    echo $datos_json;
                    break;
                case '5':
                    $pacientes = new pacientes();
                    
                    //$post = $_POST['info']; 
                    //$datos_json 	= json_encode($pacientes->update_data_paciente($post[0]["value"],$post[1]["value"],$post[2]["value"],$post[3]["value"],$post[4]["value"],$post[5]["value"],$_POST['id_paciente'],$_POST['id_city']));
                    $datos_json     = json_encode($pacientes->update_data_paciente($_POST["modal_edit_aptient_name"],$_POST["modal_edit_aptient_date"],$_POST["modal_edit_aptient_address"],$_POST["modal_edit_aptient_city"],$_POST["modal_edit_aptient_phone"],$_POST["modal_edit_aptient_email"],$_POST['id_paciente'],$_POST['id_city'],$_FILES["modal_edit_aptient_pdf"]["tmp_name"][0],$_FILES["modal_edit_aptient_pdf"]["type"][0]));
                    echo $datos_json;
                    break;
                case '6':
                    require_once '../assets/php/dompdf/autoload.inc.php';
                    $dompdf = new Dompdf();
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->export_file_patient($_POST['datos'],$dompdf));
                    echo $datos_json;
                    break;
                case '7':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->update_data_historico($_POST['info'][0]['value'],$_POST['info'][1]['value'],$_POST['info'][2]['value'],$_POST['info'][3]['value'],$_POST['id_historico']));
                    echo $datos_json;
                    break;
                case '8':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->get_lista_de_clientes());
                    echo $datos_json;
                    break;
                case '9':
                    $pacientes = new pacientes();
                    $post = $_POST['info'];
                    $datos_json 	= json_encode($pacientes->update_data_cliente($post[0]["value"],$post[1]["value"],$post[2]["value"],$post[3]["value"],$post[4]["value"],$post[5]["value"],$_POST['id_paciente'],$_POST['id_city']));
                    echo $datos_json;
                    break;
                case '10':
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->save_new_client($_POST['info']['name'],$_POST['info']['date'],$_POST['info']['address'],$_POST['info']['city'],$_POST['info']['phone'],$_POST['info']['email']));
                    echo $datos_json;
                    break;
                case '11': 
                    $pacientes = new pacientes();
                    $datos_json 	= json_encode($pacientes->get_lista_clients_information());
                    echo $datos_json;
                    break;
            }
            break;
        case 'invoices':
            switch ($_POST["tipo"]) {
                case '1': 
                     $invoices = new invoices();
                    $datos_json 	= json_encode($invoices->get_datos_iniciales($_POST['idtipo_usuario']));
                    echo $datos_json;
                    break;
                case '2':
                     $invoices = new invoices();
                    $datos_json 	= json_encode($invoices->get_citas_of_medical($_POST['id_medico']));
                    echo $datos_json;
                    break;
                case "3":
                    require_once '../assets/php/dompdf/autoload.inc.php';
                    require '../assets/php/PHPMailer-master/src/Exception.php';
                    require '../assets/php/PHPMailer-master/src/PHPMailer.php';
                    require '../assets/php/PHPMailer-master/src/SMTP.php';
                    $mail = new PHPMailer();
                    $invoices = new invoices();
                    $dompdf = new Dompdf();
                    $datos_json 	= json_encode($invoices->send_invoice($_POST['idcita'],$dompdf,$mail,$_POST['info'],$_POST['id_cliente'],$_POST['email_cliente'],$_POST['client_name'],$_POST['idestadoinvoice'],$_POST['idtype_payment'],$_POST['edit_client_name']));
                    echo $datos_json;
					break;
				case '4':
					$invoices = new invoices();
					$datos_json 	= json_encode($invoices->get_datos_input());
					echo $datos_json;
					break;
				case '5':
					$invoices = new invoices();
					$datos_json 	= json_encode($invoices->insert_new_cliente($_POST['name'],$_POST['date'],$_POST['address'],$_POST['city'],$_POST['phone'],$_POST['email']));
					echo $datos_json;
                    break;
                case '6':
                    require_once '../assets/php/dompdf/autoload.inc.php';
                    $invoices = new invoices();
                    $dompdf = new Dompdf(); 
                    $datos_json 	= json_encode($invoices->download_invoice_pdf($_POST['id_invoice'],$_POST['cliente_name_edit'],$dompdf));
                    echo $datos_json;
                    break;
                default:
                    # code...
                    break;
            }
           
            break;


        case "ciudades":
            switch($_POST['tipo']){
                case '1':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->get_lista_medicos($_POST['id_sucursal']));
                    echo $datos_json;
                    break;
                case '2':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->get_datos_medicos());
                    echo $datos_json;
                    break;
                case '3':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->add_new_medic($_POST['form_modal_medico_name'],$_POST['form_modal_medico_bussines_name'],$_POST['form_modal_medico_trecord'],$_POST['id_sucursal'],$_POST['id_usuario_ref'],$_POST['form_modal_medico_serie'],$_POST['form_modal_medico_folio'],$_FILES["new_pickture_input"]["tmp_name"],$_FILES["new_pickture_input"]["type"]));
                    echo $datos_json;
                    break;
                case '4':
                    $medicos = new medicos();
                    $datos_json 	= json_encode($medicos->update_data_medic($_POST['form_modal_edit_medico_name'],$_POST['form_modal_edit_medico_bussines_name'],$_POST['form_modal_edit_medico_trecord'],$_POST['id_medico'], $_POST['id_usuario_ref'],$_POST['form_modal_edit_medico_serie'],$_POST['form_modal_edit_medico_folio'],$_FILES["change_pickture_input"]["tmp_name"],$_FILES["change_pickture_input"]["type"]));
                    echo $datos_json;
					break;
				case '5':
					$medicos = new medicos();
					$datos_json 	= json_encode($medicos->get_lista_usuarios($_POST['id_sucursal']));
					echo $datos_json;
					break;
            }
            break;
        case "provincia":
            switch($_POST['tipo']){
                case '1':
                    $provincia = new provincia();
                    $datos_json 	= json_encode($provincia->get_lista_de_provincia());
                    echo $datos_json;
                break;
                case '2':
                    $provincia = new provincia();
                    $datos_json 	= json_encode($provincia->save_new_provincia($_POST['info']['province'],$_POST['info']['country'],$_POST['info']['user']));
                    echo $datos_json;
                break;
            }
            break;
        case "servicios":
            switch($_POST['tipo']){
                case '1':
                    $servicios = new servicios();
                    $datos_json 	= json_encode($servicios->get_lista_servicios());
                    echo $datos_json;
                break;
                case '2':
                    $servicios = new servicios();
                    $datos_json 	= json_encode($servicios->save_new_service($_POST['info']['service'],$_POST['info']['amount'],$_POST['info']['time'],$_POST['info']['tax'],$_POST['info']['user'],$_POST['info']['idmedico']));
                    echo $datos_json;
                break;
                case '3':
                    $servicios = new servicios();
                    $form = $_POST['info'];
                    $datos_json 	= json_encode($servicios->update_data_servicios($form[0]['value'],$form[1]['value'],$form[2]['value'],$form[3]['value'],$_POST['id_servicio'],$_POST['idmedico']));
                    echo $datos_json;
                break;
            }
        break;
        case "productos":
            switch($_POST['tipo']){
                case '1':
                    $productos = new productos();
                    $datos_json 	= json_encode($productos->get_lista_productos());
                    echo $datos_json;
                break;
                case '2':
                    $productos = new productos();
                    $datos_json 	= json_encode($productos->save_new_product($_POST['info']['key'],$_POST['info']['name'],$_POST['info']['cost'],$_POST['info']['tax'],$_POST['info']['user']));
                    echo $datos_json;
                break;
                case '3':
                    $productos = new productos();
                    $form = $_POST['info'];
                    $datos_json 	= json_encode($productos->update_product($form[0]['value'],$form[1]['value'],$form[2]['value'],$form[3]['value'],$_POST['id']));
                    echo $datos_json;
                break;
                case '4':
                    $productos = new productos();
                    $datos_json 	= json_encode($productos->delete_product($_POST['id']));
                    echo $datos_json;
                break;
            }
        break;
        case "proveedores":
            switch($_POST['tipo']){
                case '1':
                    $proveedores = new proveedores();
                    $datos_json 	= json_encode($proveedores->get_lista_proveedores());
                    echo $datos_json;
                break;
                case '2':
                    $proveedores = new proveedores();
                    $datos_json 	= json_encode($proveedores->save_new_proveedor($_POST['info']['name'],$_POST['info']['address'],$_POST['info']['city'],$_POST['info']['contact'],$_POST['info']['mail'],$_POST['info']['user']));
                    echo $datos_json;
                break;
                case '3':
                    $proveedores = new proveedores();
                    $form = $_POST['info'];
                    $datos_json 	= json_encode($proveedores->update_proveedores($form[0]['value'],$form[1]['value'],$form[3]['value'],$form[4]['value'],$_POST['id'],$_POST['idciudad']));
                    echo $datos_json;
                break;
                case '4':
                    $proveedores = new proveedores();
                    $datos_json 	= json_encode($proveedores->delete_proveedores($_POST['id']));
                    echo $datos_json;
                break;
            }
        break;
        case "compras": 
            switch($_POST['tipo']){
                case '2':
                    $compras = new compras();
                    $datos_json 	= json_encode($compras->save_new_compra($_POST['info']));
                    var_dump($_POST);
                    echo $datos_json;
                break;
                case '5':
                    $compras = new compras();
                    $datos_json 	= json_encode($compras->get_typepayment());
                    echo $datos_json;
                break;
            }
        break;

        /*ZAPIER EXPERT'S CODE START*/

        case 'opentok':
            switch($_POST['tipo']){
                case '1':
                    $test = "hoa";
                break;
                case '2':
                    $psii_citas = new psii_citas();
                    echo json_encode($psii_citas->get_latest_eventos());
                    // echo json_encode($psii_citas->get_datos_using_date("2020-01-06"));
                    /*$opentok = new customOpenTok();
                    $sessionId = "";
                    $apiKey = $opentok->apiKey;
                    if(isset($_POST['sessionId']) && !empty($_POST['sessionId'])){
                        $sessionId = $_POST['sessionId'];
                    }
                    $token = $opentok->genrateToken($sessionId);
                    $sessionId = $opentok->sessionId;
                    echo json_encode(array("apiKey" => $apiKey, "sessionId" => $sessionId, "token" => $token));*/
                break;
                case '3':
                    if(!isset($_POST['citaId']) && empty($_POST['citaId'])){
                        echo json_encode(array("error" => 1, "message" => "please try again, something went wrong"));
                        die();
                    }
                    $psii_citas = new psii_citas();
                    $datos_json = $psii_citas->get_datos_new_cita($_POST['citaId']);
                    if (isset($datos_json['open_tok_session']) && $datos_json['open_tok_session'] != $_POST['sessionId']){
                        echo json_encode(array("error" => 1, "message" => "Url is not correct"));
                        die();
                    }elseif (!isset($datos_json['open_tok_session'])) {
                        echo json_encode(array("error" => 1, "message" => "Url is not correct"));
                        die();
                    }
                    $date = date("Y-m-d H:i:s"); 
                    $current = strtotime($date);
                    $startDate = strtotime($datos_json['finicio']);
                    $endDate = strtotime($datos_json['ffinal']); 
                    if($current < $startDate){
                        echo json_encode(array("error" => 1, "message" => "Currently this session is not start."));
                        die();
                    }
                    if($current >= $endDate){
                        echo json_encode(array("error" => 1, "message" => "Your session time has gone."));
                        die();
                    }
                    $opentok = new customOpenTok();
                    $sessionId = "";
                    $apiKey = $opentok->apiKey;
                    if(isset($_POST['sessionId']) && !empty($_POST['sessionId'])){
                        $sessionId = $_POST['sessionId'];
                    }else{
                        echo json_encode(array("error" => 1, "message" => "please try again, something went wrong"));
                        die();
                    }
                    $token = $opentok->genrateToken($sessionId);
                    $sessionId = $opentok->sessionId;
                    echo json_encode(array("error" => 0, "apiKey" => $apiKey, "sessionId" => $sessionId, "token" => $token));
                break;
            }

            /*ZAPIER EXPERT'S CODE END*/

        break;
    }
?>