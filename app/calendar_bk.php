<?php

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/calendar/v3/calendars/primary/events?maxAttendees=1&sendNotifications=true&key=AIzaSyDVMnc5wmn1YUor6EOy5WvvFiMSg_WFGSw');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
  'summary' => 'Google I/O 2015',
  'location' => '800 Howard St., San Francisco, CA 94103',
  'description' => 'cREATD VIA api',
  'start' => array(
    'dateTime' => '2020-02-06T10:00:00-07:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2020-02-06T11:00:00-07:00',
    'timeZone' => 'America/Los_Angeles',
  ),
   'recurrence' => array(
                    'RRULE:FREQ=DAILY;COUNT=1'
                   ),
    'attendees' => array(
      array('email' => 'gaurav.jain952@gmail.com',
          'responseStatus' => 'needsAction'),
    ),
     'reminders'=> array(
                        'useDefault' => false,
                        'overrides' => array(
                        array('method' => 'email', 'minutes' => 24 * 60),
                        array('method' => 'email', 'minutes' => 48 * 60),
                      ),
 

                    ),

)));
curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');


// echo accessToken();die;
$headers = array();
$headers[] = 'Authorization: Bearer '.accessToken();
$headers[] = 'Accept: application/json';
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
print($result);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);



function accessToken()
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://www.googleapis.com/oauth2/v4/token",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "refresh_token=1//0fl20thtx-6uxCgYIARAAGA8SNwF-L9IrLvpHM9nibnPkyTxCRogtdhV4hVn7eBWCgGpJM7VfTGasoRdp81lOg4melb4Es16akl8&client_id=853700852482-290ea0qpvm3qr0g7p872stafseb755cn.apps.googleusercontent.com&client_secret=HdzBAPWBp3Yv9bgBwqoaJo_q&grant_type=refresh_token",
    CURLOPT_HTTPHEADER => array(
           "Cache-Control: no-cache",
           "Content-Type: application/x-www-form-urlencoded",
           "Postman-Token: 57d46e6f-d560-4931-a8be-b9dcc1e9698c",
          ),
        ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    return $access_token = json_decode($response)->access_token;
}
