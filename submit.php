<?php

if (isset($_POST['ajax_file_save'])) {
    $web_pic = $_POST['image_data'];

    try {

        $data = explode(',', $web_pic);
        $pic = @base64_decode($data[1]);

        $file_name = uniqid(time() . rand(1, 100));
        $extan = ".png";
        $r = @file_put_contents("uploads/temp/" . $file_name . $extan, $pic);

        echo json_encode(array(
            'status' => 'SUCCESS',
            'data' => $file_name . $extan
        ));
    } catch (Exception $e) {
        echo json_encode(array(
            'status' => 'FALIURE',
            'data' => null
        ));
    }
}

?>