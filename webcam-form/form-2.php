<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Webcamp Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

	<style>
		.form-group label:not(.checkbox-inline) {
			text-transform: uppercase;
		}
		.write_canvas {
			background: #fff; 
			display: none;
		}
		.write_canvas.active {
			display: block;
		}
		.img-thumbnail {
			margin: 2px;
		}
        .img-thumbnail_2 {
            display: inline-block;
            max-width: 100%;
            height: auto;
            padding: 4px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            margin: 2px;
        }
        .img-thumbnail_3 {
            display: inline-block;
            max-width: 100%;
            height: auto;
            padding: 4px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            margin: 2px;
        }
        .img-thumbnail_4 { 
            display: inline-block;
            max-width: 100%;
            height: auto;
            padding: 4px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            margin: 2px;
        }
	</style>
</head>

<body>
	<h1 class="text-center">Clinical Note Scan</h1>
	<form action="submit-2.php" method="POST">
		<div class="container">

			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
				<div class="form-group">
						<h4 style="font-size: 15px; font-weight: 600;">TYPE OF THERAPY</h4>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="In person"> In person
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Over the phone"> Over the phone
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Teleconference"> Teleconference
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Secure text or email"> Secure text or email
						</label>
					</div>
					<div class="form-group">
						<label for="client_name">CLIENT'S NAME</label>
						<input type="text" class="form-control" name="client_name" id="client_name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<label for="date">DATE</label>
						<input type="date" class="form-control" id="date" name="date" placeholder="DATE" required value="<?= date('Y-m-d') ?>">
					</div>
					<div class="form-group">
						<label for="clinical_notes">PRESENTING PROBLEM</label>
						<textarea class="form-control" rows="3" name="presenting_problem" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">clinical observations</label>
						<textarea class="form-control" rows="3" name="clinical_observations" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">Treatment/clinical intervention</label>
						<textarea class="form-control" rows="3" name="clinical_intervention" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">homework</label>
						<textarea class="form-control" rows="3" name="homework" required></textarea>
					</div>

					<div class="form-group">
						<label for="observations">observations</label>
						<textarea class="form-control" rows="3" name="observations" required></textarea>
					</div>


					<div class="form-group">
						<label for="observations">CLINICAL NOTE & HANDWRITING</label>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<button type="button" class="btn btn-default form-control" onclick="open_modal('clinical_note_scan')">Click here to open handwriting mode</button>
								<div id="img-tst" class="text-center" style="margin-top: 10px;"></div>
								<div id="img-name"></div>
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>

					<div class="form-group">
						<label for="therapist_name">THERAPIST NAME</label>
						<input type="text" class="form-control" id="therapist_name" name="therapist_name" placeholder="THERAPIST NAME" required>
					</div>
					<button type="submit" class="btn btn-default" name="submit">Submit</button>
				</div>
				<div class="col-md-3">

				</div>
			</div>
		</div>
	</form>



	<!-- Modal -->
	<div class="modal fade" id="webcam" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body" style="background: #f3f3f3">
					<div class="row">
						<div class="col-sm-4" style="margin-bottom: 10px;">
							<button type="button" class="btn btn-default" id="prev_btn" onclick="change_page('prev')" disabled>Previous</button>
							<button type="button" class="btn btn-default" id="next_btn" onclick="change_page('next')" disabled>Next</button>
							|
							<button type="button" class="btn btn-default" onclick="addPage()">Add Page</button>
						</div>
						<div class="col-sm-4 text-center">
							<span id="page_text">1 of 1</span>
						</div>
						<div class="col-sm-4 text-right" style="margin-bottom: 10px;">
						
							<button type="button" class="btn btn-default tool_btn" id="select_pen" onclick="change_tool(this, 'pen')" disabled>Pen</button>
							<button type="button" class="btn btn-default tool_btn" id="select_eraser" onclick="change_tool(this, 'eraser')">Eraser</button>
							|
							<button type="button" class="btn btn-danger" onclick="clear_canvas();">Clear</button>

							<button type="button" class="btn btn-primary" id="save_btn" onclick="save_image();">Save</button>
						</div>
					</div>
					<div id="iframe">

					</div>
				</div>
			</div>

		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

	<script type="text/javascript">
		
		var selected_tool = 'pen';

		$('#webcam').on('shown.bs.modal', function() {
			var modal_div = document.getElementById('iframe'); 
			modal_div.innerHTML = '';

//			$("#img-tst").html('');
//			$("#img-name").html('');
			createCanvas();
		})

		function createCanvas() {
			var modal_div = document.getElementById('iframe'); 
			
			var new_id = modal_div.getElementsByClassName('write_canvas').length;

			var canvas = document.createElement('canvas');
			canvas.id = 'canvas_' + new_id;
			canvas.className = 'write_canvas active';
			canvas.width = modal_div.clientWidth;
			canvas.height = (window.innerHeight - (50 + 30 + 56 + 15 + 44));

			modal_div.appendChild(canvas);

			initialize(canvas);
			update_btn_status()
		}

		function addPage() {
			var all_canvas = document.getElementsByClassName('write_canvas');

			// remove active class from all canvas
			for (var i=0; i<all_canvas.length; i++) {
				all_canvas[i].className = 'write_canvas';
			}

			createCanvas();
			update_btn_status()
		}

		function update_page_text() {
			var elm = document.getElementById('page_text');
			var total = document.getElementsByClassName('write_canvas').length

			var nodes = Array.prototype.slice.call( document.getElementById('iframe').children );
			var selected = document.querySelector('.write_canvas.active');
			var i = nodes.indexOf(selected) + 1;

			elm.innerText = i + ' of ' + total;
		}

		function update_btn_status() {
			$active = $('.write_canvas.active');

			$('#prev_btn').attr('disabled', true);
			$('#next_btn').attr('disabled', true);

			if ($active.prev('.write_canvas').length)
				$('#prev_btn').attr('disabled', false);

			if ($active.next('.write_canvas').length)
				$('#next_btn').attr('disabled', false);

			update_page_text();
		}

		function change_page(type) {
			$active = $('.write_canvas.active');

			if (type == 'next') {
				if ($active.next('.write_canvas').length) {
					$active.removeClass('active');
					$active.next('.write_canvas').addClass('active');
				}
			} else {
				if ($active.prev('.write_canvas').length) {
					$active.removeClass('active');
					$active.prev('.write_canvas').addClass('active');
				}
			}

			update_btn_status()
		}

		function change_tool(selector, tool) {
			tool = tool || 'pen';
			selected_tool = tool;

			$('.tool_btn').attr('disabled', false);
			$(selector).attr('disabled', true);
		}

		/***********************************/
		/* Start Canvas */
		// works out the X, Y position of the click inside the canvas from the X, Y position on the page
		function getPosition(mouseEvent, sigCanvas) {
			var rect = sigCanvas.getBoundingClientRect();
			return {
				X: mouseEvent.clientX - rect.left,
				Y: mouseEvent.clientY - rect.top
			};
		}

		function initialize(sigCanvas) {
			// get references to the canvas element as well as the 2D drawing context
			//var sigCanvas = document.getElementById("canvas");
			var context = sigCanvas.getContext("2d");
			context.lineWidth = 1;
			context.lineJoin = "round";
			context.lineCap = "round";
			context.imageSmoothingQuality = "high";

			context.fillStyle = "white";
			context.fillRect(0, 0, sigCanvas.width, sigCanvas.height);


			// This will be defined on a TOUCH device such as iPad or Android, etc.
			var is_touch_device = 'ontouchstart' in document.documentElement;

			
			// create a drawer which tracks touch movements
			var drawer = {
				isDrawing: false,
				touchstart: function(coors) {
					context.beginPath();
					context.moveTo(coors.x, coors.y);
					this.isDrawing = true;
				},
				touchmove: function(coors) {
					if (this.isDrawing) {
						if (selected_tool == 'eraser') {
							context.strokeStyle = '#ffffff';
							context.lineWidth = 20;
						} else {
							context.strokeStyle = '#000000';
							context.lineWidth = 2;
						}

						context.lineTo(coors.x, coors.y);
						context.stroke();
					}
				},
				touchend: function(coors) {
					if (this.isDrawing) {
						this.touchmove(coors);
						this.isDrawing = false;
					}
				}
			};

			// create a function to pass touch events and coordinates to drawer
			function draw(event) {

				// get the touch coordinates.  Using the first touch in case of multi-touch
				var coors = {
					x: event.targetTouches[0].pageX,
					y: event.targetTouches[0].pageY
				};

				// Now we need to get the offset of the canvas location
				var obj = sigCanvas;

				if (obj.offsetParent) {
					// Every time we find a new object, we add its offsetLeft and offsetTop to curleft and curtop.
					do {
						coors.x -= obj.offsetLeft;
						coors.y -= obj.offsetTop;
					}
					// The while loop can be "while (obj = obj.offsetParent)" only, which does return null
					// when null is passed back, but that creates a warning in some editors (i.e. VS2010).
					while ((obj = obj.offsetParent) != null);
				}

				// pass the coordinates to the appropriate handler
				drawer[event.type](coors);
			}

			// attach the touchstart, touchmove, touchend event listeners.
			sigCanvas.addEventListener('touchstart', draw, false);
			sigCanvas.addEventListener('touchmove', draw, false);
			sigCanvas.addEventListener('touchend', draw, false);

			// prevent elastic scrolling
			sigCanvas.addEventListener('touchmove', function(event) {
				event.preventDefault();
			}, false);
			
			if (!is_touch_device) {

				// start drawing when the mousedown event fires, and attach handlers to
				// draw a line to wherever the mouse moves to
				$(sigCanvas).mousedown(function(mouseEvent) {
					var position = getPosition(mouseEvent, sigCanvas);
					context.lineWidth = 1;
					context.lineJoin = "round";
					context.lineCap = "round";
					context.imageSmoothingQuality = "high";

					context.moveTo(position.X, position.Y);
					context.beginPath();

					// attach event handlers
					$(this).mousemove(function(mouseEvent) {
						drawLine(mouseEvent, sigCanvas, context);
					}).mouseup(function(mouseEvent) {
						finishDrawing(mouseEvent, sigCanvas, context);
					}).mouseout(function(mouseEvent) {
						finishDrawing(mouseEvent, sigCanvas, context);
					});
				});

			}
		}

		// draws a line to the x and y coordinates of the mouse event inside
		// the specified element using the specified context
		function drawLine(mouseEvent, sigCanvas, context) {

			var position = getPosition(mouseEvent, sigCanvas);

			if (selected_tool == 'eraser') {
				context.strokeStyle = '#ffffff';
				context.lineWidth = 20;
			} else {
				context.strokeStyle = '#000000';
				context.lineWidth = 2;
			}

			context.lineTo(position.X, position.Y);
			context.stroke();
		}

		// draws a line from the last coordiantes in the path to the finishing
		// coordinates and unbind any event handlers which need to be preceded
		// by the mouse down event
		function finishDrawing(mouseEvent, sigCanvas, context) {
			// draw the line to the finishing coordinates
			drawLine(mouseEvent, sigCanvas, context);

			context.closePath();

			// unbind any events which could draw
			$(sigCanvas).unbind("mousemove")
				.unbind("mouseup")
				.unbind("mouseout");
		}

		/* End Canvas */
		/***********************************/


		function save_image() {
			var all_canvas = document.getElementsByClassName('write_canvas');
			var i = 0;

			$('#save_btn').attr('disabled', true);

			function __save(canvas) {
				var capturedImage = canvas.toDataURL("image/png");

				$.ajax({
					type: 'POST',
					url: "submit-2.php", 
					data: {
						ajax_file_save: 1,
						image_data: capturedImage
					},
					success: function(result){
						var result = JSON.parse(result);
						console.log(result)

                        var type = $('#webcam').attr('data-type');

						if (type == 'clinical_note_scan') {
                            $("#img-tst").append('<img width="200" class="img-thumbnail" src="uploads/temp/' + result.data + '">');
                            $("#img-name").append('<input type="hidden" name="files[]"  value="' + result.data + '">');
                        } else if(type == 'cinical_intervention'){
                            $("#img-tst_2").append('<img width="200" class="img-thumbnail_2" src="uploads/temp/' + result.data + '">');
                            $("#img-name_2").append('<input type="hidden" name="files2[]"  value="' + result.data + '">');
                        } else if(type == 'homework'){
                            $("#img-tst_3").append('<img width="200" class="img-thumbnail_3" src="uploads/temp/' + result.data + '">');
                            $("#img-name_3").append('<input type="hidden" name="files3[]"  value="' + result.data + '">');
                        } else if(type == 'observations'){
                            $("#img-tst_4").append('<img width="200" class="img-thumbnail_4" src="uploads/temp/' + result.data + '">');
                            $("#img-name_4").append('<input type="hidden" name="files4[]"  value="' + result.data + '">');
                        }
                        i++;
						if (i < all_canvas.length) {
							__save(all_canvas[i]);
						} else {
							$('#webcam').modal('hide');
							$('#save_btn').attr('disabled', false);
						}
					
					}
				});
			}
			
			__save(all_canvas[i])
		}

		function clear_canvas() {
			var canvas = document.querySelector('.write_canvas.active');
			var context = canvas.getContext('2d');
			context.clearRect(0, 0, canvas.width, canvas.height);
		}



		function open_modal(type) {
			window.scrollTo(0, 0);

            if (type == 'clinical_note_scan') {
                $('.modal-title').text('Handwriting mode');
            } else if (type == 'cinical_intervention') {
                $('.modal-title').text('Cinical intervention');
            } else if (type == 'homework') {
                $('.modal-title').text('Homework');
            } else if (type == 'observations') {
                $('.modal-title').text('Observations');
            }

            $('#webcam').modal('show').attr('data-type', type);
        }

	</script>
</body>

</html>