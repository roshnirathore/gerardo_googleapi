<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Webcamp Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	crossorigin="anonymous">

	<style>
	.form-group label:not(.checkbox-inline) {
		text-transform: uppercase;
	}
	</style>
</head>

<body>
	<h1 class="text-center">Clinical Note Scan</h1>
	<form action="submit.php" method="POST">
		<div class="container">

			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="form-group">
						<h4 style="font-size: 15px; font-weight: 600;">TYPE OF THERAPY</h4>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="In person"> In person
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Over the phone"> Over the phone
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Teleconference"> Teleconference
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" id="type_of_therapy" name="type_of_therapy[]" value="Secure text or email"> Secure text or email
						</label>
					</div>
					<div class="form-group">
						<label for="client_name">CLIENT'S NAME</label>
						<input type="text" class="form-control" name="client_name" id="client_name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<label for="date">DATE</label>
						<input type="date" class="form-control" id="date" name="date" placeholder="DATE" required value="<?= date('Y-m-d') ?>">
					</div>
					<div class="form-group">
						<label for="clinical_notes">PRESENTING PROBLEM</label>
						<textarea class="form-control" rows="3" name="individual_snapshot_presenting_problem" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">CLINICAL OBSERVATIONS</label>
						<textarea class="form-control" rows="3" name="individual_snapshot_clinical_observations" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">TREATMENT/ CLINICAL INTERVENTION</label>
						<textarea class="form-control" rows="3" name="individual_snapshot_clinical_intervention" required></textarea>
					</div>

					<div class="form-group">
						<label for="clinical_notes">HOMEWORK</label>
						<textarea class="form-control" rows="3" name="individual_snapshot_homework" required></textarea>
					</div>

					<div class="form-group">
						<label for="observations">OBSERVATIONS</label>
						<textarea class="form-control" rows="3" name="individual_snapshot_observations" required></textarea>
					</div>

					<div class="form-group">
						<label for="observations">CLINICAL NOTE & WRITING MODE</label>
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<button type="button" class="form-control" onclick="open_modal_camera('clinical_note_scan')">Click here to take a picture of your writing notes</button>
								<div id="img-tst" class="text-center" style="margin-top: 10px;"></div>
								<div id="img-name"></div>
							</div>
							<div class="col-md-1"></div>
						</div>
					</div>


					<div class="form-group">
						<label for="therapist_name">THERAPIST NAME</label>
						<input type="text" class="form-control" id="therapist_name" name="therapist_name" placeholder="THERAPIST NAME" required>
					</div>
					<button type="submit" class="btn btn-default" name="submit">Submit</button>
				</div>
				<div class="col-md-3">



				</div>
			</div>


		</div>
	</form>



	<!-- Modal -->
	<div class="modal fade" id="webcam_camera" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Capture Image</h4>
				</div>
				<div class="modal-body" id="iframe">
					<div class="form-group text-center">
						<select name="camera" id="select">
							
						</select>
					</div>
					<video onclick="snapshot(this);" style="width:100%;" width=500 height=400 id="video" controls autoplay></video>
					<canvas id="myCanvas" style="margin-left: 36px; display: none;" width="500" height="500"></canvas>
				</div>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<button type="button" style="margin-bottom: 10px;" class="btn-block" id="cam_save_btn" onclick="snapshot();">Save</button>
					</div>
					<div class="col-sm-4"></div>
				</div>
			</div>

		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

	<script type="text/javascript">
		var select = document.getElementById('select');

		$(document).ready(function() {
			init();

		});

		navigator.getUserMedia = (navigator.getUserMedia ||
			navigator.webkitGetUserMedia ||
			navigator.mozGetUserMedia ||
			navigator.msGetUserMedia);

		var video;
		var webcamStream;


		select.addEventListener('change', function(e) {
		
			webcamStream.getTracks().forEach(track => {
				track.stop();
			});

			startWebcam()
		})

		function gotDevices(mediaDevices) {
			select.innerHTML = '<option value="0" selected disabled>- Select Camera -</option>';
			
			let count = 1;
			mediaDevices.forEach(mediaDevice => {
				if (mediaDevice.kind === 'videoinput' && mediaDevice.deviceId != '') {
					var option = document.createElement('option');
					option.value = mediaDevice.deviceId;
					var label = mediaDevice.label || `Camera ${count++}`;
					var textNode = document.createTextNode(label);
					option.appendChild(textNode);
					select.appendChild(option);
					
				}
			});
		}

		function startWebcam() {
			if (navigator.getUserMedia) {

				navigator.mediaDevices.enumerateDevices().then(gotDevices);


				const videoConstraints = {};
				if (select.value === '') {
					videoConstraints.facingMode = 'environment';
				} else {
					videoConstraints.deviceId = { exact: select.value };
				}

				var constraints = {
					audio: false,
					video: videoConstraints
				};

				function handleSuccess(stream) {
					//self.stream = stream; // make stream available to browser console
					console.log(stream);
					webcamStream = stream;
					video = document.querySelector('video');
					video.srcObject = stream;
					video.src = window.URL.createObjectURL(stream);
					video.onloadedmetadata = function(e) {
						video.play();
					};

					
				}

				function handleError(error) {
					console.log('navigator.getUserMedia error: ', error);
				}

				navigator.mediaDevices.getUserMedia(constraints)
					.then(handleSuccess)
					.catch(handleError);

			} else {
				console.log("getUserMedia not supported");
			}
		}

		function stopWebcam() {
			//webcamStream.stop();
			webcamStream.getTracks()[0].stop();

			$('#webcam_camera').modal('hide');
		}
		//---------------------
		// TAKE A SNAPSHOT CODE
		//---------------------
		var canvas, ctx;

		function init() {
			// Get the canvas and obtain a context for
			// drawing in it
			canvas = document.getElementById("myCanvas");
			ctx = canvas.getContext('2d');
			video = document.querySelector('video');
		}

		function snapshot() {
			$('#cam_save_btn').attr('disabled', true);
			// Draws current image from the video element into the canvas
			ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

			var capturedImage = canvas.toDataURL("image/png");
			//console.log(capturedImage);
			//window.open(capturedImage)

			$.ajax({
				type: 'POST',
				url: "submit.php",
				data: {
					ajax_file_save: 1,
					image_data: capturedImage
				},
				success: function(result) {
					var result = JSON.parse(result);
					console.log(result)

					var type = $('#webcam_camera').attr('data-type');

					if (type == 'clinical_note_scan') {
						$("#img-tst").append('<img width="200" class="img-thumbnail" src="uploads/temp/' + result.data + '">');
						$("#img-name").append('<input type="hidden" name="cap_image"  value="' + result.data + '">');
					} else if (type == 'cinical_intervention') {
						$("#img-tst_2").append('<img width="200" class="img-thumbnail_2" src="uploads/temp/' + result.data + '">');
						$("#img-name_2").append('<input type="hidden" name="cap_image_2"  value="' + result.data + '">');
					} else if (type == 'homework') {
						$("#img-tst_3").append('<img width="200" class="img-thumbnail_3" src="uploads/temp/' + result.data + '">');
						$("#img-name_3").append('<input type="hidden" name="cap_image_3"  value="' + result.data + '">');
					} else if (type == 'observations') {
						$("#img-tst_4").append('<img width="200" class="img-thumbnail_4" src="uploads/temp/' + result.data + '">');
						$("#img-name_4").append('<input type="hidden" name="cap_image_4"  value="' + result.data + '">');
					}

					$('#cam_save_btn').attr('disabled', false);

					webcamStream.getTracks()[0].stop();

					$('#webcam_camera').modal('hide');
				}
			});







		}

		function open_modal_camera(type) {
			if (type == 'clinical_note_scan') {
				$('.modal-title').text('CLINICAL NOTE SCAN');
			} else if (type == 'cinical_intervention') {
				$('.modal-title').text('Cinical intervention');
			} else if (type == 'homework') {
				$('.modal-title').text('Homework');
			} else if (type == 'observations') {
				$('.modal-title').text('Observations');
			}

			
			
			$('#webcam_camera').modal('show').attr('data-type', type);
			
		}

		$('#webcam_camera').on('shown.bs.modal', function() {
			var modal_div = document.getElementById('iframe'); 

			canvas.width = modal_div.clientWidth;
			canvas.height = (window.innerHeight - (50 + 30 + 56 + 15 + 44));

			video.width = modal_div.clientWidth;
			video.height = (window.innerHeight - (50 + 30 + 56 + 15 + 44));

			startWebcam();
		});
	</script>
</body>

</html>